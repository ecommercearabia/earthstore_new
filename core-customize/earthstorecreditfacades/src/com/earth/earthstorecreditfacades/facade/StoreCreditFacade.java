/**
 *
 */
package com.earth.earthstorecreditfacades.facade;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.earth.earthstorecreditfacades.StoreCreditHistoryData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface StoreCreditFacade
{

	public Optional<PriceData> getStoreCreditAmountByCurrentUserAndCurrentBaseStore();


	public Optional<PriceData> getStoreCreditAmountByCurrentUser(BaseStoreModel baseStoreModel);


	public Optional<PriceData> getStoreCreditAmount(AbstractOrderModel abstractOrderModel);


	public Optional<List<StoreCreditHistoryData>> getStoreCreditHistoryByCurrentUser();

	public Optional<PriceData> getStoreCreditAmountFullRedeem(AbstractOrderModel abstractOrderModel);

}
