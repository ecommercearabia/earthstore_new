package com.earth.earthstorecreditfacades.facade;

import java.util.List;
import java.util.Optional;

import com.earth.earthstorecreditfacades.data.StoreCreditModeData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface StoreCreditModeFacade
{
	public Optional<StoreCreditModeData> getStoreCreditMode(String storeCreditModeTypeCode);

	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModesCurrentBaseStore();

	public boolean isStoreCreditModeSupportedByCurrentBaseStore(final String StoreCreditTypeCode);

}
