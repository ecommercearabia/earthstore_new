/**
 *
 */
package com.earth.earthstorecreditfacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.earth.earthstorecredit.enums.StoreCreditModeType;
import com.earth.earthstorecredit.model.StoreCreditModeModel;
import com.earth.earthstorecreditfacades.data.StoreCreditModeData;
import com.earth.earthstorecreditfacades.data.StoreCreditModeTypeData;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class StoreCreditModePopulator implements Populator<StoreCreditModeModel, StoreCreditModeData>
{

	@Resource(name = "storeCreditModeTypeConverter")
	private Converter<StoreCreditModeType, StoreCreditModeTypeData> storeCreditModeTypeConverter;


	@Override
	public void populate(final StoreCreditModeModel source, final StoreCreditModeData target)
	{
		target.setDescription(source.getDescription());

		target.setName(StringUtils.isEmpty(source.getDisplayName()) ? source.getName() : source.getDisplayName());

		if (source.getStoreCreditModeType() != null)
		{
			target.setStoreCreditModeType(storeCreditModeTypeConverter.convert(source.getStoreCreditModeType()));
		}

	}
}
