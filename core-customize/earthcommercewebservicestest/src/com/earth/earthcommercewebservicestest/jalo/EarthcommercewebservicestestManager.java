/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservicestest.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthcommercewebservicestest.constants.EarthcommercewebservicestestConstants;

import org.apache.log4j.Logger;


public class EarthcommercewebservicestestManager extends GeneratedEarthcommercewebservicestestManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(EarthcommercewebservicestestManager.class.getName());

	public static final EarthcommercewebservicestestManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcommercewebservicestestManager) em.getExtension(EarthcommercewebservicestestConstants.EXTENSIONNAME);
	}

}
