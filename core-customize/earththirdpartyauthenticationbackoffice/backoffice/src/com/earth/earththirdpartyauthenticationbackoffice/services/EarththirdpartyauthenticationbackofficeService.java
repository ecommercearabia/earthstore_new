/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earththirdpartyauthenticationbackoffice.services;

/**
 * Hello World EarththirdpartyauthenticationbackofficeService
 */
public class EarththirdpartyauthenticationbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
