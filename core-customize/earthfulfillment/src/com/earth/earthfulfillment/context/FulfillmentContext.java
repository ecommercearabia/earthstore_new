package com.earth.earthfulfillment.context;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.earth.earthfulfillment.enums.FulfillmentProviderType;
import com.earth.earthfulfillment.exception.FulfillmentException;


/**
 *
 */
public interface FulfillmentContext
{
	public Optional<String> createShipmentByCurrentStore(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<byte[]> printAWBByCurrentStore(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<String> getStatusByCurrentStore(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatusByCurrentStore(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderType type)
			throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderType type) throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderType type) throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel, FulfillmentProviderType type)
			throws FulfillmentException;
}