package com.earth.earthfulfillment.shipox.model.request;

import java.io.Serializable;

import com.earth.earthfulfillment.shipox.model.request.enums.ChargeType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ChargeItem implements Serializable{

	private ChargeType charge_type;
	private Boolean paid;
	private Double charge;
	private String payer;

	public ChargeType getCharge_type() {
		return charge_type;
	}
	public void setCharge_type(final ChargeType charge_type) {
		this.charge_type = charge_type;
	}
	public Boolean isPaid() {
		return paid;
	}
	public void setPaid(final Boolean paid) {
		this.paid = paid;
	}
	public Double getCharge() {
		return charge;
	}
	public void setCharge(final Double charge) {
		this.charge = charge;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(final String payer) {
		this.payer = payer;
	}
	@Override
	public String toString() {
		return "ChargeItem [charge_type=" + charge_type + ", paid=" + paid + ", charge=" + charge + ", payer=" + payer
				+ "]";
	}

}
