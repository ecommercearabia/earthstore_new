/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.shipox.model;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.earth.earthfulfillment.model.ShipoxFulfillmentProviderModel;


/**
 *
 */
public class ShipoxOrderContainer
{

	private ConsignmentModel consignment;
	private ShipoxFulfillmentProviderModel provider;

	public ShipoxOrderContainer()
	{
	}

	/**
	 *
	 */
	public ShipoxOrderContainer(final ConsignmentModel consignment, final ShipoxFulfillmentProviderModel provider)
	{
		super();
		this.consignment = consignment;
		this.provider = provider;
	}

	/**
	 * @return the consignment
	 */
	public ConsignmentModel getConsignment()
	{
		return consignment;
	}

	/**
	 * @param consignment
	 *           the consignment to set
	 */
	public void setConsignment(final ConsignmentModel consignment)
	{
		this.consignment = consignment;
	}

	/**
	 * @return the provider
	 */
	public ShipoxFulfillmentProviderModel getProvider()
	{
		return provider;
	}

	/**
	 * @param provider
	 *           the provider to set
	 */
	public void setProvider(final ShipoxFulfillmentProviderModel provider)
	{
		this.provider = provider;
	}



}
