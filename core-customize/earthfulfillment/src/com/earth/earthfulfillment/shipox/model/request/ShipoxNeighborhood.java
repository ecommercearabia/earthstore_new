package com.earth.earthfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ShipoxNeighborhood implements Serializable{

	private String id;
	private String name;

	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "NeighborhoodData [id=" + id + ", name=" + name + "]";
	}

}
