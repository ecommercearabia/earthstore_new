package com.earth.earthfulfillment.shipox.model.request.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DeliveryNote {
    @JsonProperty("do_not_deliver")
	DO_NOT_DELIVER("do_not_deliver"),  @JsonProperty("leave_at_door")LEAVE_AT_DOOR("leave_at_door"),@JsonProperty("leave_with_concierge") LEAVE_WITH_CONCIERGE("leave_with_concierge");

	private String value;

	private DeliveryNote(final String value) {
		this.value = value;

	}

	public String getValue() {
		return value;
	}
	/*
	 * public static String toString() { return value; }
	 */

}
