package com.earth.earthfulfillment.saee.service;

import java.util.Optional;

import com.earth.earthfulfillment.saee.exceptions.SaeeException;
import com.earth.earthfulfillment.saee.model.SaeeOrderData;
import com.earth.earthfulfillment.saee.model.responses.Details;


public interface SaeeService
{

	public Optional<String> createShipment(SaeeOrderData param, String baseURL) throws SaeeException;

	public Optional<byte[]> getPDF(String waybill, String baseURL) throws SaeeException;

	public Optional<Details> getStatus(String waybill, String baseURL) throws SaeeException;
}
