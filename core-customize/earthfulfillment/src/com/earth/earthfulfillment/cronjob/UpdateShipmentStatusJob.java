/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.cronjob;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.earthfulfillment.context.FulfillmentContext;
import com.earth.earthfulfillment.model.UpdateDeliveryStatusCronJobModel;
import com.earth.earthfulfillment.service.CustomConsignmentService;


/**
 *
 */
public class UpdateShipmentStatusJob extends AbstractJobPerformable<UpdateDeliveryStatusCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(UpdateShipmentStatusJob.class);

	private static final String CRONJOB_FINISHED = "UpdateShipmentStatusJob is Finished ...";

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Override
	public PerformResult perform(final UpdateDeliveryStatusCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");

		final List<ConsignmentModel> consignments = customConsignmentService
				.getConsignmentsByNotStatus(ConsignmentStatus.DELIVERY_COMPLETED);


		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		consignments.stream()
				.filter(consignment -> Objects.nonNull(consignment) && StringUtils.isNotBlank(consignment.getTrackingID()))
				.forEach(c -> checkAndChangeConsignmentStatus(c));
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
	{
		try
		{
			final Optional<ConsignmentStatus> optionalStatus = fulfillmentContext.updateStatusByCurrentStore(consignment);
			if (optionalStatus.isEmpty())
			{
				LOG.error("Empty status for consignment: " + consignment.getCode());
				return;
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			String msg = e.getMessage();
			if (e.getCause() != null)
			{
				msg = e.getCause().getMessage();
				if (e.getCause().getCause() != null)
				{
					msg = e.getCause().getCause().getMessage();
				}
			}
			return;
		}
		LOG.info("Consignment {} has been updated successfully", consignment.getCode());
	}

}
