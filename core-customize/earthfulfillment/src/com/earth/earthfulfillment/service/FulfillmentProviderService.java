/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderService
{
	public Optional<FulfillmentProviderModel> get(String code, final Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getActive(String baseStoreUid, final Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getActive(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass);
}
