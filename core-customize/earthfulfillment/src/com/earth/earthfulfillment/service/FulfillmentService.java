/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentService
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel,
			FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException;
}