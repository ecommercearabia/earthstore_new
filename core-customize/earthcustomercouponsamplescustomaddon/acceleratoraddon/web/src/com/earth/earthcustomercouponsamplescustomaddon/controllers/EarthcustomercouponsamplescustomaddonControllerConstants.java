/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponsamplescustomaddon.controllers;

/**
 */
public interface EarthcustomercouponsamplescustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
