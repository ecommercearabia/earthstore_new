/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponsamplescustomaddon.constants;

/**
 * Global class for all Earthcustomercouponsamplescustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class EarthcustomercouponsamplescustomaddonWebConstants // NOSONAR
{
	private EarthcustomercouponsamplescustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
