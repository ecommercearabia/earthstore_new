/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthotpbackoffice.services;

/**
 * Hello World EarthotpbackofficeService
 */
public class EarthotpbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
