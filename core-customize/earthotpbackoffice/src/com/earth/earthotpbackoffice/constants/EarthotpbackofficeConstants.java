/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthotpbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class EarthotpbackofficeConstants extends GeneratedEarthotpbackofficeConstants
{
	public static final String EXTENSIONNAME = "earthotpbackoffice";

	private EarthotpbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
