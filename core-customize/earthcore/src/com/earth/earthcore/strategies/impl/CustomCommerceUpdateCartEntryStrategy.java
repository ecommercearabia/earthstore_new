/**
 *
 */
package com.earth.earthcore.strategies.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCommerceUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy
{
	@Resource(name = "customCommerceAddToCartStrategy")
	private CustomCommerceAddToCartStrategy customCommerceAddToCartStrategy;

	/**
	 * @return the customCommerceAddToCartStrategy
	 */
	protected CustomCommerceAddToCartStrategy getCustomCommerceAddToCartStrategy()
	{
		return customCommerceAddToCartStrategy;
	}

	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return getCustomCommerceAddToCartStrategy().getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
				pointOfServiceModel);
	}
}
