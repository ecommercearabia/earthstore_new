/**
 *
 */
package com.earth.earthcore.strategies.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthcore.service.AbstractSerialNumberConfigurationService;
import com.earth.earthcore.strategies.SerialNumberConfigurationStrategy;


/**
 * @author monzer
 *
 */
public class ConsignmentSerialNumberConfigurationStrategy implements SerialNumberConfigurationStrategy
{

	@Resource(name = "consignmentSerialNumberService")
	private AbstractSerialNumberConfigurationService consignmentSerialNumberService;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore()
	{
		return consignmentSerialNumberService.generateSerialNumberByCurrentStore();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		return consignmentSerialNumberService.generateSerialNumberForBaseStore(baseStore);
	}

}
