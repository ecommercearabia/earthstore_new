/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.sourcingbanconfig.exception;

/**
 * @author monzer
 *
 */
public class AmbiguousSourcingBanConfigurationException extends RuntimeException
{
	public AmbiguousSourcingBanConfigurationException(final String message)
	{
		super(message);
	}
}