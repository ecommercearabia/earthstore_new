/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.sourcingbanconfig.service;

import de.hybris.platform.warehousing.sourcing.ban.service.SourcingBanService;

import com.earth.earthcore.model.SourcingBanConfigModel;

/**
 * The Interface CustomSourcingBanService.
 *
 * @author monzer
 */
public interface CustomSourcingBanService extends SourcingBanService
{

	/**
	 * Gets the sourcing ban configuration.
	 *
	 * @return the sourcing ban configuration
	 */
	SourcingBanConfigModel getSourcingBanConfiguration();
}
