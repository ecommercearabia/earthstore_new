/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.sourcingbanconfig.dao;

import com.earth.earthcore.model.SourcingBanConfigModel;


/**
 * @author monzer
 */
public interface SourcingBanConfigurationDao
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
