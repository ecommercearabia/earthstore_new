/**
 *
 */
package com.earth.earthcore.dao.impl;

import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.List;
import java.util.Optional;

import org.springframework.util.CollectionUtils;

import com.earth.earthcore.dao.CustomWarehouseDAO;


/**
 * @author mbaker
 *
 */
public class DefaultCustomWarehouseDAO extends DefaultWarehouseDao implements CustomWarehouseDAO
{
	private static final String BASE_SELECT_QUERY = "SELECT {p:" + WarehouseModel.PK + "} " + "FROM {" + WarehouseModel._TYPECODE
			+ " AS p} ";

	private static final String ERP_LOCATION_CODE = "erpLocationCode";

	@Override
	public Optional<WarehouseModel> findWarehouseByERPLocationCode(final String code)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + WarehouseModel.ERPLOCATIONCODE + "}=?"
				+ ERP_LOCATION_CODE;

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(ERP_LOCATION_CODE, code);

		final List<WarehouseModel> result = getFlexibleSearchService().<WarehouseModel> search(query).getResult();
		return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.ofNullable(result.get(0));
	}

	@Override
	public List<WarehouseModel> findWarehouseWithERPLocationCode()
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + WarehouseModel.ERPLOCATIONCODE + "}" + "IS NOT NULL";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		return getFlexibleSearchService().<WarehouseModel> search(query).getResult();
	}



}
