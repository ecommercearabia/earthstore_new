/**
 *
 */
package com.earth.earthcore.dao;

import de.hybris.platform.ordersplitting.daos.WarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;
import java.util.Optional;


/**
 * @author mbaker
 *
 */
public interface CustomWarehouseDAO extends WarehouseDao
{
	public Optional<WarehouseModel> findWarehouseByERPLocationCode(String code);

	public List<WarehouseModel> findWarehouseWithERPLocationCode();
}
