/**
 *
 */
package com.earth.earthcore.dao;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomOrderDAO
{
	OrderModel findOrderByCode(final String code);
}
