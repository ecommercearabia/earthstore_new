/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;
import java.util.Optional;


/**
 * @author mbaker
 *
 */
public interface CustomWarehouseService extends WarehouseService
{

	public Optional<WarehouseModel> getWarehouseByERPLocationCode(String code);

	public List<WarehouseModel> getWarehouseWithERPLocationCode();

}
