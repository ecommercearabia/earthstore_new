/**
 *
 */
package com.earth.earthcore.service.impl;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.type.TypeService;

import com.earth.earthcore.service.CustomCartService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomCartService extends DefaultCartService implements CustomCartService
{
	private TypeService typeService;

	protected KeyGenerator keyGenerator;

	@Override
	public CartModel createCartFromAbstractOrder(final OrderModel order)
	{
		return super.clone(getTypeService().getComposedTypeForClass(CartModel.class),
				getTypeService().getComposedTypeForClass(CartEntryModel.class), order, getKeyGenerator().generate().toString());
	}

	public TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}
}
