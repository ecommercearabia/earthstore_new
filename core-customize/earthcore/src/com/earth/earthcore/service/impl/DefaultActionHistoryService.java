/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.earthcore.enums.ActionHistoryType;
import com.earth.earthcore.model.ActionSourceHistoryModel;
import com.earth.earthcore.service.ActionHistoryService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultActionHistoryService implements ActionHistoryService
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void updateCartHistory(final AbstractOrderModel abstractOrder, final SalesApplication sourceApplication,
			final String operation, final String operationMethod)
	{
		Preconditions.checkArgument(abstractOrder != null, "Abstract Order can not be null");
		Preconditions.checkArgument(sourceApplication != null, "Source Application can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(operation), "Operation Name can not be null");

		abstractOrder.setLastOperationSource(sourceApplication);
		final ActionSourceHistoryModel cartHistory = modelService.create(ActionSourceHistoryModel.class);
		cartHistory.setOperationName(operation);
		cartHistory.setOperationSource(sourceApplication);
		cartHistory.setType(ActionHistoryType.CART);
		cartHistory.setOperationMethod(operationMethod);
		final List<ActionSourceHistoryModel> history = new ArrayList<ActionSourceHistoryModel>();
		if (abstractOrder.getCartOperationHistory() != null)
		{
			history.addAll(abstractOrder.getCartOperationHistory());
		}
		history.add(cartHistory);
		abstractOrder.setCartOperationHistory(history);
		modelService.save(abstractOrder);
	}

	@Override
	public void updateUserHistory(final UserModel user, final SalesApplication sourceApplication, final String operationName,
			final String operationMethod)
	{
		Preconditions.checkArgument(user != null, "Abstract Order can not be null");
		Preconditions.checkArgument(sourceApplication != null, "Source Application can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(operationName), "Operation Name can not be null");


		user.setLastOperationSource(sourceApplication);

		final ActionSourceHistoryModel userActionHistory = modelService.create(ActionSourceHistoryModel.class);
		userActionHistory.setOperationName(operationName);
		userActionHistory.setOperationSource(sourceApplication);
		userActionHistory.setType(ActionHistoryType.PROFILE);
		userActionHistory.setOperationMethod(operationMethod);
		final List<ActionSourceHistoryModel> history = new ArrayList<ActionSourceHistoryModel>();
		if (user.getUserOperationHistory() != null)
		{
			history.addAll(user.getUserOperationHistory());
		}
		history.add(userActionHistory);
		user.setUserOperationHistory(history);
		modelService.save(user);
	}

	@Override
	public void updateAddressHistory(final AddressModel address, final SalesApplication sourceApplication,
			final String operationName, final String operationMethod)
	{
		Preconditions.checkArgument(address != null, "Abstract Order can not be null");
		Preconditions.checkArgument(sourceApplication != null, "Source Application can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(operationName), "Operation Name can not be null");

		address.setLastOperationSource(sourceApplication);

		final ActionSourceHistoryModel addressActionHistory = modelService.create(ActionSourceHistoryModel.class);
		addressActionHistory.setOperationName(operationName);
		addressActionHistory.setOperationSource(sourceApplication);
		addressActionHistory.setType(ActionHistoryType.ADDRESS);
		addressActionHistory.setOperationMethod(operationMethod);
		final List<ActionSourceHistoryModel> history = new ArrayList<ActionSourceHistoryModel>();
		if (address.getAddressOperationHistory() != null)
		{
			history.addAll(address.getAddressOperationHistory());
		}
		history.add(addressActionHistory);
		address.setAddressOperationHistory(history);
		modelService.save(address);
	}

}