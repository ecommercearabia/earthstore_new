/**
 *
 */
package com.earth.earthcore.service.impl;

import de.hybris.platform.ordersplitting.impl.DefaultWarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.earth.earthcore.dao.CustomWarehouseDAO;
import com.earth.earthcore.service.CustomWarehouseService;
import com.google.common.base.Preconditions;


/**
 * @author mbaker
 *
 */
public class DefaultCustomWarehouseService extends DefaultWarehouseService implements CustomWarehouseService
{
	@Resource(name = "customWarehouseDAO")
	private CustomWarehouseDAO customWarehouseDAO;

	@Override
	public Optional<WarehouseModel> getWarehouseByERPLocationCode(final String code)
	{
		Preconditions.checkArgument(Strings.isNotBlank(code), "code is empty");
		return getCustomWarehouseDAO().findWarehouseByERPLocationCode(code);
	}

	@Override
	public List<WarehouseModel> getWarehouseWithERPLocationCode()
	{
		return getCustomWarehouseDAO().findWarehouseWithERPLocationCode();
	}

	/**
	 * @return the customWarehouseDAO
	 */
	protected CustomWarehouseDAO getCustomWarehouseDAO()
	{
		return customWarehouseDAO;
	}


}
