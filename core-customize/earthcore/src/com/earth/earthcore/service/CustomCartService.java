/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomCartService
{
	CartModel createCartFromAbstractOrder(final OrderModel order);
}
