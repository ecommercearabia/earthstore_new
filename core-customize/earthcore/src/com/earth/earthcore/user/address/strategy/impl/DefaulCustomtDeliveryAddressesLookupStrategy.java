/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.user.address.strategy.impl;

import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;


public class DefaulCustomtDeliveryAddressesLookupStrategy
		extends DefaultDeliveryAddressesLookupStrategy
		implements DeliveryAddressesLookupStrategy
{

	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		List<AddressModel> addressesForOrder = new ArrayList<>();
		if (abstractOrder != null)
		{
			final UserModel user = abstractOrder.getUser();
			if (user instanceof CustomerModel)
			{
				List<AddressModel> allAddresses;
				if (visibleAddressesOnly)
				{
					allAddresses = getCustomerAccountService().getAddressBookDeliveryEntries((CustomerModel) user);
				}
				else
				{
					allAddresses = getCustomerAccountService().getAllAddressEntries((CustomerModel) user);
				}
				final Collection<CountryModel> deliveryCountries = abstractOrder.getStore().getDeliveryCountries();

				if (!CollectionUtils.isEmpty(allAddresses) && !CollectionUtils.isEmpty(deliveryCountries))
				{
					final List<String> isoCodes = deliveryCountries.stream().map(CountryModel::getIsocode)
							.collect(Collectors.toList());
					addressesForOrder = allAddresses.stream().filter(address -> isoCodes.contains(address.getCountry().getIsocode()))
							.collect(Collectors.toList());
				}

				// If the user had no addresses, check the order for an address in case it's a guest checkout.
				if (getCheckoutCustomerStrategy().isAnonymousCheckout() && addressesForOrder.isEmpty()
						&& abstractOrder.getDeliveryAddress() != null)
				{
					addressesForOrder.add(abstractOrder.getDeliveryAddress());
				}
			}
		}
		return addressesForOrder;
	}
}
