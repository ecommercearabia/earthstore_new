/**
 *
 */
package com.earth.earthcore.user.area.service;

import java.util.List;
import java.util.Optional;

import com.earth.earthcore.model.AreaModel;


/**
 * The Interface AreaService.
 *
 * @author mnasro
 */

public interface AreaService
{

	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	public Optional<List<AreaModel>> getByCityCode(final String cityCode);

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public Optional<List<AreaModel>> getAll();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<AreaModel> get(final String code);
}
