/**
 *
 */
package com.earth.earthcore.user.nationality.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.CollectionUtils;

import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.dao.NationalityDao;


/**
 * The Class DefaultNationalityDao.
 *
 * @author mnasro
 */
public class DefaultNationalityDao extends DefaultGenericDao<NationalityModel> implements NationalityDao
{


	/**
	 * @param typecode
	 */
	public DefaultNationalityDao(final String typecode)
	{
		super(typecode);
	}


	/**
	 * Instantiates a new default area dao.
	 */
	public DefaultNationalityDao()
	{
		super(NationalityModel._TYPECODE);
	}


	/**
	 * Find.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<NationalityModel> find(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(NationalityModel.CODE, code);
		final List<NationalityModel> find = find(params);
		return find.stream().findFirst();
	}

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	@Override
	public List<NationalityModel> findAll()
	{
		final List<NationalityModel> find = find();
		return CollectionUtils.isEmpty(find) ? Collections.emptyList() : find;
	}
}
