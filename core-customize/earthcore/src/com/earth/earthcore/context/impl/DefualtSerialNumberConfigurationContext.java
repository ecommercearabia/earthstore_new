/**
 *
 */
package com.earth.earthcore.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthcore.context.SerialNumberConfigurationContext;
import com.earth.earthcore.enums.SerialNumberSource;
import com.earth.earthcore.strategies.SerialNumberConfigurationStrategy;


/**
 * @author monzer
 *
 */
public class DefualtSerialNumberConfigurationContext implements SerialNumberConfigurationContext
{

	@Resource(name = "serialNumberConfigurationStrategyMap")
	private Map<SerialNumberSource, SerialNumberConfigurationStrategy> serialNumberStrategyMap;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore(final SerialNumberSource source)
	{
		final Optional<SerialNumberConfigurationStrategy> strategy = getStrategyBySource(source);
		if (strategy.isPresent())
		{
			return strategy.get().generateSerialNumberByCurrentStore();
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore, final SerialNumberSource source)
	{
		final Optional<SerialNumberConfigurationStrategy> strategy = getStrategyBySource(source);
		if (strategy.isPresent())
		{
			return strategy.get().generateSerialNumberForBaseStore(baseStore);
		}
		return Optional.empty();
	}

	/**
	 *
	 */
	private Optional<SerialNumberConfigurationStrategy> getStrategyBySource(final SerialNumberSource source)
	{
		return Optional.ofNullable(serialNumberStrategyMap.get(source));
	}

}
