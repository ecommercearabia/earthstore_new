/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.populators.product;


import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class GeneralProductPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source == null || target == null)
		{
			return;
		}

		populateExpress(source, target);

	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateExpress(final ProductModel source, final ProductData target)
	{
		target.setExpressDelivery(source.isExpressDelivery());

	}


}
