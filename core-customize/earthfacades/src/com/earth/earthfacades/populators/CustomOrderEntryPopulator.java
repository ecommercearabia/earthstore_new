/**
 *
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author monzer
 *
 */
public class CustomOrderEntryPopulator extends OrderEntryPopulator
{

	@Override
	protected void addCommon(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addCommon(orderEntry, entry);
		if (orderEntry.getUnit() != null && "KG".equals(orderEntry.getUnit().getCode()))
		{
			entry.setWieghtedQuantity(String.valueOf(orderEntry.getQuantity() / 1000d));
		}
	}


	@Override
	protected void addTotals(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addTotals(orderEntry, entry);

		if (orderEntry.getBasePrice() != null)
		{
			// createPrice(orderEntry, value);
			entry.setBasePriceWithoutTax(createPrice(orderEntry, orderEntry.getBasePrice()));
			entry.setBasePriceWithTax(createPrice(orderEntry, orderEntry.getBasePrice()));
		}
		if (orderEntry.getTotalPrice() != null)
		{
			entry.setTotalPriceWithoutTax(createPrice(orderEntry, orderEntry.getTotalPrice()));
			entry.setTotalPriceWithTax(createPrice(orderEntry, orderEntry.getTotalPrice()));
		}

		if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
		{
			entry.setTotalDiscounts(DiscountValue.sumAppliedValues(orderEntry.getDiscountValues()));
		}

		if (!CollectionUtils.isEmpty(orderEntry.getTaxValues()))
		{
			entry.setTotalTaxAmount(TaxValue.sumAppliedTaxValues(orderEntry.getTaxValues()));
		}


	}

}
