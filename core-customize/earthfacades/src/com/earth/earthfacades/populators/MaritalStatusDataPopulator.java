/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.earth.earthcore.enums.MaritalStatus;
import com.earth.earthfacades.customer.data.MaritalStatusData;
import com.earth.earthfacades.product.data.GenderData;


/**
 * Populates {@link GenderData} with name and code.
 */
public class MaritalStatusDataPopulator implements Populator<MaritalStatus, MaritalStatusData>
{
	@Resource(name="typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	@Override
	public void populate(final MaritalStatus source, final MaritalStatusData target)
	{
		target.setCode(source.getCode());
		target.setName(source.name());
	}
}
