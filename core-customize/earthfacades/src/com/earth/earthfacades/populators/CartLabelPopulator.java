/**
 *
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.earth.earthcore.order.cart.service.CartValidationService;


/**
 * @author mbaker
 *
 */
public class CartLabelPopulator implements Populator<CartModel, CartData>
{

	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Override
	public void populate(final CartModel source, final CartData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		final Optional<String> cartMinAmountLabel = cartValidationService.getCartMinAmountLabel(source);
		final Optional<String> cartMaxAmountLabel = cartValidationService.getCartMaxAmountLabel(source);
		if (cartMinAmountLabel.isPresent())
		{
			target.setMinAmountLabel(cartMinAmountLabel.get());
		}

		if (cartMaxAmountLabel.isPresent())
		{
			target.setMaxAmountLabel(cartMaxAmountLabel.get());
		}
	}

}
