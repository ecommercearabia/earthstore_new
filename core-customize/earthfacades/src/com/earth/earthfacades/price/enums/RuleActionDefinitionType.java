/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.price.enums;

/**
 * The Enum RuleActionDefinitionType.
 *
 * @author amjad.shati@erabia.com
 */
public enum RuleActionDefinitionType
{

	/** The percentage discount. */
	PERCENTAGE_DISCOUNT("y_order_entry_percentage_discount"),
	/** The product free gift. */
	PRODUCT_FREE_GIFT("y_free_gift"),
	/** The fixed amount discount. */
	FIXED_AMOUNT_DISCOUNT("y_order_entry_fixed_discount");

	/** The name. */
	private String name;

	/**
	 * Instantiates a new rule action definition type.
	 *
	 * @param name
	 *           the name
	 */
	private RuleActionDefinitionType(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
}
