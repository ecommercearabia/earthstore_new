/**
 *
 */
package com.earth.earthfacades.user.nationality.facade.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.service.NationalityService;
import com.earth.earthfacades.user.nationality.facade.NationalityFacade;



/**
 * The Class DefaultNationalityFacade.
 *
 * @author mnasro
 */
public class DefaultNationalityFacade implements NationalityFacade
{
	/** The nationality service. */
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;
	/** The nationality converter. */
	@Resource(name = "nationalityConverter")
	private Converter<NationalityModel, NationalityData> nationalityConverter;

	/**
	 * Gets the nationality service.
	 *
	 * @return the nationalityService
	 */
	protected NationalityService getNationalityService()
	{
		return nationalityService;
	}

	/**
	 * Gets the nationality converter.
	 *
	 * @return the nationalityConverter
	 */
	protected Converter<NationalityModel, NationalityData> getNationalityConverter()
	{
		return nationalityConverter;
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<NationalityData> getAll()
	{
		final List<NationalityModel> nationalitys = getNationalityService().getAll();
		return CollectionUtils.isEmpty(nationalitys) ? Collections.emptyList() : getNationalityConverter().convertAll(nationalitys);
	}

	/**
	 * Gets the by site.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the by site
	 */
	@Override
	public List<NationalityData> getBySite(final CMSSiteModel cmsSiteModel)
	{
		final List<NationalityModel> nationalitys = getNationalityService().getBySite(cmsSiteModel);
		return CollectionUtils.isEmpty(nationalitys) ? Collections.emptyList() : getNationalityConverter().convertAll(nationalitys);
	}

	/**
	 * Gets the by current site.
	 *
	 * @return the by current site
	 */
	@Override
	public List<NationalityData> getByCurrentSite()
	{
		final List<NationalityModel> nationalitys = getNationalityService().getByCurrentSite();
		if (!CollectionUtils.isEmpty(nationalitys))
		{
			final List<NationalityData> list = getNationalityConverter().convertAll(nationalitys);
			Collections.sort(list, new Comparator<NationalityData>()
			{
				@Override
				public int compare(final NationalityData n1, final NationalityData n2)
				{
					return n1.getName().compareTo(n2.getName());
				}
			});
			return list;
		}
		return Collections.emptyList();

	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<NationalityData> get(final String code)
	{
		final Optional<NationalityModel> nationality = getNationalityService().get(code);
		if (nationality.isPresent())
		{
			return Optional.of(getNationalityConverter().convert(nationality.get()));
		}
		return Optional.empty();
	}

}
