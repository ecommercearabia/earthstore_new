/**
 *
 */
package com.earth.earthfacades.user.area.facade.impl;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.earthcore.model.AreaModel;
import com.earth.earthcore.user.area.service.AreaService;
import com.earth.earthfacades.user.area.facade.AreaFacade;



/**
 * The Class DefaultAreaFacade.
 *
 * @author mnasro
 */
public class DefaultAreaFacade implements AreaFacade
{

	/** The area service. */
	@Resource(name = "areaService")
	private AreaService areaService;


	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;



	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	@Override
	public Optional<List<AreaData>> getByCityCode(final String cityCode)
	{
		final Optional<List<AreaModel>> areas = getAreaService().getByCityCode(cityCode);
		if (areas.isPresent())
		{
			final List<AreaData> areaList = getAreaConverter().convertAll(areas.get());

			Collections.sort(areaList, (a1, a2) -> StringUtils.compare(a1.getName(), a2.getName()));

			return Optional.ofNullable(areaList);
		}
		return Optional.empty();
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<AreaData>> getAll()
	{
		final Optional<List<AreaModel>> areas = getAreaService().getAll();
		if (areas.isPresent())
		{
			return Optional.ofNullable(getAreaConverter().convertAll(areas.get()));
		}
		return Optional.empty();
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaData> get(final String code)
	{

		final Optional<AreaModel> area = getAreaService().get(code);
		if (area.isPresent())
		{
			return Optional.of(getAreaConverter().convert(area.get()));
		}
		return Optional.empty();
	}

	protected AreaService getAreaService()
	{
		return areaService;
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}
}
