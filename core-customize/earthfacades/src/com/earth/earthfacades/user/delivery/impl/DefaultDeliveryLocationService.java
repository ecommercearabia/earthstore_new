/**
 *
 */
package com.earth.earthfacades.user.delivery.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.earthfacades.customer.DeliveryCityAreaData;
import com.earth.earthfacades.customer.TimeDeliveryLocationData;
import com.earth.earthtimeslotfacades.PeriodData;
import com.earth.earthtimeslotfacades.exception.TimeSlotException;
import com.earth.earthtimeslotfacades.exception.type.TimeSlotExceptionType;
import com.earth.earthtimeslotfacades.facade.TimeSlotFacade;
import com.earth.earthcore.model.AreaModel;
import com.earth.earthcore.model.CityModel;
import com.earth.earthcore.user.area.service.AreaService;
import com.earth.earthcore.user.city.service.CityService;
import com.earth.earthfacades.user.area.facade.AreaFacade;
import com.earth.earthfacades.user.city.facade.CityFacade;
import com.earth.earthfacades.user.delivery.DeliveryLocationService;



/**
 * @author mohammad-abumuhasien
 *
 */
public class DefaultDeliveryLocationService implements DeliveryLocationService
{

	@Resource(name = "cityService")
	private CityService cityService;

	@Resource(name = "areaService")
	private AreaService areaService;

	@Resource(name = "customerService")
	private CustomerService customerService;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;
	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public Optional<TimeDeliveryLocationData> saveCustomerDeliveryLocation(final String areaCode, final String cityCode)
			throws TimeSlotException
	{

		if (StringUtils.isBlank(areaCode))
		{
			throw new IllegalArgumentException("AreaCode can not be empty or null");
		}
		if (StringUtils.isBlank(cityCode))
		{
			throw new IllegalArgumentException("City code can not be empty or null");
		}

		final Optional<AreaModel> area = areaService.get(areaCode);
		final Optional<CityModel> city = cityService.get(cityCode);


		if (!city.isPresent())
		{
			throw new IllegalArgumentException("could not find city with code " + cityCode);
		}


		final Optional<PeriodData> timeSlotData = timeSlotFacade.getFirstDeliverySlotForArea(areaCode);

		if (!timeSlotData.isPresent())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOTS_FOUND, "No timeslots found");

		}
		final Optional<TimeDeliveryLocationData> customerDeliveryLocationData = getTimeDeliveryLocationData(areaCode, cityCode);

		if (!customerDeliveryLocationData.isPresent())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_CONFIGURATIONS_AVAILABLE,
					"No timeslots configured for this area");
		}

		final UserModel currentUser = userService.getCurrentUser();
		if (userService.isAnonymousUser(currentUser) || !(currentUser instanceof CustomerModel))
		{
			final Session currentSession = sessionService.getCurrentSession();
			currentSession.setAttribute("selectedCityCode", cityCode);
			currentSession.setAttribute("selectedAreaCode", areaCode);
		}
		else
		{
			final CustomerModel customer = (CustomerModel) userService.getCurrentUser();
			customer.setSelectedDeliveryCity(city.get());
			customer.setSelectedDeliveryArea(area.orElse(null));
			modelService.save(customer);
		}

		return Optional.ofNullable(customerDeliveryLocationData.get());
	}

	@Override
	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationData(final String areaCode, final String cityCode)
			throws TimeSlotException

	{
		final Optional<PeriodData> timeSlotData = timeSlotFacade.getFirstDeliverySlotForArea(areaCode);
		if (timeSlotData.isEmpty())
		{
			return Optional.empty();
		}
		final TimeDeliveryLocationData timeDeliveryLocationData = new TimeDeliveryLocationData();
		final PeriodData periodData = timeSlotData.get();
		final Optional<AreaData> areaData = areaFacade.get(areaCode);
		final Optional<CityData> cityData = cityFacade.get(cityCode);

		timeDeliveryLocationData.setStartTime(periodData.getStart().format(DateTimeFormatter.ofPattern("HH:mm")));
		timeDeliveryLocationData.setEndTime(periodData.getEnd().format(DateTimeFormatter.ofPattern("HH:mm")));
		timeDeliveryLocationData.setFormatedTime(periodData.getIntervalFormattedValue());
		timeDeliveryLocationData.setFormatedDeliveryTimeMessage("Next Drive " + periodData.getIntervalFormattedValue());
		timeDeliveryLocationData.setCity(cityData.isPresent() ? cityData.get() : null);
		timeDeliveryLocationData.setArea(areaData.isPresent() ? areaData.get() : null);
		timeDeliveryLocationData.setFormatedDeliveryLocationMessage(" Deliver To : " + cityCode + " - " + areaCode);
		return Optional.ofNullable(timeDeliveryLocationData);
	}

	@Override
	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationDataForCurrentCustomer() throws TimeSlotException
	{
		final UserModel currentUser = userService.getCurrentUser();
		final String selectedCityCode = null;
		final String selectedAreaCode = null;
		final Optional<DeliveryCityAreaData> selectedDeliveryCityAndArea = getSelectedDeliveryCityAndArea();

		if (selectedDeliveryCityAndArea.isPresent())
		{
			return getTimeDeliveryLocationData(selectedDeliveryCityAndArea.get().getSelectedAreaCode(),
					selectedDeliveryCityAndArea.get().getSelectedCityCode());

		}
		else
		{
			return Optional.empty();
		}
	}

	@Override
	public Optional<DeliveryCityAreaData> getSelectedDeliveryCityAndArea()
	{
		final UserModel currentUser = userService.getCurrentUser();
		String selectedCityCode = null;
		String selectedAreaCode = null;
		if (userService.isAnonymousUser(currentUser) || !(currentUser instanceof CustomerModel))
		{

			selectedCityCode = sessionService.getAttribute("selectedCityCode");
			selectedAreaCode = sessionService.getAttribute("selectedAreaCode");

		}
		else
		{
			final CustomerModel customer = (CustomerModel) userService.getCurrentUser();
			selectedCityCode = customer.getSelectedDeliveryCity() == null ? null : customer.getSelectedDeliveryCity().getCode();
			selectedAreaCode = customer.getSelectedDeliveryArea() == null ? null : customer.getSelectedDeliveryArea().getCode();
		}
		if (StringUtils.isBlank(selectedCityCode) && cmsSiteService.getCurrentSite() != null
				&& cmsSiteService.getCurrentSite().getDefaultDeliveryLocationCity() != null)
		{
			selectedCityCode = cmsSiteService.getCurrentSite().getDefaultDeliveryLocationCity().getCode();
		}
		if (StringUtils.isBlank(selectedAreaCode) && cmsSiteService.getCurrentSite() != null
				&& cmsSiteService.getCurrentSite().getDefaultDeliveryLocationArea() != null)
		{
			selectedAreaCode = cmsSiteService.getCurrentSite().getDefaultDeliveryLocationArea().getCode();
		}
		final DeliveryCityAreaData selected = new DeliveryCityAreaData();
		selected.setSelectedCityCode(selectedCityCode);
		selected.setSelectedAreaCode(selectedAreaCode);
		return Optional.ofNullable(selected);
	}

}
