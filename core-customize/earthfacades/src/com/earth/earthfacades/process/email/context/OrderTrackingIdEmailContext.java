package com.earth.earthfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;



/**
 * @author monzer
 */
public class OrderTrackingIdEmailContext extends AbstractEmailContext<ConsignmentProcessModel>
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(OrderTrackingIdEmailContext.class);

	private static final String ORDER_CODE = "orderCode";
	private static final String TRACKING_ID = "trackingId";
	private static final String TRACKING_URL = "trackingURL";
	private static final String SHIPPER_NAME = "shipperName";
	private String orderCode;
	private String trackingId;
	private String shipperName;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Inits the.
	 *
	 * @param genarateOrderTrackingIdEmailModel
	 *           the email failed notification process model
	 * @param emailPageModel
	 *           the email page model
	 */
	@Override
	public void init(final ConsignmentProcessModel genarateOrderTrackingIdEmailModel, final EmailPageModel emailPageModel)
	{
		super.init(genarateOrderTrackingIdEmailModel, emailPageModel);

		if (genarateOrderTrackingIdEmailModel == null || genarateOrderTrackingIdEmailModel.getConsignment() == null
				|| genarateOrderTrackingIdEmailModel.getConsignment().getOrder() == null)
		{
			LOG.error("Consignment or Consignment order is null or empty");
			return;
		}
		final ConsignmentModel consignment = genarateOrderTrackingIdEmailModel.getConsignment();
		modelService.refresh(consignment);

		orderCode = consignment.getOrder().getCode();
		trackingId = consignment.getTrackingID();

		if (consignment.getCarrierDetails() != null && consignment.getCarrierDetails().getFulfillmentProviderType() != null)
		{
			shipperName = consignment.getCarrierDetails().getFulfillmentProviderType().getCode();
		}

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());
		if (fulfillmentProvider.isPresent())
		{
			put(TRACKING_URL, fulfillmentProvider.get().getTrackingUrl());
		}

		put(EMAIL, getCustomerEmailResolutionService().getEmailForCustomer(getCustomer(genarateOrderTrackingIdEmailModel)));
		put(DISPLAY_NAME, getCustomer(genarateOrderTrackingIdEmailModel).getDisplayName());
		put(ORDER_CODE, orderCode);
		put(TRACKING_ID, trackingId);
		put(SHIPPER_NAME, shipperName);

	}

	/**
	 * Gets the site.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final ConsignmentProcessModel consignmentProcessModel)
	{
		return consignmentProcessModel.getConsignment().getOrder().getSite();
	}

	/**
	 * Gets the customer.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final ConsignmentProcessModel consignmentProcessModel)
	{
		return (CustomerModel) consignmentProcessModel.getConsignment().getOrder().getUser();
	}

	/**
	 * Gets the email language.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final ConsignmentProcessModel consignmentProcessModel)
	{
		// may throw ClassCastException
		return ((OrderModel) consignmentProcessModel.getConsignment().getOrder()).getLanguage();
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the trackingId
	 */
	public String getTrackingId()
	{
		return trackingId;
	}

	/**
	 * @param trackingId
	 *           the trackingId to set
	 */
	public void setTrackingId(final String trackingId)
	{
		this.trackingId = trackingId;
	}

	/**
	 * @return the shipperName
	 */
	public String getShipperName()
	{
		return shipperName;
	}

	/**
	 * @param shipperName
	 *           the shipperName to set
	 */
	public void setShipperName(final String shipperName)
	{
		this.shipperName = shipperName;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	public FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param fulfillmentProviderContext
	 *           the fulfillmentProviderContext to set
	 */
	public void setFulfillmentProviderContext(final FulfillmentProviderContext fulfillmentProviderContext)
	{
		this.fulfillmentProviderContext = fulfillmentProviderContext;
	}


}
