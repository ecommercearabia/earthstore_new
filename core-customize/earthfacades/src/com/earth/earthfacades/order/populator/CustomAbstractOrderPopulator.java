package com.earth.earthfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.springframework.util.CollectionUtils;


/**
 *
 * The Class CustomAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class CustomAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{


	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populateAllEntriesCount(source, target);
			target.setOrderRefId(source.getOrderRefId());
		}

	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateAllEntriesCount(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (target instanceof CartData)
		{
			final int allEntriesCount = CollectionUtils.isEmpty(source.getEntries()) ? 0 : source.getEntries().size();
			((CartData) target).setAllEntriesCount(allEntriesCount);
		}
	}


}
