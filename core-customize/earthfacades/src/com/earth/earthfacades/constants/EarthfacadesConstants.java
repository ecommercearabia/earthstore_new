/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.constants;

/**
 * Global class for all Earthfacades constants.
 */
public class EarthfacadesConstants extends GeneratedEarthfacadesConstants
{
	public static final String EXTENSIONNAME = "earthfacades";

	private EarthfacadesConstants()
	{
		//empty
	}
}
