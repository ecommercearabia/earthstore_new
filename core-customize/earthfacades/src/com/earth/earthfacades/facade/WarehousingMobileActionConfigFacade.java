/**
 *
 */
package com.earth.earthfacades.facade;

import com.earth.earthfacades.dto.config.WarehousingActionConfigData;


/**
 * @author monzer
 *
 */
public interface WarehousingMobileActionConfigFacade
{

	WarehousingActionConfigData getWarehousingActionsConfigForConsignment(String consignmentCode);

}
