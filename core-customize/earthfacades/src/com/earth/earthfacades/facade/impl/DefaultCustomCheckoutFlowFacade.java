/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.facade.impl;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;

import com.earth.earthtimeslotfacades.exception.TimeSlotException;
import com.earth.earthtimeslotfacades.facade.TimeSlotFacade;
import com.earth.earthtimeslotfacades.validation.TimeSlotValidationService;
import com.earth.earthcore.order.cart.exception.CartValidationException;
import com.earth.earthcore.order.cart.service.CartValidationService;
import com.earth.earthfacades.facade.CustomAcceleratorCheckoutFacade;
import com.earth.earthfacades.facade.CustomCheckoutFlowFacade;




/**
 * @author mnasro
 *
 *         The Class DefaultCustomCheckoutFlowFacade.
 */
public class DefaultCustomCheckoutFlowFacade implements CustomCheckoutFlowFacade
{

	/** The checkout flow facade. */
	@Resource(name = "checkoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotValidationService")
	private TimeSlotValidationService timeSlotValidationService;

	@Override
	public boolean hasValidCart()
	{
		try
		{
			return checkoutFlowFacade.hasValidCart() && cartService.hasSessionCart()
					&& cartValidationService.validateCartMaxAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartMinAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartDeliveryType(cartService.getSessionCart());
		}
		catch (final CartValidationException e)
		{
			return false;
		}
	}

	/**
	 * Gets the checkout flow facade.
	 *
	 * @return the checkoutFlowFacade
	 */
	public de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade getCheckoutFlowFacade()
	{
		return checkoutFlowFacade;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	@Override
	public boolean hasNoPaymentMode()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || cartData.getPaymentMode() == null;
	}

	@Override
	public boolean hasPaymentProvider()
	{
		if (!"card".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode()))
		{
			return false;
		}

		return getCheckoutFacade().getSupportedPaymentProvider().isPresent();
	}

	/**
	 * @return the checkoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Override
	public boolean hasNoTimeSlot() throws TimeSlotException
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite()
				&& !timeSlotValidationService.validate(cartData.getTimeSlotInfoData());
	}

	@Override
	public boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException
	{
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite();
	}

}
