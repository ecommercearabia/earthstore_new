/**
 *
 */
package com.earth.earthfacades.facade.impl;


import de.hybris.platform.warehousingfacades.order.impl.DefaultWarehousingConsignmentFacade;

import java.util.Optional;

import javax.ws.rs.NotSupportedException;

import com.earth.earthfacades.facade.CustomWarehousingConsignmentFacade;

/**
 * @author monzer
 *
 */
public class DefaultCustomWarehousingConsignmentFacade extends DefaultWarehousingConsignmentFacade
		implements CustomWarehousingConsignmentFacade
{



	@Override
	public Optional<String> createShipment(final String consignmentCode)
	{
		throw new NotSupportedException();
	}

	@Override
	public Optional<byte[]> printConsignmentAWB(final String consignmentCode)
	{
		throw new NotSupportedException();
	}

	@Override
	public void updateConsignmentTrackingId(final String consignmentCode, final String trackingId)
	{
		throw new NotSupportedException();
	}



}
