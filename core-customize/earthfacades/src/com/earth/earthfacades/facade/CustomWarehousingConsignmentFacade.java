/**
 *
 */
package com.earth.earthfacades.facade;

import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.util.Optional;


/**
 * @author monzer
 *
 */
public interface CustomWarehousingConsignmentFacade extends WarehousingConsignmentFacade
{
	Optional<String> createShipment(String consignmentCode);

	Optional<byte[]> printConsignmentAWB(String consignmentCode);

	void updateConsignmentTrackingId(String consignmentCode, String trackingId);

}
