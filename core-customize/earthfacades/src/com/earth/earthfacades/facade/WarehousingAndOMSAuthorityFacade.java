/**
 *
 */
package com.earth.earthfacades.facade;

import java.util.List;

import com.earth.earthfacades.authority.AuthorityGroupData;


/**
 * @author monzer
 *
 */
public interface WarehousingAndOMSAuthorityFacade
{

	AuthorityGroupData getAllAuthorityGroupsForCurrentUser();

	AuthorityGroupData getAllAuthorityGroupsForUser(String userUid);

	List<String> getAllViewsAvailabelForUser(String userUid);

}
