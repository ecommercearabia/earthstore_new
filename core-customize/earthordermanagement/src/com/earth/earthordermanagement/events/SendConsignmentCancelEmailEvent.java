package com.earth.earthordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendConsignmentCancelEmailEvent extends ConsignmentProcessingEvent
{
	private static final long serialVersionUID = 1L;
	
	public SendConsignmentCancelEmailEvent(ConsignmentProcessModel process)
	{
		super(process);
	}
}
