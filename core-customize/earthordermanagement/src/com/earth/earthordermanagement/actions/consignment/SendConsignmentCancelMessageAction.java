package com.earth.earthordermanagement.actions.consignment;

import javax.annotation.Resource;

import com.earth.earthordermanagement.events.SendConsignmentCancelEmailEvent;

import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.task.RetryLaterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendConsignmentCancelMessageAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SendConsignmentCancelMessageAction.class);
	
	@Resource(name = "eventService")
	private EventService eventService;
	
	@Override
	public void executeAction(ConsignmentProcessModel process)
	{
		LOGGER.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		getEventService().publishEvent(new SendConsignmentCancelEmailEvent(process));
	}

	public EventService getEventService()
	{
		return eventService;
	}
}