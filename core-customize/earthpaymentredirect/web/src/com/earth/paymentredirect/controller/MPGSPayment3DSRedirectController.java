/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.paymentredirect.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.earth.earthpayment.exception.PaymentException;


/**
 *
 */
@Controller
@RequestMapping(value = "/mpgs/3ds")
public class MPGSPayment3DSRedirectController
{

	@RequestMapping(method =
	{ RequestMethod.POST, RequestMethod.GET })
	public String processAndValidRedirect(final HttpServletRequest request, final Model model) throws IOException, PaymentException
	{
		final String result = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);

		model.addAttribute("body", URLEncoder.encode(result, "UTF-8"));
		return "mpgsRedirect";
	}

}
