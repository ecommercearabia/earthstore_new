/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.paymentredirect.service;

public interface EarthpaymentredirectService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
