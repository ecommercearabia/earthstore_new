/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcmscustomocc.beans;

import java.io.Serializable;
import java.util.List;


/**
 * @author monzer
 */
public class ComponentResultListDTO implements Serializable
{
	/** Default serialVersionUID value. */

	private static final long serialVersionUID = 1L;

	/**
	 * <i>Generated property</i> for <code>ComponentResultListData.components</code> property defined at extension
	 * <code>earthcmscustomocc</code>.
	 */

	private List<ComponentResultDTO> components;

	public ComponentResultListDTO()
	{
		// default constructor
	}

	public void setComponents(final List<ComponentResultDTO> components)
	{
		this.components = components;
	}

	public List<ComponentResultDTO> getComponents()
	{
		return components;
	}
}
