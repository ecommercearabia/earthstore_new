/**
 *
 */
package com.earth.earthorderconfirmation.service;

import java.util.List;

import com.earth.earthorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailService
{
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
