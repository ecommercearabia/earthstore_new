/**
 *
 */
package com.earth.earthorderconfirmation.dao;

import java.util.List;

import com.earth.earthorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailDao
{
	/**
	 * @param storeUid
	 * @return
	 */
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
