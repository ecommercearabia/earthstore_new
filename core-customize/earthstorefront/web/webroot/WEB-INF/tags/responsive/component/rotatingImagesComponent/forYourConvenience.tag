<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/responsive/component"%>
<%@ attribute name="banners" required="false" type="java.util.List" %>



<div class="headline-title">${component.title}</div>


<div class="row Convienience">
		
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
					<c:if test="${ycommerce:validateUrlScheme(banner.media.url)}">
						<div class="col-xs-12 col-md-4">
							<div class="ConvienienceItem ${fn:escapeXml(banner.content)}">

							<c:choose>
								<c:when test="${!ycommerce:validateUrlScheme(encodedUrl)}">
									<img src="${fn:escapeXml(banner.media.url)}" 
										alt="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}" 
										title="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}"/>
								</c:when>
								
								<c:otherwise>
								
								
									<a tabindex="-1" href="${fn:escapeXml(encodedUrl)}"<c:if test="${banner.external}"> target="_blank"</c:if>>
										
										<img src="${fn:escapeXml(banner.media.url)}" 
											alt="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}" 
											title="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}"/>
										<h3>${fn:escapeXml(banner.content)}</h3>
									</a>
								</c:otherwise>
							</c:choose>

							</div>
						</div>
					</c:if>
					

					
				</c:if>
			</c:forEach>
		
	</div>

