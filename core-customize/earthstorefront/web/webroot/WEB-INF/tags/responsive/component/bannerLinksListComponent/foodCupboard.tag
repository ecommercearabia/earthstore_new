<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="bannerLinksList" required="false" type="com.earth.earthcomponents.model.BannerLinksListComponentModel" %>

<div class="carousel__component foodCupboard">


	<div class="titleSection"><span class="carousel__component--headline">${bannerLinksList.title}</span> <span class="linkView"><cms:component component="${bannerLinksList.link}"  /></span></div>

<%--
		<cms:component component="${bannerLinksList.link}"  />
--%>
<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-board owl-theme secondTheme">

	<c:forEach items="${bannerLinksList.bannerLinksComponents}"
			   step="4" varStatus="status"	var="bannerLinks">
		<div class="carousel__item"  >
			<div class="row">

		<c:url value="${bannerLinks.link.url}" var="simpleBannerUrl" />
		<%--<cms:component component="${bannerLinks.link}"  />--%>
		<div class="col-xs-12 ">
			<span class="fullColImage">

<div class=" banner__component simple-banner box_img text-center ">
	<c:if test="${ycommerce:validateUrlScheme(bannerLinks.media.url)}">
		<c:choose>
			<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
				<img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(bannerLinks.media.altText)}"
					src="${fn:escapeXml(bannerLinks.media.url)}">
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(simpleBannerUrl)}"><img title="${fn:escapeXml(bannerLinks.media.altText)}"
					alt="${fn:escapeXml(bannerLinks.media.altText)}" src="${fn:escapeXml(bannerLinks.media.url)}">
					<h6 class="titleBox">
						<span>	${bannerLinks.title}
					<br/>
						<button class="btn">SHOP NOW</button>
						</span>
					</h6>
				</a>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>
			</span>
		</div>

				<c:set var="limit" value="3"/>
				<c:if test="${not (status.index +3  <= bannerLinksList.bannerLinksComponents.size()-1)}">
					<c:set var="limit" value="${  bannerLinksList.bannerLinksComponents.size() -(status.index+1) }"/>

				</c:if>
		<c:forEach items="${bannerLinksList.bannerLinksComponents}"
				   var="SecondbannerLinks" begin="${status.index+1}" end="${status.index+limit}" >
			<div class="col-xs-4 ">
				<span class="subColImage">
				<div class=" banner__component simple-banner box_img text-center ">
					<c:if test="${ycommerce:validateUrlScheme(SecondbannerLinks.media.url)}">
						<c:choose>
							<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
								<img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(SecondbannerLinks.media.altText)}"
									 src="${fn:escapeXml(SecondbannerLinks.media.url)}">
							</c:when>
							<c:otherwise>
								<a href="${fn:escapeXml(simpleBannerUrl)}"><img title="${fn:escapeXml(SecondbannerLinks.media.altText)}"
																				alt="${fn:escapeXml(SecondbannerLinks.media.altText)}" src="${fn:escapeXml(SecondbannerLinks.media.url)}">
									<h6 class="titleBox">${SecondbannerLinks.title}

									</h6>
								</a>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
				</span>
			</div>

		</c:forEach>

	</div>
		</div>
	</c:forEach>
</div>
</div>


