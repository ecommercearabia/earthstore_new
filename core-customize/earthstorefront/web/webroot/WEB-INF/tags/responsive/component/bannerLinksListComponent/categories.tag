<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ attribute name="bannerLinksList" required="false" type="com.earth.earthcomponents.model.BannerLinksListComponentModel" %>

<div class="carousel__component foodCupboard">
	<div class="titleSection"><span class="carousel__component--headline">${bannerLinksList.title}</span> <span class="linkView"><cms:component component="${bannerLinksList.link}"  /></span></div>

	<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-freshFood owl-theme">

		<c:forEach items="${bannerLinksList.bannerLinksComponents}"
				   var="bannerLinks" step="2" varStatus="status">
			<div class="carousel__item"  >
			<span class="hoverAni">
	<div class="item_fresh">

		<c:url value="${bannerLinks.link.url}" var="simpleBannerUrl" />
<%-- 		<cms:component component="${bannerLinks.link}"  /> --%>

<div class="banner__component simple-banner box_img text-center">
	<c:if test="${ycommerce:validateUrlScheme(bannerLinks.media.url)}">
		<c:choose>
			<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
				<img  alt="${fn:escapeXml(bannerLinks.media.altText)}"
					  src="${fn:escapeXml(bannerLinks.media.url)}"/>
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(simpleBannerUrl)}"><img
						alt="${fn:escapeXml(bannerLinks.media.altText)}" src="${fn:escapeXml(bannerLinks.media.url)}"></a>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>


</div><h3 class="titleBox">${bannerLinks.title}</h3></span>
				<c:if test ="${status.index +1 != bannerLinksList.bannerLinksComponents.size() }">

					<c:forEach items="${bannerLinksList.bannerLinksComponents}"
							   var="SecondbannerLinks" begin="${status.index+1}" end="${status.index+1}" >
			<span class="hoverAni">
	<div class="item_fresh">

		<c:url value="${SecondbannerLinks.link.url}" var="simpleBannerUrl" />
<%-- 		<cms:component component="${SecondbannerLinks.link}"  /> --%>

<div class="banner__component simple-banner box_img text-center">
	<c:if test="${ycommerce:validateUrlScheme(SecondbannerLinks.media.url)}">
		<c:choose>
			<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
				<img alt="${fn:escapeXml(SecondbannerLinks.media.altText)}"
					 src="${fn:escapeXml(SecondbannerLinks.media.url)}">
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(simpleBannerUrl)}"><img
						alt="${fn:escapeXml(SecondbannerLinks.media.altText)}" src="${fn:escapeXml(SecondbannerLinks.media.url)}"></a>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>

</div><h3 class="titleBox">${SecondbannerLinks.title } </h3>
			</span>
					</c:forEach>




				</c:if>
			</div>







		</c:forEach>

	</div>

</div>
