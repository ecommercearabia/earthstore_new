<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<%-- <cms:pageSlot position="DeliveryLocation" var="component" element="ul" class="topsection "> --%>
<%-- 	<cms:component component="${component}"/> --%>
<%-- </cms:pageSlot> --%>


<cms:pageSlot position="TopHeaderSlot" var="component" element="div">
    <cms:component component="${component}" />
</cms:pageSlot>
<input type="hidden" class="titletoast" value='<spring:theme code="basket.added.to.basket"/>'>

<cms:pageSlot position="DeliveryLocation" var="component" element="ul" class="topsection ">
    <cms:component component="${component}" />
</cms:pageSlot>

<div class="branding-mobile row hidden-md hidden-lg">
    <div class="col-xs-1 btnMobilenav ">
        <button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation" type="button">
            <img src="${fn:escapeXml(themeResourcePath)}/images/menu.svg" />
        </button>
    </div>

    <div class="col-xs-4 js-mobile-logo">

    </div>
    <div class="col-xs-7 Cartmobile ">
        <c:if test="${not empty cartMinAmount}">
            <div class="minorder"><spring:theme code="minimum.Order.lbl" /> <span class="minval">${cartMinAmount}</span>
            </div>
        </c:if>
        <cms:pageSlot position="MiniCart" var="cart" element="div"
                      class="miniCartSlot componentContainer   hidden-md hidden-lg">
            <cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
        </cms:pageSlot>

    </div>
</div>
<div class="js-offcanvas-links">
    <header class="js-mainHeader ">

        <div class="darkback hidden"></div>
        <nav class="navigation navigation--top hidden-xs hidden-sm w1300">
            <div class="row desktop_header_nav">


                <div class="col-sm-12 col-md-4">
                    <div class="nav__left js-site-logo">
                        <cms:pageSlot position="SiteLogo" var="logo" limit="1">
                            <cms:component component="${logo}" element="div" class="yComponentWrapper" />
                        </cms:pageSlot>
                    </div>
                </div>

                <div class="col-md-4">


                    <div class="site-search">
                        <cms:pageSlot position="SearchBox" var="component">
                            <cms:component component="${component}" element="div" />
                        </cms:pageSlot>
                    </div>

                </div>

                <div class="col-sm-12 col-md-4">


                    <div class="nav__right">
                        <ul class="nav__links nav__links--account">

                            <li class="liOffcanvas MiniCartHeader">
                                <cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
                                    <cms:component component="${cart}" element="div" />
                                </cms:pageSlot>
                            </li>

                            <c:if test="${empty hideHeaderLinks}">
                                <c:if test="${uiExperienceOverride}">
                                    <li class="backToMobileLink">
                                        <c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
                                        <a href="${fn:escapeXml(backToMobileStoreUrl)}">
                                            <spring:theme code="text.backToMobileStore" />
                                        </a>
                                    </li>
                                </c:if>

                                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">

                                    <li class="liOffcanvas wishlisticon">
                                        <c:url value="/my-account/my-wishlist" var="wishlist" />
                                        <a href="${wishlist}">
                                            <i class="fas fa-heart redicon"></i>
                                        </a></li>

                                </sec:authorize>


                                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                    <c:set var="maxNumberChars" value="25" />
                                    <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                                        <c:set target="${user}" property="firstName"
                                               value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
                                    </c:if>

                                    <li class="logged_in js-logged_in hidden">


                                        <cms:pageSlot position="HeaderLinks" var="link">
                                            <cms:component component="${link}" element="span" />
                                        </cms:pageSlot>
                                        <span class="text-wellcome"><ycommerce:testId code="header_LoggedUser">
                                            <spring:theme code="header.welcome"
                                                          arguments="${user.firstName},${user.lastName}" />
                                        </ycommerce:testId></span>
                                        <i class="far fa-chevron-down header_arrow_list"></i>
                                    </li>
                                </sec:authorize>


                                <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">

                                    <li class="  liOffcanvas registration">
                                        <div class="tab_login_header hidden"><i class="fas fa-user redicon"></i>
                                        </div>
                                        <ycommerce:testId code="header_Login_link">
                                            <c:url value="/login" var="loginUrl" />

                                            <a class="login_action" href="javascript:;">
                                                <i class="fas fa-user redicon"></i><span
                                                    class="text-login"><spring:theme code="header.link.login" /></span>

                                            </a>
                                        </ycommerce:testId>
                                        <div class="login_box hidden">
                                            <ycommerce:testId code="header_Login_link">
                                                <cms:pageSlot position="LoginPopup" var="component" limit="1">
                                                    <cms:component component="${component}" />
                                                </cms:pageSlot>
                                            </ycommerce:testId>
                                        </div>


                                    </li>
                                </sec:authorize>


                            </c:if>


                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <%-- a hook for the my account links in desktop/wide desktop--%>
        <div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
            <ul class="nav__links">

            </ul>
        </div>
        <div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
            <ul class="nav__links js-nav__links">

            </ul>
        </div>
        <nav class="navigation navigation--middle js-navigation--middle">
            <div class="container-fluid">

                <div class="row hidden-md hidden-lg">
                    <div class="col-md-4 col-lg-5 col-xs-12 col-sm-12">
                        <div class="site-search">
                            <cms:pageSlot position="SearchBox" var="component">
                                <cms:component component="${component}" element="div" />
                            </cms:pageSlot>
                        </div>
                    </div>
                </div>
                <div class="row navmobileConetnt hidden">
                    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">


                        <div class="col-xs-4 serachNavmobile">

                            <ycommerce:testId code="header_search_activation_button">
                                <button class=" js-toggle-xs-search hidden-md hidden-lg" type="button">
                                    <span class="fal fa-search"></span>
                                </button>
                            </ycommerce:testId>


                        </div>


                        <div class="col-xs-4 callNavmobile">
                            <a href="tel:${cmsSite.contactUsNumber}">
                                <i class="fas fa-phone-alt"></i>
                            </a>

                        </div>
                        <div class="col-xs-4 AccountNavmobile">
                            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                <li class="tab_login_header hidden"><i class="fas fa-user"></i></li>

                                <li class="  liOffcanvas registration">
                                    <ycommerce:testId code="header_Login_link">
                                        <c:url value="/login" var="loginUrl" />

                                        <a class="login_action" href="${fn:escapeXml(loginUrl)}">
                                            <i class="fas fa-user"></i>

                                        </a>

                                        <c:url value="/login" var="loginUrl" />
                                        <a class="login_action_menu_register hidden" href="${fn:escapeXml(loginUrl)}">
                                            <i class="fas fa-user"></i> <spring:theme code="header.link.login" />
                                        </a>

                                    </ycommerce:testId>
                                    <div class="login_box hidden">
                                        <ycommerce:testId code="header_Login_link">
                                            <cms:pageSlot position="LoginPopup" var="component" limit="1">
                                                <cms:component component="${component}" />
                                            </cms:pageSlot>
                                        </ycommerce:testId>
                                    </div>


                                </li>
                            </sec:authorize>


                        </div>


                    </sec:authorize>


                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">

                        <div class="col-xs-3 serachNavmobile">

                            <ycommerce:testId code="header_search_activation_button">
                                <button class=" js-toggle-xs-search hidden-md hidden-lg" type="button">
                                    <span class="fal fa-search"></span>
                                </button>
                            </ycommerce:testId>


                        </div>

                        <div class="col-xs-3 wishlistNavmobile">
                            <c:url value="/my-account/my-wishlist" var="wishlist" />
                            <a href="${wishlist}" class="btn_wishlist">
                                <i class="fas fa-heart"></i>
                            </a>

                        </div>
                        <div class="col-xs-3 callNavmobile">
                            <a href="tel:${cmsSite.contactUsNumber}">
                                <i class="fas fa-phone-alt"></i>
                            </a>

                        </div>
                        <div class="col-xs-3 AccountNavmobile">


                            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                <c:set var="maxNumberChars" value="25" />
                                <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                                    <c:set target="${user}" property="firstName"
                                           value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
                                </c:if>


                            </sec:authorize>


                            <div class="darkbackMobile hidden"></div>
                            <div class="tab_profile_header hidden"><i class="fas fa-user"></i></div>
                            <div class="mobileAccountBox hidden ">
                                <span class="AccountMenuTitle">my account</span>
                                <nav class="accNavComponent__child-wrap mobileAccountMenu"></nav>

                            </div>
                        </div>
                    </sec:authorize>
                </div>


                <!-- 				<div class="mobile__nav__row mobile__nav__row--table"> -->
                <!-- 					<div class="mobile__nav__row--table-group"> -->
                <!-- 						<div class="mobile__nav__row--table-row"> -->
                <!-- 							<div class="mobile__nav__row--table-cell visible-xs visible-sm hidden-md" > -->
                <!-- 								<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation" -->
                <!-- 										type="button"> -->
                <!-- 									<span class="fas fa-align-justify"></span> -->
                <!-- 								</button> -->
                <!-- 							</div> -->

                <!-- 							<div class="mobile__nav__row--table-cell visible-xs visible-sm mobile__nav__row--seperator"> -->

                <!-- 							</div> -->

                <%-- 							<c:if test="${empty hideHeaderLinks}"> --%>
                <%-- 								<ycommerce:testId code="header_StoreFinder_link"> --%>
                <!-- <!-- 									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator"> -->
                <%-- <%-- 										<c:url value="/store-finder" var="storeFinderUrl"/> --%>
                <%-- <%-- 										<a href="${fn:escapeXml(storeFinderUrl)}" class="mobile__nav__row--btn mobile__nav__row--btn-location btn"> --%>
                <!-- <!-- 											<span class="fas fa-map-marker-alt"></span> -->
                <!-- <!-- 										</a> -->
                <!-- <!-- 									</div> -->
                <%-- 								</ycommerce:testId> --%>
                <%-- 							</c:if> --%>


                <!-- <div class="mobile__nav__row--table-cell visible-xs visible-sm mobile__nav__row--seperator"> -->
                <%-- 							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer   hidden-md hidden-lg"> --%>
                <%-- 								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" /> --%>
                <%-- 							</cms:pageSlot> --%>
                <!-- 							</div> -->


                <!-- 						</div> -->
                <!-- 					</div> -->
                <!-- 				</div> -->


                <div class="row desktop__nav hidden">
                    <div class="nav__left col-xs-12 col-sm-6">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs visible-sm mobile-menu">
                                <button class="btn js-toggle-sm-navigation" type="button">
                                    <img src="${fn:escapeXml(themeResourcePath)}/images/menu.svg" />
                                </button>
                            </div>
                            <div class="col-sm-10">
                                <div class="site-search">
                                    <cms:pageSlot position="SearchBox" var="component">
                                        <cms:component component="${component}" element="div" />
                                    </cms:pageSlot>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav__right col-xs-6 col-xs-6 hidden-xs">
                        <ul class="nav__links nav__links--shop_info">
                            <li>
                                <c:if test="${empty hideHeaderLinks}">
                                    <ycommerce:testId code="header_StoreFinder_link">
                                        <!-- <div class="nav-location hidden-xs"> -->
                                        <%-- 										<c:url value="/store-finder" var="storeFinderUrl"/> --%>
                                        <%-- 										<a href="${fn:escapeXml(storeFinderUrl)}" class="btn"> --%>
                                        <!-- <span class="fas fa-map-marker-alt"></span> -->
                                        <!-- </a> -->
                                        <!-- </div> -->
                                    </ycommerce:testId>
                                </c:if>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <a id="skiptonavigation"></a>

    </header>
    <nav:topNavigation />
</div>


<cms:pageSlot position="BottomHeaderSlot" var="component" element="div" class="container-fluid">
    <cms:component component="${component}" />
</cms:pageSlot>
