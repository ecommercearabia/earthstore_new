<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="defaultAddress" required="false" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<spring:url var="addressBookURL" value="{contextPath}/my-account/address-book" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>


<div class="cont_box deliveryAddress">

	<div class="right-side">
	<div class="headline">
	<div class="left-side">
	<div class="fal fa-truck"></div></div>

	<div class="head_myaccount">
	<spring:theme code="delivery.address"/>
	</div>
	
	
	</div>
	<div class="body">
		<div class="myaccount_title"><spring:theme code="address.addressName"/></div> <div class="myaccount_subtitle"> ${defaultAddress.addressName}<br></div>
		
		
		<div class="myaccount_title"><spring:theme code="address.line1"/></div> <div class="myaccount_subtitle">${defaultAddress.line1}<br></div>
	</div>

</div>
<a href="${addressBookURL}" class="link_box_cont"></a>
</div>
