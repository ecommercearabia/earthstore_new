<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section">
    <div class="account-section-header no-border"><spring:theme code="text.account.profile.resetPassword" /></div>
    <div class="row Reset-password">
        <div class="container-lg col-md-8 col-md-offset-2">
            <div class="account-section-content">
                <div class="account-section-form">
                    <form:form method="post" modelAttribute="updatePwdForm">
                        <div class="passwordupdatePwd">
                        <div class="form-group">
                            <formElement:formPasswordBox idKey="password" labelKey="updatePwd.pwd" path="pwd"
                                                         inputCSS="form-control password-strength" mandatory="true" />
                        </div>
                        </div>
                        <div class="passwordcheckPwd">
                        <div class="form-group">
                            <formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd"
                                                         path="checkPwd" inputCSS="form-control" mandatory="true" />
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 col-sm-offset-2 col-md-offset-6 col-lg-offset-6 accountButtons">
                              <div class="accountActions">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <spring:theme code="updatePwd.submit" />
                                </button>
                              </div>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 accountButtons">
                                <div class="accountActions">
                                <button type="button" class="btn btn-default btn-block backToHome">
                                    <spring:theme code="text.button.cancel" />
                                </button>
                                </div>
                            </div>
                        </div>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>