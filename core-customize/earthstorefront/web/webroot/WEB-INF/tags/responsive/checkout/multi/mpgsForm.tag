<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="supportedPaymentData" required="true"
	type="com.earth.earthpayment.entry.PaymentRequestData"%>

<!-- 	src="https://ap-gateway.mastercard.com/checkout/version/57/checkout.js" -->
<div class="checkout-indent PaymentDiv">
<script src="${supportedPaymentData.paymentProviderData.scriptSrc}"
	data-error="errorCallback" data-cancel="cancelCallback"></script>
<script type="text/javascript">
	function errorCallback(error) {
		console.log(JSON.stringify(error));
		console.log(error);
	}
	function cancelCallback() {
		console.log('Payment cancelled');
	}
	Checkout
			.configure({
				session : {
					id : '${supportedPaymentData.scriptSrc}'
				},
				interaction : {
					merchant : {
						name : '${supportedPaymentData.paymentProviderData.merchantName}',
						address : {
							line1 : '${supportedPaymentData.paymentProviderData.merchantLine1}',
							line2 : '${supportedPaymentData.paymentProviderData.merchantLine1}'
						}
					},
					displayControl: {
			            billingAddress  : 'HIDE',
			            customerEmail   : 'HIDE',
			            orderSummary    : 'HIDE',
			            shipping        : 'HIDE'
			   }
				}
			});
</script>

<spring:theme code="checkout.payment.mpgs.btn" var="payBtn"  />
<input type="button" class="btn btn-primary btn-block checkout-next" value="${payBtn}"
	onclick="Checkout.showLightbox();" />
</div>