<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<c:set value="" var="unitFactorRangeData"></c:set><c:set value="hidden" var="QTY"></c:set>
<c:set var="qtyMinus"
       value="${not empty product.unitFactorRangeData ? product.unitFactorRangeData[0].quantity : '1' }"/>

<c:if test="${not empty product.unitFactorRangeData}">
    <c:set value="" var="QTY"></c:set>
    <c:set value="hidden" var="unitFactorRangeData"></c:set> </c:if>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:url var="addToCartToPickupInStoreUrl" value="/store-pickup/cart/add"/>

<div class="display-details">
    <div class="store-tabs js-pickup-tabs">
        <div class="tabhead" aria-label="<spring:theme code="storeDetails.title"/>">
            <span class="fas fa-info-circle"></span>

        </div>
        <div class="tabbody">
            <div class="store-image">
                <div class="js-store-image"></div>
                <div class="distance js-store-formattedDistance"></div>
            </div>
            <div class="store-info">
                <div class="name js-store-displayName"></div>
                <div class="address">
                    <div class="js-store-line1"></div>
                    <div class="js-store-line2"></div>
                    <div class="js-store-town"></div>
                </div>
            </div>
        </div>

        <div class="tabhead js-pickup-map-tab" aria-label="<spring:theme code="storeDetails.map.link"/>">
            <span class="fas fa-map-marker-alt"></span>
        </div>
        <div class="tabbody">
            <div class="pickup-map js-map-canvas"></div>
        </div>

        <div class="tabhead" aria-label="<spring:theme code="storeDetails.table.opening"/>">
            <span class="fal fa-clock"></span>
        </div>
        <div class="tabbody">
            <div class="store-openings">
                <div class="title"><spring:theme code="storeDetails.table.opening"/></div>
                <dl class="dl-horizontal js-store-openings"></dl>
            </div>
        </div>

    </div>

    <div class="pickup-product">
        <div class="variants js-pickup-product-variants"></div>
        <div class="thumb"></div>
        <div class="pickup-product-info">
            <div class="name js-pickup-product-info "></div>
            <div class="price">


                <c:choose>
                    <c:when test="${not empty product.unitFactorRangeData}">


                        <c:choose>
                            <c:when test="${not empty product.unitFactorRangeData[0].discountUnitPrice}">


                                    <span class="price">

                                           <format:fromPrice
                                                   priceData="${product.unitFactorRangeData[0].discountUnitPrice.discountPrice}"/>


                                   </span>
                                <span class="scratched">
                                        <span class="Pricescratched">  <format:fromPrice
                                                priceData="${product.unitFactorRangeData[0].price}"/></span>
                                    <span
                                            class="line_dis "></span></span>


                            </c:when>
                            <c:otherwise>



                                     <span class="price">
                                    <format:fromPrice priceData="${product.unitFactorRangeData[0].price}"/>
                                            <span class="scratched hidden">
                                                <span class="Pricescratched"></span>
                                    <span
                                            class="line_dis hidden "></span></span>

                                    </span>

                            </c:otherwise>

                        </c:choose>


                    </c:when>
                    <c:otherwise>
                        <div class="js-pickup-product-price"></div>
                    </c:otherwise>
                </c:choose>


            </div>


        </div>
        <div class="action">


            <span class="OutOfStock hidden"><i class="fas fa-times-circle"></i><spring:theme
                    code="product.variants.out.of.stock"/></span>


            <form:form data-id="add_to_cart_storepickup_form" class="add_to_cart_storepickup_form"
                       action="${addToCartToPickupInStoreUrl}" method="post">
                <input type="hidden" class="js-store-id">
                <input type="hidden" class="js-store-productcode" name="productCodePost" value=""/>


                <c:if test="${not empty product.unitFactorRangeData}">
  										                   <span class="${QTY}">
    <span class="SelectitemUnit"> <span class="valueUnit">${product.unitFactorRangeData[0].formatedValue}</span> <i
            class="far fa-chevron-down arrowUnitVal"></i></span>
    <div class="SelectproductQtyItem hidden">  
<!--    <h4 class="closeBox"> Select Quantity <i class="fal fa-times closeBoxIcon"></i></h4> -->
    <c:set var="firstActive" value=""/>
      <c:forEach items="${product.unitFactorRangeData}" var="item" varStatus="loop">
          <c:if test="${loop.index eq 0 }">
              <c:set var="firstActive" value="active"/>
          </c:if>
          <div class="itemSelectQty ${firstActive }" data-value="${item.formatedValue}" data-Qty="${item.quantity}"
               data-stock="${item.inStock}" data-price='<format:fromPrice    priceData="${item.price}"/>'
               data-priceAfterSaving='<format:fromPrice priceData="${item.priceAfterSaving}"/>'
               data-discountedPrice='<format:fromPrice priceData="${item.discountUnitPrice.discountPrice}"/>'
               data-saving='<format:fromPrice   priceData="${item.discountUnitPrice.saving}"/>'
               data-percentage='${item.discountUnitPrice.percentage}'>${item.formatedValue}
          </div>
          <c:set var="firstActive" value=""/>
      </c:forEach>
    </div>
    </span>
                </c:if>


                <div class="qty-selector js-qty-selector ${unitFactorRangeData}">
                    <div class="input-group">
					<span class="input-group-btn">
						<button class="btn js-qty-selector-minus" type="button"><span class="fal fa-minus"
                                                                                      aria-hidden="true"></span></button>
					</span>
                        <input type="hidden" value="${qtyMinus}" id="baseValueWeight">
                        <input type="text" class="form-control js-qty-selector-input pickupInput" value="${qtyMinus}"
                               name="hiddenPickupQty">
                        <span class="input-group-btn">
						<button class="btn js-qty-selector-plus" type="button"><span class="fal fa-plus"
                                                                                     aria-hidden="true"></span></button>
					</span>
                    </div>
                </div>

                <button class="btn btn-primary js-add-to-cart-for-pickup-popup bt_pickup-popup" type="submit"><span
                        class="iconBtnCart"><i class="fas fa-cart-arrow-down"></i></span>
                    <span class="btnCartText"><spring:theme code="add.btn.cart"/></span></button>
            </form:form>
        </div>
    </div>
</div>

