<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <div class="title"><spring:theme code="newsletter.subscribe.label"/><span class="far fa-plus hidden-lg hidden-md"></span></div> --%>


<!-- <div class="form-group FormSubscribe footer__nav--links hidden"> -->
<!-- <input id="email" name="email" class="form-control EmailSubscribe"/> -->
<!-- <button class="btn-primary btnSubscribe"> -->
<!-- <i class="fal fa-arrow-right"></i> -->
<!-- </button> -->
<!-- </div> -->


<!-- Begin Mailchimp Signup Form -->
<div class="title"><spring:theme code="newsletter.subscribe.label"/><span class="far fa-plus hidden-lg hidden-md"></span></div>
<div id="mc_embed_signup" class="form-group FormSubscribe footer__nav--links"> 
<form action="https://earthcoop.us6.list-manage.com/subscribe/post-json?u=ba62555babbea452d72cc4f71&amp;id=d4b463d2da&c=?" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" class="FormSubscribe">
	<label for="mce-EMAIL"><spring:theme code="newsletter.subscribe.label"/></label>
	<input type="email" value="" name="EMAIL" class="email form-control EmailSubscribe" id="mce-EMAIL" placeholder="<spring:theme code='newsletter.subscribe.email'/>" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px; display:none;" aria-hidden="true"><input type="text" name="b_ba62555babbea452d72cc4f71_d4b463d2da" tabindex="-1" value=""></div>
    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button btn-primary btnSubscribe"><span class="arrowSub"><i class="fal fa-arrow-right "></i></span><span class="loadingSub"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></span></button>
    </div>
    <div class="msgFormSub colorSubDesgin hidden">
    
    </div>
    
</form>

</div>

<!--End mc_embed_signup-->