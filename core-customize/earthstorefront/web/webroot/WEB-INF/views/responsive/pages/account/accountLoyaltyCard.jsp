<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row">
		<div class="container-lg col-md-10 col-lg-8 col-md-offset-1">
		
<c:if test="${ not empty pageTitle}">
<c:set var="titlePage" value="${fn:split(pageTitle, '|')}" />
</c:if>
<c:choose>

<c:when test="${loyaltyEnabled}">
<span class="titleotherPages">
${titlePage[0]}
</span>
<br/>






<span class="loyaltypoint"><b><spring:theme code='text.account.loyaltycard.availablepoints'/> <span class="green_color"> ${loyaltyCustomerInfo.balance.balancePoints}</span><span class="sepratechar">|</span></b></span>
<span class="loyaltyamount"><b><spring:theme code='text.account.loyaltycard.availableamount'/> <span class="green_color">  ${loyaltyCustomerInfo.balance.balanceAmount.formattedValue}</span></b></span>

<%-- <fmt:formatDate value="${order.created}" dateStyle="short" timeStyle="medium" type="time" pattern = "hh:mm" /> <br/> --%>
<%-- 						<fmt:formatDate value="${order.created}" dateStyle="long" timeStyle="long" type="date" pattern = "EEEE, dd MMMM YYYY"/> <br/> --%>

<div class="account-overview-table loyaltyTable">
				<table class="orderhistory-list-table responsive-table">
					<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
					 		<th id="header6" class="head_account_cridit"><spring:theme code='text.account.storecredit.ordernumber'/></th>
							<th id="header2" class="head_account_cridit"><spring:theme code='text.account.loyalty.type'/></th>
							<th id="header1" class="head_account_cridit"><spring:theme code='text.account.loyalty.date'/></th>
	                        <th id="header3" class="head_account_cridit"><spring:theme code='text.account.loyalty.initialAmount'/></th>
	                        <th id="header4" class="head_account_cridit"><spring:theme code='text.account.loyalty.finalAmount'/></th>
	                        <th id="header5" class="head_account_cridit"><spring:theme code='text.account.loyalty.expire'/></th>
					</tr>
					<c:forEach items="${loyaltyCustomerInfo.histories}" var="loyaltyHistory">
						<tr class="responsive-table-item">
							<ycommerce:testId code="orderHistoryItem_orderDetails_link">
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.ordernumber'/></td>
								<td class="responsive-table-cell responsive-table-cell-bold">
								
								<c:url value="/my-account/order/${ycommerce:encodeUrl(loyaltyHistory.orderNumber)}" var="orderURL" />
																		<a href="${orderURL}" class="red_color responsive-table-link ">
										${fn:escapeXml(loyaltyHistory.orderNumber)}
									</a>
								</td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.type'/></td>
								<td class="responsive-table-cell">
									${loyaltyHistory.type}
								</td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.date'/></td>
								<td class="responsive-table-cell">
									<fmt:formatDate value="${loyaltyHistory.created}" dateStyle="long" timeStyle="long" type="date" pattern = "MMM dd, YYYY"/>
								
								</td>
								
								
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.initialAmount'/></td>
								<td class="responsive-table-cell">
									${loyaltyHistory.initialAmount.formattedValue}
								</td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.finalAmount'/></td>
								<td class="responsive-table-cell">
									${loyaltyHistory.finalAmount.formattedValue}
								</td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.date'/></td>
								<td class="responsive-table-cell">
									<fmt:formatDate value="${loyaltyHistory.expireDate}" dateStyle="long" timeStyle="long" type="date" pattern = "MMM dd, YYYY"/>
								
								</td>
								
							</ycommerce:testId>
						</tr>
					</c:forEach>
				</table>
            </div>
 </c:when>
 
 <c:otherwise>
		
			<c:choose>
				<c:when test="${needUpdateProfile}">
				<c:url var="updateProfileURL" value="/my-account/update-profile" scope="page"/>
				
					<p><spring:theme code="msg.loyalty.needupdate.msg"/></p>
								<a href="${ updateProfileURL}">update profile</a>
				
				</c:when>
			
			<c:otherwise>
				<c:url var="registerUserURL" value="/my-account/loyalty-card/reg" scope="page"/>
 		<form action="${registerUserURL}" id="loyaltyForm" method="GET">
			<p><spring:theme code="msg.loyalty.msg"/></p>
			<button type="submit" class="btn btn-primary btn-block re-order" id="reorderButton">
				<spring:theme code="text.account.loyalty.register"/>
			</button>
				</form>
			</c:otherwise>
			
			</c:choose>
			
	
 
 
 
 
 
 </c:otherwise>
            
  </c:choose>

</div>
</div>


