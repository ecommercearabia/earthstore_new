<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

    <cms:pageSlot position="Section1" var="feature" element="div"   class="row fixMar bannarImagepadding firstBanner">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section2" var="feature" element="div"   class="row fixMar bannarImagepaddingdowndots categotiesSectionCont">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>


    <cms:pageSlot position="Section3" var="feature" element="div"   class="row fixMar bannarImagepaddingdowndots">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>


    <cms:pageSlot position="Section4" var="feature" element="div"   class="row fixMar bannarImagepadding animationBanner">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section5" var="feature" element="div"   class="row fixMar bannarImagecar bannarImagepaddingdowndots">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>


    <div class=" row FirstbannerSection bannerImgSection">

        <cms:pageSlot position="Section6A" var="feature" element="div"   class="col-md-4 col-sm-12 col-xs-12">
            <cms:component component="${feature}" element="div"/>
        </cms:pageSlot>

        <div class="col-md-4 col-sm-12 col-xs-12">
            <cms:pageSlot position="Section6B1" var="feature" element="div"   class="row">
                <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
            </cms:pageSlot>


            <cms:pageSlot position="Section6B2" var="feature" element="div"   class="row">
                <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12 bottomImgSliderB"/>
            </cms:pageSlot>
        </div>

        <cms:pageSlot position="Section6C" var="feature" element="div"   class="col-md-4 col-sm-12 col-xs-12">
            <cms:component component="${feature}" element="div" />
        </cms:pageSlot>

    </div>

    <cms:pageSlot position="Section7" var="feature" element="div"   class="row fixMar cardivmobile bannarImagepaddingdowndots">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>


    <cms:pageSlot position="Section8" var="feature" element="div"   class="row fixMar youCon">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>

</template:page>

