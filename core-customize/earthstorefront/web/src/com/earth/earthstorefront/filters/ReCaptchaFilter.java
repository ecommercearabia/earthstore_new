/**
 *
 */
package com.earth.earthstorefront.filters;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.filter.OncePerRequestFilter;

import com.earth.earthstorefront.filters.exception.ReCaptchaFailedException;

import atg.taglib.json.util.JSONException;
import atg.taglib.json.util.JSONObject;


/**
 * @author monzer
 *
 */
public class ReCaptchaFilter extends OncePerRequestFilter
{

	private static final Logger LOG = Logger.getLogger(ReCaptchaFilter.class);

	private static final String RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
	private static final String RECAPTCHA_SITE_KEY_PROPERTY = "recaptcha.publickey";
	private static final String RECAPTCHA_SECRET_KEY_PROPERTY = "recaptcha.privatekey";
	private static final String RECAPTCHA_RESPONSE_PARAM = "g-recaptcha-response";

	private SiteConfigService siteConfigService;
	private BaseStoreService baseStoreService;
	private BruteForceAttackCounter bruteForceAttackCounter;

	private AuthenticationFailureHandler authenticationFailureHandler;

	@Override
	protected void doFilterInternal(final HttpServletRequest req, final HttpServletResponse res, final FilterChain filterChain)
			throws ServletException, IOException
	{

		final HttpServletRequest request = req;
		final HttpServletResponse response = res;

		if (!isCaptchaEnabledForCurrentStore())
		{
			filterChain.doFilter(request, response);
			return;
		}

		final String username = request.getParameter("j_username");
		if (StringUtils.isNotBlank(username) && bruteForceAttackCounter.getUserFailedLogins(username) < 5)
		{
			filterChain.doFilter(request, response);
			return;
		}

		if (request.getRequestURI().endsWith("/j_spring_security_check"))
		{
			final String captchaResponse = request.getParameter(RECAPTCHA_RESPONSE_PARAM);

			try
			{
				final boolean checked = StringUtils.isBlank(captchaResponse) ? false : checkAnswer(captchaResponse);
				if (!checked)
				{
					unsuccessfullAuthenticationAttepmtWithReCaptcha(request, response,
							new ReCaptchaFailedException("ReCaptcha Failed on login"));
					return;
				}
			}
			catch (final ReCaptchaFailedException e)
			{
				LOG.error(e.getMessage());
				unsuccessfullAuthenticationAttepmtWithReCaptcha(request, response, e);
				return;
			}
		}

		filterChain.doFilter(request, response);
	}

	/**
	 * @return the bruteForceAttackCounter
	 */
	public BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	/**
	 * @param bruteForceAttackCounter
	 *           the bruteForceAttackCounter to set
	 */
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	protected boolean checkAnswer(final String recaptchaResponse) throws ReCaptchaFailedException
	{
		final RequestConfig config = RequestConfig.custom().setConnectTimeout(5 * 1000).build();
		final HttpPost method = new HttpPost(RECAPTCHA_VERIFY_URL);

		final List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("secret", getCaptchaPrivateKey()));
		urlParameters.add(new BasicNameValuePair("response", recaptchaResponse));

		final HttpClientBuilder httpClientBuilder = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false));

		try (final CloseableHttpClient client = httpClientBuilder.setDefaultRequestConfig(config).build())
		{
			method.setEntity(new UrlEncodedFormEntity(urlParameters));
			final HttpResponse httpResponse = client.execute(method);

			if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
			{
				throw new ReCaptchaFailedException(
						"ReCaptcha Failed on login due to : " + httpResponse.getStatusLine().getStatusCode());
			}

			final JSONObject response = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
			return response.getBoolean("success");
		}
		catch (IOException | JSONException e)
		{
			throw new ReCaptchaFailedException("ReCaptcha Failed on login");
		}
		finally
		{
			method.releaseConnection();
		}
	}

	protected void unsuccessfullAuthenticationAttepmtWithReCaptcha(final HttpServletRequest request,
			final HttpServletResponse response, final AuthenticationException failed) throws IOException, ServletException
	{
		SecurityContextHolder.clearContext();
		getAuthenticationFailureHandler().onAuthenticationFailure(request, response, failed);
	}

	protected boolean isCaptchaEnabledForCurrentStore()
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return currentBaseStore != null && Boolean.TRUE.equals(currentBaseStore.getCaptchaCheckEnabled());
	}

	protected String getCaptchaPublicKey()
	{
		return baseStoreService.getCurrentBaseStore().getCaptchaPublicKey();
	}

	protected String getCaptchaPrivateKey()
	{
		return baseStoreService.getCurrentBaseStore().getCaptchaPrivateKey();
	}

	/**
	 * @return the siteConfigService
	 */
	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	/**
	 * @param siteConfigService
	 *           the siteConfigService to set
	 */
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the authenticationFailureHandler
	 */
	public AuthenticationFailureHandler getAuthenticationFailureHandler()
	{
		return authenticationFailureHandler;
	}

	/**
	 * @param authenticationFailureHandler
	 *           the authenticationFailureHandler to set
	 */
	public void setAuthenticationFailureHandler(final AuthenticationFailureHandler authenticationFailureHandler)
	{
		this.authenticationFailureHandler = authenticationFailureHandler;
	}


}
