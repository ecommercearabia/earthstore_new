/**
 *
 */
package com.earth.earthstorefront.filters;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.earth.earthcore.enums.ActionHistoryType;
import com.earth.earthcore.service.ActionHistoryService;


/**
 * @author monzer
 *
 */
public class ActionSourceHistoryFilter extends OncePerRequestFilter
{
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ActionSourceHistoryFilter.class);

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "actionHistoryService")
	private ActionHistoryService actionHistoryService;


	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		final ActionHistoryType type = checkRequestForActionHistoryConfigurations(currentSite, request);
		if (type != null)
		{
			switch (type)
			{
				case CART:
					final CartModel cartModel = cartService.getSessionCart();
					actionHistoryService.updateCartHistory(cartModel, SalesApplication.WEB, getPath(request), request.getMethod());
					break;
				case ADDRESS:
					saveAddressAction(request);
					break;
				case PROFILE:
					saveProfileAction(request);
					break;
			}
		}
		filterChain.doFilter(request, response);
	}

	/**
	 * @param request
	 */
	private void saveProfileAction(final HttpServletRequest request)
	{
		if (request == null)
		{
			return;
		}
		final UserModel currentUser = userService.getCurrentUser();
		if (userService.isAnonymousUser(currentUser))
		{
			return;
		}
		actionHistoryService.updateUserHistory(currentUser, SalesApplication.WEB, getPath(request), request.getMethod());
	}


	/**
	 * @param request
	 */
	private void saveAddressAction(final HttpServletRequest request)
	{
		if (request == null)
		{
			return;
		}
		final Map<String, String[]> parameterMap = request.getParameterMap();
		if (CollectionUtils.isEmpty(parameterMap) || !parameterMap.containsKey("addressId"))
		{
			return;
		}
		final String addressId = parameterMap.get("addressId")[0];
		final UserModel currentUser = userService.getCurrentUser();
		if (userService.isAnonymousUser(currentUser))
		{
			return;
		}
		if (CollectionUtils.isEmpty(currentUser.getAddresses()))
		{
			return;
		}
		final Optional<AddressModel> currentAddress = currentUser.getAddresses().stream()
				.filter(address -> address.getPk().getLongValueAsString().equals(addressId)).findFirst();
		if (currentAddress.isEmpty())
		{
			return;
		}

		actionHistoryService.updateAddressHistory(currentAddress.get(), SalesApplication.WEB, getPath(request),
				request.getMethod());
	}


	/**
	 * @param currentSite
	 * @param request
	 * @return
	 */
	private ActionHistoryType checkRequestForActionHistoryConfigurations(final CMSSiteModel cmsSite,
			final HttpServletRequest request)
	{
		if (cmsSite == null || cmsSite.getCartActionHistoryConfigurations() == null
				|| cmsSite.getProfileActionHistoryConfigurations() == null || cmsSite.getAddressActionHistoryConfigurations() == null)
		{
			return null;
		}
		if (request == null || !request.getMethod().equalsIgnoreCase("post"))
		{
			LOG.info("Request is null or not a post request");
			return null;
		}

		final String profileRegex = cmsSite.getProfileActionHistoryConfigurations().getStorefrontPathRegex();
		final String addresseRegex = cmsSite.getProfileActionHistoryConfigurations().getStorefrontPathRegex();
		final String cartRegex = cmsSite.getProfileActionHistoryConfigurations().getStorefrontPathRegex();

		if (checkRegex(request, cmsSite.getCartActionHistoryConfigurations().getStorefrontPathRegex(),
				cmsSite.getCartActionHistoryConfigurations().isEnabled()))
		{
			return ActionHistoryType.CART;
		}
		if (checkRegex(request, cmsSite.getAddressActionHistoryConfigurations().getStorefrontPathRegex(),
				cmsSite.getAddressActionHistoryConfigurations().isEnabled()))
		{
			return ActionHistoryType.ADDRESS;
		}
		if (checkRegex(request, cmsSite.getProfileActionHistoryConfigurations().getStorefrontPathRegex(),
				cmsSite.getProfileActionHistoryConfigurations().isEnabled()))
		{
			return ActionHistoryType.PROFILE;
		}
		return null;
	}


	/**
	 * @param request
	 * @param pathRegex
	 */
	private boolean checkRegex(final HttpServletRequest request, final String pathRegex, final boolean enabled)
	{
		final Set<String> protectionExcludeUrlSet = org.springframework.util.StringUtils.commaDelimitedListToSet(pathRegex);
		if (CollectionUtils.isEmpty(protectionExcludeUrlSet) || request == null)
		{
			LOG.info("Action listener patterns are empty!");
			return false;
		}
		LOG.info("Action Request Path URL {}", request.getRequestURI());
		for (final String pattern : protectionExcludeUrlSet)
		{
			if (request.getRequestURI() != null && request.getRequestURI().matches(pattern))
			{
				LOG.info("Action operation occured for this URL {}", request.getRequestURI());
				return true && enabled;
			}
		}
		return false;
	}

	protected String getPath(final HttpServletRequest request)
	{
		return StringUtils.defaultString(request.getRequestURI());
	}

}
