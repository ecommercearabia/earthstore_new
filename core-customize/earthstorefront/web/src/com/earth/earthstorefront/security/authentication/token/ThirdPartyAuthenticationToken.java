/**
 *
 */
package com.earth.earthstorefront.security.authentication.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * @author monzer
 *
 */
public class ThirdPartyAuthenticationToken extends AbstractAuthenticationToken
{

	private final Object principal;
	private final Object token;
	private final Object providerType;
	private final Object callbackUrl;

	/**
	 * @param authorities
	 * @param principle
	 * @param token
	 * @param providerType
	 */
	public ThirdPartyAuthenticationToken(final Object principal, final Object token,
			final Object providerType, final Object callbackUrl)
	{
		super(null);
		this.principal = principal;
		this.token = token;
		this.providerType = providerType;
		this.callbackUrl = callbackUrl;
		setAuthenticated(false);
	}

	@Override
	public Object getCredentials()
	{
		return this.token;
	}

	@Override
	public Object getDetails()
	{
		return this.providerType;
	}

	@Override
	public Object getPrincipal()
	{
		return this.principal;
	}

	@Override
	public void setAuthenticated(final boolean isAuthenticated) throws IllegalArgumentException
	{
		if (isAuthenticated)
		{
			throw new IllegalArgumentException(
					"Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
		}

		super.setAuthenticated(false);
	}

	@Override
	public String getName()
	{
		return String.valueOf(principal);
	}

	public Object getAuthToken()
	{
		return this.token;
	}

	public Object getProviderType()
	{
		return this.providerType;
	}

	public Object getCallbackUrl()
	{
		return callbackUrl;
	}

}
