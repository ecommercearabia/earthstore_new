package com.earth.earthstorefront.form.validation;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthotp.context.OTPContext;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthstorefront.form.OTPForm;



/**
 * The Class OTPValidator.
 *
 * @author monzer
 */
@Component("otpValidator")
public class OTPValidator implements Validator
{

	private static final Logger LOG = Logger.getLogger(OTPValidator.class);

	/** The mobile phone service. */
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;


	/*
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateSend(otpForm, errors);
		validateOTPCode(otpForm, errors);
	}

	public void validateSend(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateStandardFields(otpForm, errors);
		validatePhoneNumber(otpForm, errors);
	}

	protected void validatePhoneNumber(final OTPForm form, final Errors errors)
	{
		if (StringUtils.isNotBlank(form.getMobileNumber()))
		{
			final Optional<String> phoneNumberByIsoCode = getMobilePhoneService()
					.validateAndNormalizePhoneNumberByIsoCode(form.getMobileCountry(), form.getMobileNumber());
			if (phoneNumberByIsoCode.isEmpty())
			{
				errors.rejectValue("mobileNumber", "test.found");
			}
		}
	}

	protected void validateOTPCode(final OTPForm otpForm, final Errors errors)
	{
		if (StringUtils.isEmpty(otpForm.getOtpCode()))
		{
			errors.rejectValue("otpCode", "otp.otpCode.invalid");
		}
		else
		{
			try
			{
				if (!otpContext.verifyCodeByCurrentSite(otpForm.getMobileCountry(), otpForm.getMobileNumber(), otpForm.getOtpCode()))
				{
					errors.rejectValue("otpCode", "otp.otpCode.format.invalid");
				}

			}
			catch (final OTPException e)
			{
				errors.rejectValue("otpCode", "otp.otpCode.format.invalid");
			}
		}

	}

	/*
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return OTPForm.class.equals(aClass);
	}

	/**
	 * Validate standard fields.
	 *
	 * @param otpForm
	 *           the otp form
	 * @param errors
	 *           the errors
	 */
	protected void validateStandardFields(final OTPForm otpForm, final Errors errors)
	{
		if (StringUtils.isEmpty(otpForm.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "otp.mobileCountry.format.invalid");
		}

		if (StringUtils.isEmpty(otpForm.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
		}
		try
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(otpForm.getMobileCountry(), otpForm.getMobileNumber());
			if (normalizedPhoneNumber.isPresent())
			{
				otpForm.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
			}
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
		}
	}

	/**
	 * @return the mobilePhoneService
	 */
	public MobilePhoneService getMobilePhoneService()
	{
		return mobilePhoneService;
	}

	/**
	 * @return the otpContext
	 */
	public OTPContext getOtpContext()
	{
		return otpContext;
	}

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

}
