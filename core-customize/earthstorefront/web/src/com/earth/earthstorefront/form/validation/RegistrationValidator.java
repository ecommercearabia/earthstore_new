package com.earth.earthstorefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.enums.MaritalStatus;
import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.service.NationalityService;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthstorefront.form.RegisterForm;




/**
 * Validates registration forms.
 *
 * @author monzer
 *
 */
@Component("customRegistrationValidator")
public class RegistrationValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(RegistrationValidator.class);

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");


	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegisterForm registerForm = (RegisterForm) object;
		final String titleCode = registerForm.getTitleCode();
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();
		final boolean termsCheck = registerForm.isTermsCheck();
		final String nationality = registerForm.getNationality();
		final String nationalityId = registerForm.getNationalityId();
		final String birthDate = registerForm.getBirthDate();

		validateMobileAttributes(registerForm, errors);
		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);
		if (registerForm.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityId);
		}
		validateBirthDate(errors, currentSite, birthDate);
		validateNationality(errors, currentSite, nationality);
		validateTermsAndConditions(errors, termsCheck);

		if (currentSite != null && currentSite.isCustomerMaritalStatusEnabled() && currentSite.isCustomerMaritalStatusRequired())
		{
			validateMaritalStatus(registerForm.getMaritalStatusCode(), errors);
		}
	}

	/**
	 * @param mobileCountry
	 * @param mobileNumber
	 * @param errors
	 */
	private void validateMobileAttributes(final RegisterForm registerForm, final Errors errors)
	{
		if (StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(registerForm.getMobileNumber()) || !validateMobileNumber(registerForm.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerForm.getMobileCountry(), registerForm.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				registerForm.setMobileNumber(normalizedPhoneNumber.get());

			}
			else
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			}
		}

	}

	/**
	 * @param maritalStatusCode
	 * @param errors
	 * @param currentSite
	 */
	private void validateMaritalStatus(final String maritalStatusCode, final Errors errors)
	{
		if (StringUtils.isBlank(maritalStatusCode))
		{
			errors.rejectValue("maritalStatusCode", "register.maritalStatus.empty");
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatusCode);
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("maritalStatusCode", "register.maritalStatus.invalid");
		}
	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue("birthDate", "register.birthofdate.invalid");
		}

	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue("nationalityId", "register.nationalityid.invalid");
		}

	}

	/**
	 * @param errors
	 * @param cmsSiteModel
	 * @param nationality
	 */
	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "profile.nationality.invalid");
			}
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 * @param checkPwd
	 */
	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 */
	protected void validatePassword(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd) || StringUtils.length(pwd) < 6 || StringUtils.length(pwd) > 255)
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
	}

	/**
	 *
	 * @param errors
	 * @param email
	 */
	protected void validateEmail(final Errors errors, final String email)
	{
		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else
		{
			try
			{
				if (userFacade.getUserUID(email) != null)
				{
					errors.rejectValue("email", "registration.error.account.exists.title");
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.info("Customer is not existed this is valid where UID=[" + email + "]");

			}
		}
	}

	/**
	 *
	 * @param errors
	 * @param name
	 * @param propertyName
	 * @param property
	 */
	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name) || StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 *
	 * @param errors
	 * @param titleCode
	 */
	protected void validateTitleCode(final Errors errors, final String titleCode)
	{
		if (StringUtils.isEmpty(titleCode) || StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
	}

	/**
	 *
	 * @param number
	 * @return true or false
	 */
	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

	/**
	 *
	 * @param email
	 * @return true or false
	 */
	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = Pattern.compile(configurationService.getConfiguration().getString(WebConstants.EMAIL_REGEX))
				.matcher(email);
		return matcher.matches();
	}

	/**
	 * @return
	 */
	private String getEmailRestriction()
	{
		final String siteID = cmsSiteService.getCurrentSite() == null ? "" : cmsSiteService.getCurrentSite().getUid();
		final String emailRestriction = configurationService.getConfiguration()
				.getString("website.registration.email.restriction." + siteID);
		if (StringUtils.isBlank(emailRestriction))
		{
			return null;
		}

		return emailRestriction.replace(";", "|");
	}

	/**
	 *
	 * @param errors
	 * @param termsCheck
	 */
	protected void validateTermsAndConditions(final Errors errors, final boolean termsCheck)
	{
		if (!termsCheck)
		{
			errors.rejectValue("termsCheck", "register.terms.not.accepted");
		}
	}
}
