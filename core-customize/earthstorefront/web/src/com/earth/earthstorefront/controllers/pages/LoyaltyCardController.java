/**
 *
 */
package com.earth.earthstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.earth.earthloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.earth.earthfacades.user.customer.facade.CustomCustomerFacade;


/**
 * @author mnasro
 *
 */
@Controller
@RequestMapping(value = "/my-account/loyalty-card")
public class LoyaltyCardController extends AbstractPageController
{
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";


	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "loyaltyPaymentFacade")
	private LoyaltyPaymentFacade loyaltyPaymentFacade;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;



	private static final String LOYALTY_CARD_VIEW = "/my-account/loyalty-card";


	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getLoyaltyPage(final Model model) throws CMSItemNotFoundException
	{
		final boolean loyaltyEnabled = getLoyaltyPaymentFacade().isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer();
		model.addAttribute("loyaltyEnabled", loyaltyEnabled);
		final CustomerData currentCustomer = getCustomCustomerFacade().getCurrentCustomer();

		storeCmsPageInModel(model, getContentPageForLabelOrId(LOYALTY_CARD_VIEW));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LOYALTY_CARD_VIEW));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.loyaltycard"));

		if (StringUtils.isBlank(currentCustomer.getBirthDate()) || StringUtils.isBlank(currentCustomer.getMobileNumber()))
		{
			model.addAttribute("needUpdateProfile", true);
			return getViewForPage(model);
		}

		if (!loyaltyEnabled)
		{
			return getViewForPage(model);
		}

		final Optional<LoyaltyCustomerInfoData> loyaltyCustomerInfo = getLoyaltyPaymentFacade()
				.getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(null);
		model.addAttribute("loyaltyCustomerInfo", loyaltyCustomerInfo.isPresent() ? loyaltyCustomerInfo.get() : null);

		return getViewForPage(model);

	}

	@RequestMapping(path = "/reg", method = RequestMethod.GET)
	@RequireHardLogIn
	public String register(final Model model) throws CMSItemNotFoundException
	{
		getCustomCustomerFacade().registerCustomerInLoyaltyByCurrentCustomer();
		return REDIRECT_PREFIX + LOYALTY_CARD_VIEW;

	}

	/**
	 * @return the loyaltyPaymentFacade
	 */
	protected LoyaltyPaymentFacade getLoyaltyPaymentFacade()
	{
		return loyaltyPaymentFacade;
	}

	/**
	 * @param loyaltyPaymentFacade
	 *           the loyaltyPaymentFacade to set
	 */
	protected void setLoyaltyPaymentFacade(final LoyaltyPaymentFacade loyaltyPaymentFacade)
	{
		this.loyaltyPaymentFacade = loyaltyPaymentFacade;
	}

	/**
	 * @return the customCustomerFacade
	 */
	protected CustomCustomerFacade getCustomCustomerFacade()
	{
		return customCustomerFacade;
	}

	/**
	 * @param customCustomerFacade
	 *           the customCustomerFacade to set
	 */
	protected void setCustomCustomerFacade(final CustomCustomerFacade customCustomerFacade)
	{
		this.customCustomerFacade = customCustomerFacade;
	}


}
