/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.controllers.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.earth.earthcomponents.model.CategoryCarouselComponentModel;
import com.earth.earthstorefront.controllers.ControllerConstants;


/**
 * Controller for CMS CategoryCarouselComponent.
 */
@Controller("CategoryCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CategoryCarouselComponent)
public class CategoryCarouselComponentController extends AbstractAcceleratorCMSComponentController<CategoryCarouselComponentModel>
{

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	/**
	 * @return the categoryConverter
	 */
	public Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CategoryCarouselComponentModel component)
	{
		model.addAttribute("visible", component.getVisible());

		if (component.getVisible())
		{
			final List<CategoryData> categoriesData = new ArrayList<>();

			model.addAttribute("title", component.getTitle());
			model.addAttribute("content", component.getContent());
			model.addAttribute("link", component.getLink() != null && component.getLink().getVisible() ? component.getLink() : null);

			if (component.getReturnValueType() == null)
			{
				return;
			}
			switch (component.getReturnValueType())
			{
				case STATIC:
					categoriesData.addAll(collectLinkedCategories(component.getCategories()));
					break;
				case DYNAMIC:
					categoriesData.addAll(collectLinkedCategories(getSubcategories(component.getRootCategory())));
					break;
			}

			model.addAttribute("categories", categoriesData);
		}

	}

	/**
	 * @param categories
	 * @return
	 */
	private List<CategoryData> collectLinkedCategories(final List<CategoryModel> categories)
	{
		if (CollectionUtils.isEmpty(categories))
		{
			return null;
		}

		return getCategoryConverter().convertAll(categories);
	}

	/**
	 * @param rootCategory
	 * @return
	 */
	private List<CategoryModel> getSubcategories(final CategoryModel rootCategory)
	{
		if (rootCategory == null)
		{
			return null;
		}

		return new ArrayList(rootCategory.getAllSubcategories());
	}
}
