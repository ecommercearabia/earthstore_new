/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.earth.earthcomponents.model.BannerLinksListComponentModel;
import com.earth.earthstorefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */

@Controller("BannerLinksListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.BannerLinksListComponent)
public class BannerLinksListComponentController extends AbstractAcceleratorCMSComponentController<BannerLinksListComponentModel>
{


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BannerLinksListComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			model.addAttribute("bannerLinksList", component);
		}
	}




}
