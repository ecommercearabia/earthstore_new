/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomaddon.controllers;

/**
 */
public interface EarthcustomerinterestscustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
