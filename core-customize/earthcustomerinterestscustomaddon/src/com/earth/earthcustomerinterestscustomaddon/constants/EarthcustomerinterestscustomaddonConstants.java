/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomaddon.constants;

/**
 * Global class for all Earthcustomerinterestscustomaddon constants. You can add global constants for your extension into this class.
 */
public final class EarthcustomerinterestscustomaddonConstants extends GeneratedEarthcustomerinterestscustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthcustomerinterestscustomaddon";

	private EarthcustomerinterestscustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
