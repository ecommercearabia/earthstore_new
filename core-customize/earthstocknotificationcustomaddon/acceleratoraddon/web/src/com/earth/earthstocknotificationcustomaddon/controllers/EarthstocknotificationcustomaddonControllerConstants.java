/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstocknotificationcustomaddon.controllers;

/**
 */
public interface EarthstocknotificationcustomaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/earthstocknotificationcustomaddon/";

	interface Pages
	{
		String AddNotificationPage = ADDON_PREFIX + "pages/addStockNotification";
		String AddNotificationFromInterestPage = ADDON_PREFIX + "pages/addStockNotificationFromInterest";
	}
}
