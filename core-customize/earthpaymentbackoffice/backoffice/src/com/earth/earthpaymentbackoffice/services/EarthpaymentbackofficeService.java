/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthpaymentbackoffice.services;

/**
 * Hello World EarthpaymentbackofficeService
 */
public class EarthpaymentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
