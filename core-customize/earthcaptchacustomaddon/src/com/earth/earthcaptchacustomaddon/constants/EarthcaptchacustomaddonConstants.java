/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcaptchacustomaddon.constants;

/**
 * Global class for all Earthcaptchacustomaddon constants. You can add global constants for your extension into this class.
 */
public final class EarthcaptchacustomaddonConstants extends GeneratedEarthcaptchacustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthcaptchacustomaddon";

	private EarthcaptchacustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
