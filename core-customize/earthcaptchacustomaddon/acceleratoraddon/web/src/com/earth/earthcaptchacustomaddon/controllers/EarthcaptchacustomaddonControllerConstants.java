/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcaptchacustomaddon.controllers;

/**
 */
public interface EarthcaptchacustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
