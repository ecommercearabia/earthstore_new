/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponcustomocc.jalo;

import com.earth.earthcustomercouponcustomocc.constants.EarthcustomercouponcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustomercouponcustomoccManager extends GeneratedEarthcustomercouponcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustomercouponcustomoccManager.class.getName() );
	
	public static final EarthcustomercouponcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustomercouponcustomoccManager) em.getExtension(EarthcustomercouponcustomoccConstants.EXTENSIONNAME);
	}
	
}
