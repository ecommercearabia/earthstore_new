/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthorderselfservicecustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthorderselfservicecustomaddon.constants.EarthorderselfservicecustomaddonConstants;

@SuppressWarnings("PMD")
public class EarthorderselfservicecustomaddonManager extends GeneratedEarthorderselfservicecustomaddonManager
{
	public static final EarthorderselfservicecustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthorderselfservicecustomaddonManager) em.getExtension(EarthorderselfservicecustomaddonConstants.EXTENSIONNAME);
	}
	
}
