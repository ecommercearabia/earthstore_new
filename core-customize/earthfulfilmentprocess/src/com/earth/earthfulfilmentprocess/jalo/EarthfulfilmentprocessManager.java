/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthfulfilmentprocess.constants.EarthfulfilmentprocessConstants;

public class EarthfulfilmentprocessManager extends GeneratedEarthfulfilmentprocessManager
{
	public static final EarthfulfilmentprocessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthfulfilmentprocessManager) em.getExtension(EarthfulfilmentprocessConstants.EXTENSIONNAME);
	}
	
}
