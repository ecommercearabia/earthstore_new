/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservices.queues.impl;

import com.earth.earthcommercewebservices.queues.data.ProductExpressUpdateElementData;


/**
 * Queue for {@link com.earth.earthcommercewebservices.queues.data.ProductExpressUpdateElementData}
 */
public class ProductExpressUpdateQueue extends AbstractUpdateQueue<ProductExpressUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
