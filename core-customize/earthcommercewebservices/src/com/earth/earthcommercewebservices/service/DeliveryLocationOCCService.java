/**
 *
 */
package com.earth.earthcommercewebservices.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.earth.earthfacades.customer.SiteDeliveryCityAreaData;
import com.earth.earthfacades.user.delivery.DeliveryLocationService;


/**
 * @author monzer
 *
 */
public interface DeliveryLocationOCCService extends DeliveryLocationService
{
	Optional<SiteDeliveryCityAreaData> getDeliveryLocationDataByCurrentSite();

	Optional<SiteDeliveryCityAreaData> getDeliveryLocationData(CMSSiteModel siteUid);
}
