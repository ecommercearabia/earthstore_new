/**
 *
 */
package com.earth.earthcommercewebservices.service;

import java.util.Optional;

import com.earth.earthcommercewebservices.configuration.ContactUsConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.AddressConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.RegistrationConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.SiteConfigurationData;


/**
 * @author mbaker
 *
 */
public interface SiteConfigurationsService
{
	public Optional<RegistrationConfigurationData> getRegistrationConfigurationForCurrentSite();

	public Optional<AddressConfigurationData> getAddressConfigurationForCurrentSite();

	public Optional<SiteConfigurationData> getSiteConfigurationForCurrentSite();

	public Optional<ContactUsConfigurationData> getContactUsConfigurationForCurrentSite();

}
