package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercewebservicescommons.core.user.data.StoreCreditHistoriesData;
import de.hybris.platform.commercewebservicescommons.dto.product.PriceWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.StoreCreditHistoriesWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.earth.earthstorecreditfacades.StoreCreditHistoryData;
import com.earth.earthstorecreditfacades.facade.StoreCreditFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/storecredit")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Store Credit")
public class StoreCreditController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(StoreCreditController.class);

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	@ApiOperation(nickname = "getStoreCreditHistory", value = "Get store credit history  of a customer.", notes = "Returns all customer store credit history.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public StoreCreditHistoriesWsDTO getStoreCreditHistory(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		final Optional<List<StoreCreditHistoryData>> storeCreditHistoryByCurrentUser = storeCreditFacade
				.getStoreCreditHistoryByCurrentUser();
		final StoreCreditHistoriesData creditHistoriesData = new StoreCreditHistoriesData();
		if (storeCreditHistoryByCurrentUser.isPresent())
		{
			creditHistoriesData.setStoreCreditHistroy(storeCreditHistoryByCurrentUser.get());
		}

		return getDataMapper().map(creditHistoriesData, StoreCreditHistoriesWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/amount", method = RequestMethod.GET)
	@ApiOperation(nickname = "getStoreCreditHistory", value = "Get the store credit available amount  of a customer.", notes = "Returns the available store credit.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public PriceWsDTO getStoreCreditAvailableAmount(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{

		final Optional<PriceData> storeCreditAmountByCurrentUserAndCurrentBaseStore = storeCreditFacade
				.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
		final PriceData amount = storeCreditAmountByCurrentUserAndCurrentBaseStore.isPresent()
				? storeCreditAmountByCurrentUserAndCurrentBaseStore.get()
				: new PriceData();

		return getDataMapper().map(amount, PriceWsDTO.class, fields);
	}

}
