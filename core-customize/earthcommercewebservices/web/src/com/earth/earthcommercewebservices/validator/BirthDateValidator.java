package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class BirthDateValidator implements Validator
{

	private static final String INVALID_BIRTH_DATE_MESSAGE_ID = "field.register.birthDate.invalid";

	private static final String BIRTH_DATE_KEY = "birthdate";


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String birthDate;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String dateOfBirth = (String) errors.getFieldValue(this.birthDate);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		validateBirthDate(errors, currentSite, dateOfBirth);

	}

	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue(BIRTH_DATE_KEY, INVALID_BIRTH_DATE_MESSAGE_ID);
		}

	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate()
	{
		return birthDate;
	}

	/**
	 * @param birthDate
	 *           the birthDate to set
	 */
	public void setBirthDate(final String birthDate)
	{
		this.birthDate = birthDate;
	}



}

