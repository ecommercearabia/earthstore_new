/**
 *
 */
package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercewebservicescommons.dto.user.CountryWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.MaritalStatusWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.NationalityWsDTO;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.enums.MaritalStatus;
import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.service.NationalityService;
import com.earth.earthcore.user.service.MobilePhoneService;


/**
 * @author mbaker
 *
 */
public class UpdateUserValidator implements Validator
{

	private static final String INVALID_BIRTH_DATE_MESSAGE_ID = "field.register.birthDate.invalid";
	private static final String EMPTY_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.empty";
	private static final String INVALID_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.invalid";
	private static final String INVALID_NATIONALITY_MESSAGE_ID = "field.register.nationality.invalid";
	private static final String INVALID_NATIONALITY_ID_MESSAGE_ID = "field.register.nationalityId.invalid";
	private static final String INVALID_MOBILENUMBER_MESSAGE_ID = ".register.mobileNumber.invalid";
	private static final String INVALID_MOBILECOUNTRYCODE_ID = " field.register.mobileCountryCode.invalid";

	private static final String EMPTY_CURRENT_SITE_ERROR_MSG = "current site is null";

	private static final String NATIONALITY_KEY = "nationality";
	private static final String NATIONALITY_ID_KEY = "nationalityID";
	private static final String MARITAL_STATUS_KEY = "maritalStatus";
	private static final String BIRTH_DATE_KEY = "birthDate";


	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	private String mobileNumber;

	private String mobileCountry;

	private String nationality;

	private String nationalityId;

	private String hasNationalId;

	private String maritalStatus;

	private String birthDate;


	@Override
	public boolean supports(final Class<?> clazz)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String number = (String) errors.getFieldValue(this.mobileNumber);
		final CountryWsDTO countryWsDTO = (CountryWsDTO) errors.getFieldValue(this.mobileCountry);
		final String nationalId = (String) errors.getFieldValue(this.nationalityId);
		final NationalityWsDTO nationalityWsDTO = (NationalityWsDTO) errors.getFieldValue(this.nationality);
		final boolean hasId = (boolean) errors.getFieldValue(this.hasNationalId);
		final MaritalStatusWsDTO status = (MaritalStatusWsDTO) errors.getFieldValue(this.maritalStatus);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		final String dateOfBirth = (String) errors.getFieldValue(this.birthDate);

		validateNumber(number, countryWsDTO, errors);
		validateBirthDate(errors, currentSite, dateOfBirth);
		validateMaritalStatus(status, currentSite, errors);
		validateNationality(errors, currentSite, nationalityWsDTO);
		validateNationalID(errors, currentSite, nationalId, hasId);
	}

	private void validateNumber(final String number, final CountryWsDTO countryWsDTO, final Errors errors)
	{


		if (Objects.isNull(countryWsDTO) || StringUtils.isEmpty(countryWsDTO.getIsocode()))
		{
			errors.rejectValue(this.mobileCountry, INVALID_MOBILECOUNTRYCODE_ID, new String[]
			{ this.mobileCountry }, "This field is not a valid mobile country code.");

		}
		if (StringUtils.isEmpty(number))
		{
			errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
			{ this.mobileNumber }, "This field is not a valid mobile number.");
		}
		else if (!Objects.isNull(countryWsDTO) && !StringUtils.isEmpty(countryWsDTO.getIsocode()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(countryWsDTO.getIsocode(),
					number);

			if (!normalizedPhoneNumber.isPresent())
			{
				errors.rejectValue(this.mobileCountry, INVALID_MOBILECOUNTRYCODE_ID, new String[]
				{ this.mobileCountry }, "This field is not a valid mobile country code.");
				errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
				{ this.mobileNumber }, "This field is not a valid mobile number.");

			}
		}
	}

	private void validateNationality(final Errors errors, final CMSSiteModel currentSite, final NationalityWsDTO nationalityWsDTO)
	{

		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (Objects.isNull(nationalityWsDTO))
		{
			throw new IllegalArgumentException("nationalityWsDTO is null");

		}

		if (!currentSite.isNationalityCustomerEnabled() || !currentSite.isNationalityCustomerRequired())
		{
			return;
		}
		final String code = nationalityWsDTO.getCode();
		if (StringUtils.isEmpty(code))
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
			return;
		}

		final Optional<NationalityModel> national = nationalityService.get(code);
		if (!national.isPresent())
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
		}

	}

	private void validateNationalID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId,
			final boolean hasId)
	{

		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (!currentSite.isNationalityIdCustomerEnabled() || !currentSite.isNationalityIdCustomerRequired())
		{
			return;
		}

		if (hasId && StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue(NATIONALITY_ID_KEY, INVALID_NATIONALITY_ID_MESSAGE_ID);
		}

	}

	private void validateMaritalStatus(final MaritalStatusWsDTO maritalStatus, final CMSSiteModel currentSite, final Errors errors)
	{
		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (!currentSite.isCustomerMaritalStatusEnabled() || !currentSite.isCustomerMaritalStatusRequired())
		{
			return;
		}
		if ((Objects.isNull(maritalStatus) || StringUtils.isBlank(maritalStatus.getCode())))
		{
			errors.rejectValue(MARITAL_STATUS_KEY, EMPTY_MARITAL_STATUS_MESSAGE_ID);
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatus.getCode());
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue(MARITAL_STATUS_KEY, INVALID_MARITAL_STATUS_MESSAGE_ID);
		}
	}

	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue(BIRTH_DATE_KEY, INVALID_BIRTH_DATE_MESSAGE_ID);
		}

	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * @return the nationalityId
	 */
	public String getNationalityId()
	{
		return nationalityId;
	}

	/**
	 * @param nationalityId
	 *           the nationalityId to set
	 */
	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	/**
	 * @return the hasNationalId
	 */
	public String getHasNationalId()
	{
		return hasNationalId;
	}

	/**
	 * @param hasNationalId
	 *           the hasNationalId to set
	 */
	public void setHasNationalId(final String hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}

	/**
	 * @return the maritalStatus
	 */
	public String getMaritalStatus()
	{
		return maritalStatus;
	}

	/**
	 * @param maritalStatus
	 *           the maritalStatus to set
	 */
	public void setMaritalStatus(final String maritalStatus)
	{
		this.maritalStatus = maritalStatus;
	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate()
	{
		return birthDate;
	}

	/**
	 * @param birthdate
	 *           the birthDate to set
	 */
	public void setBirthDate(final String birthDate)
	{
		this.birthDate = birthDate;
	}

}
