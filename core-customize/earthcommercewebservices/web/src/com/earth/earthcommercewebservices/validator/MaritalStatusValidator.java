package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.enums.MaritalStatus;


public class MaritalStatusValidator implements Validator
{

	private static final String EMPTY_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.empty";
	private static final String INVALID_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.invalid";
	private static final String MARITAL_STATUS_KEY = "maritalStatusCode";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String maritalStatusCode;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String status = (String) errors.getFieldValue(this.maritalStatusCode);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite != null && currentSite.isCustomerMaritalStatusEnabled() && currentSite.isCustomerMaritalStatusRequired())
		{
			validateMaritalStatus(status, currentSite, errors);
		}
	}


	private void validateMaritalStatus(final String maritalStatusCode, final CMSSiteModel currentSite, final Errors errors)
	{
		if (StringUtils.isBlank(maritalStatusCode))
		{
			errors.rejectValue(MARITAL_STATUS_KEY, EMPTY_MARITAL_STATUS_MESSAGE_ID);
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatusCode);
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue(MARITAL_STATUS_KEY, INVALID_MARITAL_STATUS_MESSAGE_ID);
		}
	}

	/**
	 * @return the maritalStatusCode
	 */
	public String getmaritalStatusCode()
	{
		return maritalStatusCode;
	}

	/**
	 * @param maritalStatusCode
	 *           the maritalStatusCode to set
	 */
	public void setmaritalStatusCode(final String maritalStatusCode)
	{
		this.maritalStatusCode = maritalStatusCode;
	}

}

