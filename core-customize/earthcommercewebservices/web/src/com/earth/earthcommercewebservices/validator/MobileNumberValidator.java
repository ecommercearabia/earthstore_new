package com.earth.earthcommercewebservices.validator;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.user.service.MobilePhoneService;


public class MobileNumberValidator implements Validator
{

	private static final String INVALID_MOBILENUMBER_MESSAGE_ID = "field.register.mobileNumber.invalid";
	private static final String INVALID_MOBILECOUNTRYCODE_ID = " field.register.mobileCountryCode.invalid";

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	private String mobileCountry;
	private String mobileNumber;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String number = (String) errors.getFieldValue(this.mobileNumber);
		final String countryCode = (String) errors.getFieldValue(this.mobileCountry);

		if (StringUtils.isEmpty(countryCode))
		{
			errors.rejectValue(this.mobileCountry, INVALID_MOBILECOUNTRYCODE_ID, new String[]
			{ this.mobileCountry }, "This field is not a valid mobile country code.");

		}
		if (StringUtils.isEmpty(number))
		{
			errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
			{ this.mobileNumber }, "This field is not a valid mobile number.");
		}
		else if (!StringUtils.isEmpty(countryCode))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(countryCode,
					number);

			if (!normalizedPhoneNumber.isPresent())
			{
				errors.rejectValue(this.mobileCountry, INVALID_MOBILECOUNTRYCODE_ID, new String[]
				{ this.mobileCountry }, "This field is not a valid mobile country code.");
				errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
				{ this.mobileNumber }, "This field is not a valid mobile number.");

			}
		}

	}


	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

}

