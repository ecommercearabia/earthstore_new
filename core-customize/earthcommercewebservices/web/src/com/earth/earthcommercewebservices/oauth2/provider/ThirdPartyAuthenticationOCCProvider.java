/**
 *
 */
package com.earth.earthcommercewebservices.oauth2.provider;

import de.hybris.platform.core.Constants;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;
import de.hybris.platform.spring.security.CoreUserDetails;

import java.util.Collections;

import javax.annotation.Resource;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.earth.earthcommercewebservices.oauth2.token.ThirdPartyAuthenticationOCCToken;
import com.earth.earththirdpartyauthentication.context.ThirdPartyAuthenticationContext;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;



/**
 * @author monzer
 *
 */
public class ThirdPartyAuthenticationOCCProvider extends CoreAuthenticationProvider
{

	private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();
	private GrantedAuthority adminGroup = new SimpleGrantedAuthority(ROLE_ADMIN_GROUP);
	private final UserDetailsChecker postAuthenticationChecks = new CoreAuthenticationProvider().getPreAuthenticationChecks();

	@Resource(name = "thirdPartyAuthenticationContext")
	private ThirdPartyAuthenticationContext thirdPartyAuthenticationContext;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		if (Registry.hasCurrentTenant() && JaloConnection.getInstance().isSystemInitialized())
		{

			final String username = authentication.getPrincipal() == null ? "NONE_PROVIDED" : authentication.getName();

			UserDetails userDetails = null;
			try
			{
				userDetails = this.retrieveUser(username);
			}
			catch (final UsernameNotFoundException var6)
			{
				throw new BadCredentialsException(
						this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"), var6);
			}

			if (!isAuthenticatedFromThirdParty(authentication, userDetails))
			{
				throw new BadCredentialsException(
						this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));
			}

			this.getPreAuthenticationChecks().check(userDetails);
			final User user = UserManager.getInstance().getUserByLogin(userDetails.getUsername());

			this.additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);
			this.postAuthenticationChecks.check(userDetails);

			JaloSession.getCurrentSession().setUser(user);
			return this.createSuccessAuthentication(authentication, userDetails);
		}
		else
		{
			return super.createSuccessAuthentication(authentication, new CoreUserDetails("systemNotInitialized",
					"systemNotInitialized", true, false, true, true, Collections.EMPTY_LIST, (String) null));
		}

	}

	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{

	}

	protected Boolean isAuthenticatedFromThirdParty(final Authentication auth, final UserDetails user)
	{
		final Object token = ((ThirdPartyAuthenticationOCCToken) auth).getAuthToken();
		final String providerType = String.valueOf(((ThirdPartyAuthenticationOCCToken) auth).getProviderType());
		final String callbackHostUrl = String.valueOf(((ThirdPartyAuthenticationOCCToken) auth).getCallbackUrl());
		final String userId = user.getUsername().split("\\|")[0];
		try
		{
			return thirdPartyAuthenticationContext.verifyAccessTokenWithThirdPartyByCurrentSite(userId, token,
					ThirdPartyAuthenticationType.valueOf(providerType), callbackHostUrl);
		}
		catch (final ThirdPartyAuthenticationException e1)
		{
			return false;
		}

	}

	/**
	 * @param adminGroup
	 *           the adminGroup to set
	 */
	public void setAdminGroup(final String adminGroup)
	{
		this.adminGroup = new SimpleGrantedAuthority(adminGroup);

	}

	/**
	 * @return the adminGroup
	 */
	public GrantedAuthority getAdminGroup()
	{
		return adminGroup;
	}

	@Override
	public boolean supports(final Class authentication)
	{
		return ThirdPartyAuthenticationOCCToken.class.isAssignableFrom(authentication);
	}

}
