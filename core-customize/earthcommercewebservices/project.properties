# ---------------------------------------------------------------------------
# Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------
# you can put key/value pairs here.
# Use Config.getParameter(..) to retrieve the values during runtime.

earthcommercewebservices.key=value

# Specifies the location of the spring context file putted automatically to the global platform application context.
earthcommercewebservices.application-context=earthcommercewebservices-spring.xml

# Enables / disables XSS filter (overrides global settings)
#earthcommercewebservices.xss.filter.enabled=false

#Fallback taxcode is returned by DefaultTaxCodeStrategy when it cannot find taxCode for product and taxArea
#Different value can be configure for different base store by adding base store name at the end of property name
externaltax.fallbacktaxcode=PC040100

# For each flow, multiple scopes are supported. Delimiter is ',' e.g. basic,extended
earthcommercewebservices.oauth2.password.scope=basic
earthcommercewebservices.oauth2.clientCredentials.scope=extended
earthcommercewebservices.oauth2.tokenUrl=/authorizationserver/oauth/token

#Logger level set to warn to avoid information about mapping during server start (they could contain Exception string in method signature)
log4j2.logger.commerceHandlerMapping.name = com.earth.earthcommercewebservices.request.mapping.handler.CommerceHandlerMapping
log4j2.logger.commerceHandlerMapping.level = warn
log4j2.logger.commerceHandlerMapping.appenderRef.stdout.ref = STDOUT

# <v1-api>

earthcommercewebservices.v1.description=Commerce Webservices Version 1
earthcommercewebservices.v1.title=Commerce Webservices V1
earthcommercewebservices.v1.version=1.0

# </v1-api>

earthcommercewebservices.v2.description=These services manage all of the common commerce functionality, and also include customizations from installed AddOns. The implementing extension is called earthcommercewebservices.
earthcommercewebservices.v2.title=Commerce Webservices
earthcommercewebservices.v2.version=2.0
earthcommercewebservices.v2.license=Use of this file is subject to the terms of your agreement with SAP SE or its affiliates respecting the use of the SAP product for which this file relates.
earthcommercewebservices.v2.license.url=

earthcommercewebservices.sap.apiType=REST
earthcommercewebservices.sap.shortText=Enables you to manage all of the common commerce functionality.
earthcommercewebservices.sap.state=Active
earthcommercewebservices.sap.servers=default
earthcommercewebservices.sap.server.default.url=https://{url}/rest/v2
earthcommercewebservices.sap.server.default.description=Commerce Webservices v2 REST API endpoint
earthcommercewebservices.sap.server.default.templates.url.description=SAP Commerce Cloud server URL where the application is deployed.
earthcommercewebservices.sap.securityNames=oauth2_Password,oauth2_client_credentials
earthcommercewebservices.sap.security.oauth2_Password.scopes=basic
earthcommercewebservices.sap.security.oauth2_client_credentials.scopes=extended

#Use for generating static swagger documentation
earthcommercewebservices.documentation.static.generate=true
ext.earthcommercewebservices.extension.webmodule.webroot=/rest/v2

#################################
### Jar scanning setup for Tomcat
#################################
earthcommercewebservices.tomcat.tld.scan=*jstl-1*.jar
earthcommercewebservices.tomcat.tld.default.scan.enabled=false
earthcommercewebservices.tomcat.pluggability.scan=*jstl-1*.jar
earthcommercewebservices.tomcat.pluggability.default.scan.enabled=false

corsfilter.earthcommercewebservices.allowedOrigins=http://localhost:4200 https://localhost:4200
corsfilter.earthcommercewebservices.allowedMethods=GET HEAD OPTIONS PATCH PUT POST DELETE
corsfilter.earthcommercewebservices.allowedHeaders=origin content-type accept authorization cache-control if-none-match x-anonymous-consents

# Use to set if the cart should be refreshed by default or not
earthcommercewebservices.cart.refreshed.by.default=false

#Used for specifying compatible site channels for annotated api endpoints restricted by site channel
api.compatibility.b2c.channels=B2C

#Web root for commerce web services - used in CommerceWebServicesPaymentFacade for creating full SOP merchant callback url
webroot.commercewebservices.http=http://localhost:9001/rest
webroot.commercewebservices.https=https://localhost:9002/rest

# Used to specify the list of API endpoint identifiers that must be disabled (example: getCatalogs,getBaseSites)
# The endpoint identifier is represented by the "nickname" attribute of the ApiOperation annotation used on an endpoint method
#earthcommercewebservices.api.restrictions.disabled.endpoints=
