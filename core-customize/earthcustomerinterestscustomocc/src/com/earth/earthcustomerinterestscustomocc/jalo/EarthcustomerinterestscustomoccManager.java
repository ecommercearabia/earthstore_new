/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomocc.jalo;

import com.earth.earthcustomerinterestscustomocc.constants.EarthcustomerinterestscustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustomerinterestscustomoccManager extends GeneratedEarthcustomerinterestscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustomerinterestscustomoccManager.class.getName() );
	
	public static final EarthcustomerinterestscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustomerinterestscustomoccManager) em.getExtension(EarthcustomerinterestscustomoccConstants.EXTENSIONNAME);
	}
	
}
