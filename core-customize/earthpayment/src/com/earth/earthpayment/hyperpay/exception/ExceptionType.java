/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.hyperpay.exception;

/**
 *
 * @author monzer
 *
 */
public enum ExceptionType {

	BAD_REQUEST,
	INVALID_PARAMETER_FORMAT,
	SERVER_ERROR;

}
