/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.strategy;

import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.Optional;


/**
 * The Interface CustomPaymentTransactionStrategy.
 */
public interface CustomPaymentTransactionStrategy extends PaymentTransactionStrategy
{

	/**
	 * Save payment transaction entry.
	 *
	 * @param orderModel
	 *           the order model
	 * @param requestId
	 *           the request id
	 * @param paymentTransactionType
	 *           the payment transaction type
	 * @param transactionStatus
	 *           the transaction status
	 * @param transactionStatusDetails
	 *           the transaction status details
	 * @param requestPaymentBody
	 *           the request payment body
	 * @param responsePaymentBody
	 *           the response payment body
	 * @param entryCode
	 *           the entry code
	 * @return the optional
	 */
	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final TransactionStatusDetails transactionStatusDetails, final String requestPaymentBody,
			final String responsePaymentBody, final String entryCode);

	/**
	 * Save payment transaction entry.
	 *
	 * @param orderModel
	 *           the order model
	 * @param requestId
	 *           the request id
	 * @param paymentTransactionType
	 *           the payment transaction type
	 * @param transactionStatus
	 *           the transaction status
	 * @param transactionStatusDetails
	 *           the transaction status details
	 * @param requestPaymentBody
	 *           the request payment body
	 * @param responsePaymentBody
	 *           the response payment body
	 * @param entryCode
	 *           the entry code
	 * @return the optional
	 */
	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final String transactionStatusDetails, final String requestPaymentBody, final String responsePaymentBody,
			final String entryCode);

	/**
	 * Creates the capture entry if not exists.
	 *
	 * @param orderModel
	 *           the order model
	 */
	public void createCaptureEntryIfNotExists(final AbstractOrderModel orderModel);

	/**
	 * Creates the cancel entry if not exists.
	 *
	 * @param orderModel
	 *           the order model
	 */
	public void createCancelEntryIfNotExists(final AbstractOrderModel orderModel);
}
