/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.enums;

/**
 *
 */
public enum PaymentResponseStatus
{
	SUCCESS, FAILURE
}
