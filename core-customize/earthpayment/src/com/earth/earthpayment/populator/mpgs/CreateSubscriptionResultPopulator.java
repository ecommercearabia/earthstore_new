/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;

import java.util.Map;

import javax.annotation.Resource;

import com.earth.earthpayment.hyperpay.enums.TransactionStatus;
import com.earth.earthpayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Response;
import com.earth.earthpayment.mpgs.service.MpgsService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class CreateSubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter source (Map<String, String>) cannot be null");
		validateParameterNotNull(target, "Parameter target (CreateSubscriptionResult) cannot be null");
		final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = _builder.setPrettyPrinting().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		final Response response = transaction.getResponse();
		validateParameterNotNull(response, "Response cannot be null");

		final String gatewayCode = response.getGatewayCode();

		final TransactionStatus transactionStatus = getPaymentResponseStatus(gatewayCode);

		if (TransactionStatus.SUCCESS.equals(transactionStatus))
		{
			target.setDecision(DecisionsEnum.ACCEPT.toString());

		}
		else
		{
			target.setDecision(DecisionsEnum.ERROR.toString());

		}
		target.setDecisionPublicSignature(transaction.getResponse().getAcquirerMessage());
		target.setTransactionSignatureVerified(null);
		target.setReasonCode(1);
		target.setRequestId(transaction.getTransaction().getId());

	}

	private TransactionStatus getPaymentResponseStatus(final String responseGatewayCode)
	{
		if (Strings.isNullOrEmpty(responseGatewayCode))
		{
			return TransactionStatus.FAILED;
		}

		switch (responseGatewayCode)
		{
			case "APPROVED":
			case "APPROVED_AUTO":
				return TransactionStatus.SUCCESS;
			default:
				return TransactionStatus.FAILED;
		}

	}

}

