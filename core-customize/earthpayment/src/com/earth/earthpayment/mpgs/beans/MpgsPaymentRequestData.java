/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans;

/**
 *
 */
public class MpgsPaymentRequestData
{

	private String scriptSrc;
	private String merchantName;
	private String merchantLine1;
	private String merchantLine2;
	private String sessionId;
	private String version;
	private String merchantId;
	private String apiKey;


	/**
	 *
	 */
	public MpgsPaymentRequestData(final String scriptSrc, final String merchantName, final String merchantLine1,
			final String merchantLine2, final String merchantId, final String apiKey, final String sessionId, final String version)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.merchantName = merchantName;
		this.merchantLine1 = merchantLine1;
		this.merchantLine2 = merchantLine2;
		this.sessionId = sessionId;
		this.version = version;
		this.merchantId = merchantId;
		this.apiKey = apiKey;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId()
	{
		return merchantId;
	}

	/**
	 * @param merchantId
	 *           the merchantId to set
	 */
	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}

	/**
	 *
	 */
	public MpgsPaymentRequestData()
	{
		super();
	}

	/**
	 * @return the scriptSrc
	 */
	public String getScriptSrc()
	{
		return scriptSrc;
	}

	/**
	 * @param scriptSrc
	 *           the scriptSrc to set
	 */
	public void setScriptSrc(final String scriptSrc)
	{
		this.scriptSrc = scriptSrc;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName()
	{
		return merchantName;
	}

	/**
	 * @param merchantName
	 *           the merchantName to set
	 */
	public void setMerchantName(final String merchantName)
	{
		this.merchantName = merchantName;
	}

	/**
	 * @return the merchantLine1
	 */
	public String getMerchantLine1()
	{
		return merchantLine1;
	}

	/**
	 * @param merchantLine1
	 *           the merchantLine1 to set
	 */
	public void setMerchantLine1(final String merchantLine1)
	{
		this.merchantLine1 = merchantLine1;
	}

	/**
	 * @return the merchantLine2
	 */
	public String getMerchantLine2()
	{
		return merchantLine2;
	}

	/**
	 * @param merchantLine2
	 *           the merchantLine2 to set
	 */
	public void setMerchantLine2(final String merchantLine2)
	{
		this.merchantLine2 = merchantLine2;
	}

	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * @param version
	 *           the version to set
	 */
	public void setVersion(final String version)
	{
		this.version = version;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId()
	{
		return sessionId;
	}

	/**
	 * @param sessionId
	 *           the sessionId to set
	 */
	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey()
	{
		return apiKey;
	}

	/**
	 * @param apiKey
	 *           the apiKey to set
	 */
	public void setApiKey(final String apiKey)
	{
		this.apiKey = apiKey;
	}

}
