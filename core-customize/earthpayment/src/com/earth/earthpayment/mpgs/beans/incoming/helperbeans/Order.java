package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Order
{
	@Expose
	@SerializedName("amount")
	private String amount;
	@Expose
	@SerializedName("certainty")
	private String certainty;
	@Expose
	@SerializedName("chargeback")
	private Chargeback chargeback;
	@Expose
	@SerializedName("creationTime")
	private Date creationTime;
	@Expose
	@SerializedName("currency")
	private String currency;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("lastUpdatedTime")
	private Date lastUpdatedTime;
	@Expose
	@SerializedName("merchantAmount")
	private String merchantAmount;
	@Expose
	@SerializedName("merchantCategoryCode")
	private String merchantCategoryCode;
	@Expose
	@SerializedName("merchantCurrency")
	private String merchantCurrency;
	@Expose
	@SerializedName("status")
	private String status;
	@Expose
	@SerializedName("totalAuthorizedAmount")
	private String totalAuthorizedAmount;
	@Expose
	@SerializedName("totalCapturedAmount")
	private String totalCapturedAmount;
	@Expose
	@SerializedName("totalRefundedAmount")
	private String totalRefundedAmount;
	@Expose
	@SerializedName("authenticationStatus")
	private String authenticationStatus;
	@Expose
	@SerializedName("valueTransfer")
	private ValueTransferBean valueTransfer;

	public Order()
	{
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getCertainty()
	{
		return certainty;
	}

	public void setCertainty(final String certainty)
	{
		this.certainty = certainty;
	}

	public Chargeback getChargeback()
	{
		return chargeback;
	}

	public void setChargeback(final Chargeback chargeback)
	{
		this.chargeback = chargeback;
	}

	public Date getCreationTime()
	{
		return creationTime;
	}

	public void setCreationTime(final Date creationTime)
	{
		this.creationTime = creationTime;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public Date getLastUpdatedTime()
	{
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(final Date lastUpdatedTime)
	{
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getMerchantAmount()
	{
		return merchantAmount;
	}

	public void setMerchantAmount(final String merchantAmount)
	{
		this.merchantAmount = merchantAmount;
	}

	public String getMerchantCategoryCode()
	{
		return merchantCategoryCode;
	}

	public void setMerchantCategoryCode(final String merchantCategoryCode)
	{
		this.merchantCategoryCode = merchantCategoryCode;
	}

	public String getMerchantCurrency()
	{
		return merchantCurrency;
	}

	public void setMerchantCurrency(final String merchantCurrency)
	{
		this.merchantCurrency = merchantCurrency;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public String getTotalAuthorizedAmount()
	{
		return totalAuthorizedAmount;
	}

	public void setTotalAuthorizedAmount(final String totalAuthorizedAmount)
	{
		this.totalAuthorizedAmount = totalAuthorizedAmount;
	}

	public String getTotalCapturedAmount()
	{
		return totalCapturedAmount;
	}

	public void setTotalCapturedAmount(final String totalCapturedAmount)
	{
		this.totalCapturedAmount = totalCapturedAmount;
	}

	public String getTotalRefundedAmount()
	{
		return totalRefundedAmount;
	}

	public void setTotalRefundedAmount(final String totalRefundedAmount)
	{
		this.totalRefundedAmount = totalRefundedAmount;
	}

}
