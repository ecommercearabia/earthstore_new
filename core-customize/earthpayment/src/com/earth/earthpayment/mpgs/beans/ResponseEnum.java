/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans;

/**
 *
 */
public enum ResponseEnum
{
	ABORTED("ABORTED", "Transaction aborted by payer"),
	ACQUIRER_SYSTEM_ERROR("ACQUIRER_SYSTEM_ERROR", "Acquirer system error occurred processing the transaction"),
	APPROVED("APPROVED", "Transaction Approved"),
	APPROVED_AUTO("APPROVED_AUTO", "The transaction was automatically approved by the gateway. it was not submitted to the acquirer."),
	APPROVED_PENDING_SETTLEMENT("APPROVED_PENDING_SETTLEMENT", "Transaction Approved - pending batch settlement"),
	AUTHENTICATION_FAILED("AUTHENTICATION_FAILED", "Payer authentication failed"),
	AUTHENTICATION_IN_PROGRESS("AUTHENTICATION_IN_PROGRESS", "The operation determined that payer authentication is possible for the given card, but this has not been completed, and requires further action by the merchant to proceed."),
	BALANCE_AVAILABLE("BALANCE_AVAILABLE", "A balance amount is available for the card, and the payer can redeem points."),
	BALANCE_UNKNOWN("BALANCE_UNKNOWN", "A balance amount might be available for the card. Points redemption should be offered to the payer."),
	BLOCKED("BLOCKED", "Transaction blocked due to Risk or 3D Secure blocking rules"),
	CANCELLED("CANCELLED", "Transaction cancelled by payer"),
	DECLINED("DECLINED", "The requested operation was not successful. For example, a payment was declined by issuer or payer authentication was not able to be successfully completed."),
	DECLINED_AVS("DECLINED_AVS", "Transaction declined due to address verification"),
	DECLINED_AVS_CSC("DECLINED_AVS_CSC", "Transaction declined due to address verification and card security code"),
	DECLINED_CSC("DECLINED_CSC", "Transaction declined due to card security code"),
	DECLINED_DO_NOT_CONTACT("DECLINED_DO_NOT_CONTACT", "Transaction declined - do not contact issuer"),
	DECLINED_INVALID_PIN("DECLINED_INVALID_PIN", "Transaction declined due to invalid PIN"),
	DECLINED_PAYMENT_PLAN("DECLINED_PAYMENT_PLAN", "Transaction declined due to payment plan"),
	DECLINED_PIN_REQUIRED("DECLINED_PIN_REQUIRED", "Transaction declined due to PIN required"),
	DEFERRED_TRANSACTION_RECEIVED("DEFERRED_TRANSACTION_RECEIVED", "Deferred transaction received and awaiting processing"),
	DUPLICATE_BATCH("DUPLICATE_BATCH", "Transaction declined due to duplicate batch"),
	EXCEEDED_RETRY_LIMIT("EXCEEDED_RETRY_LIMIT", "Transaction retry limit exceeded"),
	EXPIRED_CARD("EXPIRED_CARD", "Transaction declined due to expired card"),
	INSUFFICIENT_FUNDS("INSUFFICIENT_FUNDS", "Transaction declined due to insufficient funds"),
	INVALID_CSC("INVALID_CSC", "Invalid card security code"),
	LOCK_FAILURE("LOCK_FAILURE", "Order locked - another transaction is in progress for this order"),
	NOT_ENROLLED_3D_SECURE("NOT_ENROLLED_3D_SECURE", "Card holder is not enrolled in 3D Secure"),
	NOT_SUPPORTED("NOT_SUPPORTED", "Transaction type not supported"),
	NO_BALANCE("NO_BALANCE", "A balance amount is not available for the card. The payer cannot redeem points."),
	PARTIALLY_APPROVED("PARTIALLY_APPROVED", "The transaction was approved for a lesser amount than requested. The approved amount is returned in order.totalAuthorizedAmount."),
	PENDING("PENDING", "Transaction is pending"),
	REFERRED("REFERRED", "Transaction declined - refer to issuer"),
	SUBMITTED("SUBMITTED", "The transaction has successfully been created in the gateway. It is either awaiting submission to the acquirer or has been submitted to the acquirer but the gateway has not yet received a response about the success or otherwise of the payment."),
	SYSTEM_ERROR("SYSTEM_ERROR", "Internal system error occurred processing the transaction"),
	TIMED_OUT("TIMED_OUT", "The gateway has timed out the request to the acquirer because it did not receive a response. Points redemption should not be offered to the payer."),
	UNKNOWN("UNKNOWN", "The transaction has been submitted to the acquirer but the gateway was not able to find out about the success or otherwise of the payment. If the gateway subsequently finds out about the success of the payment it will update the response code."),
	UNSPECIFIED_FAILURE("UNSPECIFIED_FAILURE", "Transaction could not be processed");
	
	
	private String name;
	private String msg;
	
	/**
	 * 
	 */
	private ResponseEnum(String name, String msg)
	{
		this.name = name;
		this.msg = msg;
				
	}
}
