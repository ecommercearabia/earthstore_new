package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Address
{
	@Expose
	@SerializedName("city")
	private String city;
	@Expose
	@SerializedName("country")
	private String country;
	@Expose
	@SerializedName("postcodeZip")
	private String postcodeZip;
	@Expose
	@SerializedName("stateProvince")
	private String stateProvince;
	@Expose
	@SerializedName("street")
	private String street;

	@Expose
	@SerializedName("sameAsBilling")
	private String sameAsBilling;

	public Address()
	{
	}

	public String getCity()
	{
		return city;
	}



	/**
	 * @return the sameAsBilling
	 */
	public String getSameAsBilling()
	{
		return sameAsBilling;
	}

	/**
	 * @param sameAsBilling
	 *           the sameAsBilling to set
	 */
	public void setSameAsBilling(final String sameAsBilling)
	{
		this.sameAsBilling = sameAsBilling;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getPostcodeZip()
	{
		return postcodeZip;
	}

	public void setPostcodeZip(final String postcodeZip)
	{
		this.postcodeZip = postcodeZip;
	}

	public String getStateProvince()
	{
		return stateProvince;
	}

	public void setStateProvince(final String stateProvince)
	{
		this.stateProvince = stateProvince;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(final String street)
	{
		this.street = street;
	}

}

