/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans;

/**
 *
 */
public class ReturnCallbackResponseBean
{
	private final String resultIndicator;
	private final String sessionVersion;



	/**
	 *
	 */
	public ReturnCallbackResponseBean(final String resultIndicator, final String sessionVersion)
	{
		super();
		this.resultIndicator = resultIndicator;
		this.sessionVersion = sessionVersion;
	}

	/**
	 * @return the resultIndicator
	 */
	public String getResultIndicator()
	{
		return resultIndicator;
	}

	/**
	 * @return the sessionVersion
	 */
	public String getSessionVersion()
	{
		return sessionVersion;
	}


}
