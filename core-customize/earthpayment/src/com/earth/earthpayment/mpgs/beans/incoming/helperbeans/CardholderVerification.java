package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardholderVerification {
	@Expose
	@SerializedName("avs")
	private Avs avs;

	public CardholderVerification() {
	}

	public Avs getAvs() {
		return avs;
	}

	public void setAvs(Avs avs) {
		this.avs = avs;
	}

}