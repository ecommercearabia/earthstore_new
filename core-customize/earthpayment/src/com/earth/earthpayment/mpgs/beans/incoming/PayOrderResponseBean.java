/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.incoming;

import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.InnerTransaction;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Order;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Response;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class PayOrderResponseBean extends MpgsResponse
{
	@Expose
	@SerializedName("authorizationResponse")
	private AuthorizationResponse authorizationResponse;
	@Expose
	@SerializedName("gatewayEntryPoint")
	private String gatewayEntryPoint;
	@Expose
	@SerializedName("merchant")
	private String merchant;
	@Expose
	@SerializedName("order")
	private Order order;
	@Expose
	@SerializedName("response")
	private Response response;
	@Expose
	@SerializedName("sourceOfFunds")
	private SourceOfFunds sourceOfFunds;
	@Expose
	@SerializedName("timeOfLastUpdate")
	private String timeOfLastUpdate;
	@Expose
	@SerializedName("timeOfRecord")
	private String timeOfRecord;
	@Expose
	@SerializedName("transaction")
	private InnerTransaction transaction;
	@Expose
	@SerializedName("version")
	private String version;

	/**
	 *
	 */
	public PayOrderResponseBean()
	{
	}

	/**
	 * @return the authorizationResponse
	 */
	public AuthorizationResponse getAuthorizationResponse()
	{
		return authorizationResponse;
	}

	/**
	 * @param authorizationResponse
	 *           the authorizationResponse to set
	 */
	public void setAuthorizationResponse(final AuthorizationResponse authorizationResponse)
	{
		this.authorizationResponse = authorizationResponse;
	}

	/**
	 * @return the gatewayEntryPoint
	 */
	public String getGatewayEntryPoint()
	{
		return gatewayEntryPoint;
	}

	/**
	 * @param gatewayEntryPoint
	 *           the gatewayEntryPoint to set
	 */
	public void setGatewayEntryPoint(final String gatewayEntryPoint)
	{
		this.gatewayEntryPoint = gatewayEntryPoint;
	}

	/**
	 * @return the merchant
	 */
	public String getMerchant()
	{
		return merchant;
	}

	/**
	 * @param merchant
	 *           the merchant to set
	 */
	public void setMerchant(final String merchant)
	{
		this.merchant = merchant;
	}

	/**
	 * @return the order
	 */
	public Order getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	public void setOrder(final Order order)
	{
		this.order = order;
	}

	/**
	 * @return the response
	 */
	public Response getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final Response response)
	{
		this.response = response;
	}

	/**
	 * @return the sourceOfFunds
	 */
	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	/**
	 * @param sourceOfFunds
	 *           the sourceOfFunds to set
	 */
	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	/**
	 * @return the timeOfLastUpdate
	 */
	public String getTimeOfLastUpdate()
	{
		return timeOfLastUpdate;
	}

	/**
	 * @param timeOfLastUpdate
	 *           the timeOfLastUpdate to set
	 */
	public void setTimeOfLastUpdate(final String timeOfLastUpdate)
	{
		this.timeOfLastUpdate = timeOfLastUpdate;
	}

	/**
	 * @return the timeOfRecord
	 */
	public String getTimeOfRecord()
	{
		return timeOfRecord;
	}

	/**
	 * @param timeOfRecord
	 *           the timeOfRecord to set
	 */
	public void setTimeOfRecord(final String timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}

	/**
	 * @return the transaction
	 */
	public InnerTransaction getTransaction()
	{
		return transaction;
	}

	/**
	 * @param transaction
	 *           the transaction to set
	 */
	public void setTransaction(final InnerTransaction transaction)
	{
		this.transaction = transaction;
	}

	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * @param version
	 *           the version to set
	 */
	public void setVersion(final String version)
	{
		this.version = version;
	}

}
