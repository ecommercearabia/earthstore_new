package com.earth.earthpayment.mpgs.beans.incoming;

import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SessionBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateCheckoutSessionResponseBean extends MpgsResponse {

	@Expose
	@SerializedName("merchant")
	private String merchant;

	@Expose
	@SerializedName("session")
	private SessionBean session;

	@Expose
	@SerializedName("successIndicator")
	private String successIndicator;

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public String getSuccessIndicator() {
		return successIndicator;
	}

	public void setSuccessIndicator(String successIndicator) {
		this.successIndicator = successIndicator;
	}

}