/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class BrowserDetails
{

	@Expose
	@SerializedName("3DSecureChallengeWindowSize")
	private final String _3DSecureChallengeWindowSize;
	@Expose
	@SerializedName("acceptHeaders")
	private final String acceptHeaders;
	@Expose
	@SerializedName("colorDepth")
	private final String colorDepth;
	@Expose
	@SerializedName("javaEnabled")
	private final String javaEnabled;
	@Expose
	@SerializedName("language")
	private final String language;
	@Expose
	@SerializedName("screenHeight")
	private final String screenHeight;
	@Expose
	@SerializedName("screenWidth")
	private final String screenWidth;
	@Expose
	@SerializedName("timeZone")
	private final String timeZone;

	/**
	 *
	 */
	public BrowserDetails()
	{
		this._3DSecureChallengeWindowSize = "FULL_SCREEN";
		this.acceptHeaders = "application/json";
		this.colorDepth = "24";
		this.javaEnabled = "true";
		this.language = "en-US";
		this.screenHeight = "640";
		this.screenWidth = "480";
		this.timeZone = "273";
	}

	/**
	 * @return the _3DSecureChallengeWindowSize
	 */
	public String get_3DSecureChallengeWindowSize()
	{
		return _3DSecureChallengeWindowSize;
	}

	/**
	 * @return the acceptHeaders
	 */
	public String getAcceptHeaders()
	{
		return acceptHeaders;
	}

	/**
	 * @return the colorDepth
	 */
	public String getColorDepth()
	{
		return colorDepth;
	}

	/**
	 * @return the javaEnabled
	 */
	public String getJavaEnabled()
	{
		return javaEnabled;
	}

	/**
	 * @return the language
	 */
	public String getLanguage()
	{
		return language;
	}

	/**
	 * @return the screenHeight
	 */
	public String getScreenHeight()
	{
		return screenHeight;
	}

	/**
	 * @return the screenWidth
	 */
	public String getScreenWidth()
	{
		return screenWidth;
	}

	/**
	 * @return the timeZone
	 */
	public String getTimeZone()
	{
		return timeZone;
	}

}
