package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Response
{
	@Expose
	@SerializedName("acquirerCode")
	private String acquirerCode;
	@Expose
	@SerializedName("acquirerMessage")
	private String acquirerMessage;
	@Expose
	@SerializedName("cardSecurityCode")
	private CardSecurityCode cardSecurityCode;
	@Expose
	@SerializedName("cardholderVerification")
	private CardholderVerification cardholderVerification;
	@Expose
	@SerializedName("gatewayCode")
	private String gatewayCode;
	@Expose
	@SerializedName("gatewayRecommendation")
	private String gatewayRecommendation;

	public Response()
	{
	}

	public String getAcquirerCode()
	{
		return acquirerCode;
	}

	public void setAcquirerCode(final String acquirerCode)
	{
		this.acquirerCode = acquirerCode;
	}

	public String getAcquirerMessage()
	{
		return acquirerMessage;
	}

	public void setAcquirerMessage(final String acquirerMessage)
	{
		this.acquirerMessage = acquirerMessage;
	}

	public CardSecurityCode getCardSecurityCode()
	{
		return cardSecurityCode;
	}

	public void setCardSecurityCode(final CardSecurityCode cardSecurityCode)
	{
		this.cardSecurityCode = cardSecurityCode;
	}

	public CardholderVerification getCardholderVerification()
	{
		return cardholderVerification;
	}

	public void setCardholderVerification(final CardholderVerification cardholderVerification)
	{
		this.cardholderVerification = cardholderVerification;
	}

	public String getGatewayCode()
	{
		return gatewayCode;
	}

	public void setGatewayCode(final String gatewayCode)
	{
		this.gatewayCode = gatewayCode;
	}

	public String getGatewayRecommendation()
	{
		return gatewayRecommendation;
	}

	public void setGatewayRecommendation(final String gatewayRecommendation)
	{
		this.gatewayRecommendation = gatewayRecommendation;
	}

}
