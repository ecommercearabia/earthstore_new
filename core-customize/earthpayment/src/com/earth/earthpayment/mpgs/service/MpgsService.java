package com.earth.earthpayment.mpgs.service;

import java.util.List;
import java.util.Optional;

import com.earth.earthpayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.CreateCheckoutSessionResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.Initiate3DAuthenticationResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.OrderStatusResponse;
import com.earth.earthpayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.earth.earthpayment.mpgs.beans.incoming.PayOrderResponseBean;
import com.earth.earthpayment.mpgs.beans.outgoing.CreateCheckoutSessionBean;
import com.earth.earthpayment.mpgs.beans.serviceparams.PayServiceParams;
import com.earth.earthpayment.mpgs.beans.serviceparams._3DSecureParams;
import com.earth.earthpayment.mpgs.constants.MpgsConstants;
import com.earth.earthpayment.mpgs.exception.MpgsException;


public interface MpgsService
{

	Optional<CreateCheckoutSessionResponseBean> createCheckoutSession(CreateCheckoutSessionBean createCheckoutSessionBean,
			MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> voidOrder(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> getTransaction(String orderId, String transactonId, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> getLastTransaction(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<List<OuterTransactionResponse>> getTransactions(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OrderStatusResponse> getOrder(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> authorizeOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> captureOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> refundOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> getLastTransaction(OrderStatusResponse order) throws MpgsException;

	Optional<Initiate3DAuthenticationResponseBean> initiate3DSecureAuthentication(_3DSecureParams params,
			final MpgsConstants constants) throws MpgsException;

	Optional<AuthenticatePayerResponseBean> authenticate3DSecurePayer(_3DSecureParams params, final MpgsConstants constants)
			throws MpgsException;

	Optional<PayOrderResponseBean> pay(final PayServiceParams params, final MpgsConstants constants) throws MpgsException;

}
