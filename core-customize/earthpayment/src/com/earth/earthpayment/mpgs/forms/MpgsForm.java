package com.earth.earthpayment.mpgs.forms;

public class MpgsForm {
	private String apiKey;
	private String merchantId;
	private String orderId;
	private String currency;
	private String amount;
	private String redirectUrl;
	private String cancelUrl;
	private String language;
	private String billingCity;
	private String billingCompany;
	private String billingCountry;
	private String billingPostcodeZip;
	private String billingStateProvince;
	private String billingStreet;
	private String billingStreet2;
	private String shippingCity;
	private String shippingCompany;
	private String shippingCountry;
	private String shippingPostcodeZip;
	private String shippingStateProvince;
	private String shippingStreet;
	private String shippingStreet2;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingCompany() {
		return billingCompany;
	}

	public void setBillingCompany(String billingCompany) {
		this.billingCompany = billingCompany;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPostcodeZip() {
		return billingPostcodeZip;
	}

	public void setBillingPostcodeZip(String billingPostcodeZip) {
		this.billingPostcodeZip = billingPostcodeZip;
	}

	public String getBillingStateProvince() {
		return billingStateProvince;
	}

	public void setBillingStateProvince(String billingStateProvince) {
		this.billingStateProvince = billingStateProvince;
	}

	public String getBillingStreet() {
		return billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingStreet2() {
		return billingStreet2;
	}

	public void setBillingStreet2(String billingStreet2) {
		this.billingStreet2 = billingStreet2;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingCompany() {
		return shippingCompany;
	}

	public void setShippingCompany(String shippingCompany) {
		this.shippingCompany = shippingCompany;
	}

	public String getShippingCountry() {
		return shippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}

	public String getShippingPostcodeZip() {
		return shippingPostcodeZip;
	}

	public void setShippingPostcodeZip(String shippingPostcodeZip) {
		this.shippingPostcodeZip = shippingPostcodeZip;
	}

	public String getShippingStateProvince() {
		return shippingStateProvince;
	}

	public void setShippingStateProvince(String shippingStateProvince) {
		this.shippingStateProvince = shippingStateProvince;
	}

	public String getShippingStreet() {
		return shippingStreet;
	}

	public void setShippingStreet(String shippingStreet) {
		this.shippingStreet = shippingStreet;
	}

	public String getShippingStreet2() {
		return shippingStreet2;
	}

	public void setShippingStreet2(String shippingStreet2) {
		this.shippingStreet2 = shippingStreet2;
	}

	@Override
	public String toString() {
		return "MpgsForm [apiKey=" + apiKey + ", merchantId=" + merchantId + ", orderId=" + orderId + ", currency="
				+ currency + ", amount=" + amount + ", redirectUrl=" + redirectUrl + ", cancelUrl=" + cancelUrl
				+ ", language=" + language + ", billingCity=" + billingCity + ", billingCompany=" + billingCompany
				+ ", billingCountry=" + billingCountry + ", billingPostcodeZip=" + billingPostcodeZip
				+ ", billingStateProvince=" + billingStateProvince + ", billingStreet=" + billingStreet
				+ ", billingStreet2=" + billingStreet2 + ", shippingCity=" + shippingCity + ", shippingCompany="
				+ shippingCompany + ", shippingCountry=" + shippingCountry + ", shippingPostcodeZip="
				+ shippingPostcodeZip + ", shippingStateProvince=" + shippingStateProvince + ", shippingStreet="
				+ shippingStreet + ", shippingStreet2=" + shippingStreet2 + "]";
	}

}
