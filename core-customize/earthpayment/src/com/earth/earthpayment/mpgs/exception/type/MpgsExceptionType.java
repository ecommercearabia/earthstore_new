package com.earth.earthpayment.mpgs.exception.type;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public enum MpgsExceptionType
{
	INVALID_REQUEST("INVALID_REQUEST", "The request was rejected because it did not conform to the API protocol."),
	REQUEST_REJECTED("REQUEST_REJECTED",
			"The request was rejected due to security reasons such as firewall rules, expired certificate, etc."),
	SERVER_BUSY("SERVER_BUSY", "The server did not have enough resources to process the request at the moment."),
	SERVER_FAILED("SERVER_FAILED", "There was an internal system failure."),
	UNKNOWN("UNKNOWN", "Unknown Error");

	private String code;
	private String message;

	private static Map<String, MpgsExceptionType> map = new HashMap<>();

	static {
		Arrays.asList(MpgsExceptionType.values()).stream().forEach(type -> map.put(type.code, type));
	}

	private MpgsExceptionType(final String result, final String message) {
		this.message = message;
		this.code = result;
	}

	public String getMessage() {
		return message;
	}

	public String getResult() {
		return code;
	}

	public static Optional<MpgsExceptionType> getExceptionFromCode(final String code) {
		return Optional.ofNullable(map.get(code));
	}

	public static MpgsExceptionType getExceptionOrDefault(final String code) {
		return map.getOrDefault(code, UNKNOWN);
	}
}
