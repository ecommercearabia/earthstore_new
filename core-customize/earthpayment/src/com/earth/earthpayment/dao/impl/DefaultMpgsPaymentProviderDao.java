/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.dao.impl;

import com.earth.earthpayment.dao.PaymentProviderDao;
import com.earth.earthpayment.model.MpgsPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultHyperpayPaymentProviderDao.
 */
public class DefaultMpgsPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultMpgsPaymentProviderDao()
	{
		super(MpgsPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return MpgsPaymentProviderModel._TYPECODE;
	}

}
