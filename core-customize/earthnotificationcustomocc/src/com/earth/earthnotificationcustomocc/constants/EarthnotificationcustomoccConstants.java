/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthnotificationcustomocc.constants;

/**
 * Global class for all earthnotificationcustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthnotificationcustomoccConstants extends GeneratedEarthnotificationcustomoccConstants
{
	public static final String EXTENSIONNAME = "earthnotificationcustomocc"; //NOSONAR

	private EarthnotificationcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
