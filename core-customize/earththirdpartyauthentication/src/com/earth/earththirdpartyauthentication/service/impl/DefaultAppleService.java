/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.service.impl;

import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.earth.earththirdpartyauthentication.service.AppleService;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import io.jsonwebtoken.io.Decoders;


/**
 * @author monzer
 */
public class DefaultAppleService implements AppleService
{

	private static final Logger LOG = Logger.getLogger(DefaultAppleService.class);
	private static final String APPLE_AUTH_URL = "https://appleid.apple.com/auth/token";

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String authorizationCode, final String jwtToken,
			final String clientId) throws ThirdPartyAuthenticationException
	{
		LOG.info("AppleUserServiceImpl getData()");

		if (StringUtils.isEmpty(authorizationCode) || StringUtils.isEmpty(jwtToken) || StringUtils.isEmpty(clientId))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		HttpResponse<String> response;
		try
		{
			response = Unirest.post(APPLE_AUTH_URL).header("Content-Type", "application/x-www-form-urlencoded")
					.field("client_id", clientId).field("client_secret", jwtToken).field("grant_type", "authorization_code")
					.field("code", authorizationCode).asString();
			LOG.info("Got the response code " + response.getStatus() + " from apple to get the user data");
		}
		catch (final UnirestException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (response.getStatus() != 200)
		{
			return Optional.empty();
		}
		final ThirdPartyAuthenticationUserData user = extractUserInfoFromResponse(response, authorizationCode);
		return Optional.of(user);
	}

	@Override
	public boolean verifyThirdPartyAccessToken(final String authorizationCode, final String jwtToken, final String clientId,
			final String id)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(authorizationCode) || StringUtils.isEmpty(jwtToken) || StringUtils.isEmpty(clientId))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final String refreshToken = sessionService.getCurrentSession().getAttribute("refresh_token");
		if (StringUtils.isEmpty(refreshToken))
		{
			return false;
		}
		HttpResponse<String> response;
		try
		{
			response = Unirest.post(APPLE_AUTH_URL).header("Content-Type", "application/x-www-form-urlencoded")
					.field("client_id", clientId).field("client_secret", jwtToken).field("grant_type", "refresh_token")
					.field("refresh_token", refreshToken).asString();
			LOG.info("Got the response code " + response.getStatus() + " from apple to verify the user refresh token");
		}
		catch (final UnirestException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}
		if (response.getStatus() != 200)
		{
			return false;
		}

		final ThirdPartyAuthenticationUserData user = extractUserInfoFromResponse(response, refreshToken);
		sessionService.getCurrentSession().removeAttribute("refresh_token");
		return user.getId().equals(id);
	}

	private ThirdPartyAuthenticationUserData extractUserInfoFromResponse(final HttpResponse<String> response, final String refreshToken)
	{
		final Map<String, Object> tokenResponse = new Gson().fromJson(response.getBody(), Map.class);
		final String accessToken = String.valueOf(tokenResponse.get("access_token"));
		final String idToken = String.valueOf(tokenResponse.get("id_token"));
		final String payload = idToken.split("\\.")[1];
		final String decoded = new String(Decoders.BASE64URL.decode(payload));

		// Store refresh token in session to validate the request in verifyThirdPartyAccessToken api.
		sessionService.getCurrentSession().setAttribute("refresh_token",
				tokenResponse.get("refresh_token") != null ? tokenResponse.get("refresh_token") : refreshToken);

		final Map<String, Object> userMap = new Gson().fromJson(decoded, Map.class);

		final ThirdPartyAuthenticationUserData user = new ThirdPartyAuthenticationUserData();
		user.setEmail(String.valueOf(userMap.get("email")));
		user.setId(String.valueOf(userMap.get("sub")));
		return user;
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getRegisterData(final String refreshToken, final String jwtToken,
			final String clientId) throws ThirdPartyAuthenticationException
	{
		LOG.info("AppleUserServiceImpl getRegisterData()");

		if (StringUtils.isEmpty(refreshToken) || StringUtils.isEmpty(jwtToken) || StringUtils.isEmpty(clientId))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		HttpResponse<String> response;
		try
		{
			response = Unirest.post(APPLE_AUTH_URL).header("Content-Type", "application/x-www-form-urlencoded")
					.field("client_id", clientId).field("client_secret", jwtToken).field("grant_type", "refresh_token")
					.field("refresh_token", refreshToken).asString();
			LOG.info("Got the response code " + response.getStatus() + " from apple to verify the user refresh token");
		}
		catch (final UnirestException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (response.getStatus() != 200)
		{
			return Optional.empty();
		}
		final ThirdPartyAuthenticationUserData user = extractUserInfoFromResponse(response, refreshToken);
		return Optional.of(user);
	}

}
