/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.earth.earththirdpartyauthentication.service.GoogleOauth2Service;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

/**
 * The Class DefaultGoogleOauth2Service.
 */
public class DefaultGoogleOauth2Service implements GoogleOauth2Service
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultGoogleOauth2Service.class);

	/** The Constant GOOGLE_LINK. */
	private static final String GOOGLE_LINK = "https://oauth2.googleapis.com/token";

	/**
	 * Gets the data.
	 *
	 * @param code
	 *           the code
	 * @param clientId
	 *           the client id
	 * @param clientSecret
	 *           the client secret
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String code, final String clientId, final String clientSecret)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("GoogleBasicUserServiceImpl getData()");
		if (StringUtils.isEmpty(code) || StringUtils.isEmpty(clientSecret) || StringUtils.isEmpty(clientId))
		{
			LOG.error("code or clientSecret or clientId is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		GoogleTokenResponse tokenResponse = null;

		try
		{
			tokenResponse = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(), JacksonFactory.getDefaultInstance(),
					GOOGLE_LINK, clientId, clientSecret, code, "postmessage").execute();
		}
		catch (final IOException e)
		{
			LOG.error("Not Authorized");

			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		GoogleIdToken idToken = null;
		try
		{
			idToken = tokenResponse.parseIdToken();
		}
		catch (final IOException e)
		{
			LOG.error("Not Authorized");

			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final GoogleIdToken.Payload payload = idToken.getPayload();
		final String userId = payload.getSubject();
		final String email = payload.getEmail();

		final String name = (String) payload.get("name");

		final String familyName = (String) payload.get("family_name");
		final String givenName = (String) payload.get("given_name");

		final ThirdPartyAuthenticationUserData user = new ThirdPartyAuthenticationUserData();
		user.setId(userId);
		user.setEmail(email);
		user.setName(name);
		user.setLastName(familyName);
		user.setFirstName(givenName);

		return Optional.ofNullable(user);
	}

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param appId
	 *           the app id
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String appId, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(appId) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(id, appId, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}
