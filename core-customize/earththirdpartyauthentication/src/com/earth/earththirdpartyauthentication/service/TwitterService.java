/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.service;

import java.util.Optional;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.entry.TwitterFormData;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;


/**
 * The Interface TwitterService.
 */
public interface TwitterService
{

	/**
	 * Gets the data.
	 *
	 * @param verifier
	 *           the verifier
	 * @param consumerKey
	 *           the consumer key
	 * @param consumerSecret
	 *           the consumer secret
	 * @param accessToken
	 *           the access token
	 * @param tokenSecret
	 *           the token secret
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public Optional<ThirdPartyAuthenticationUserData> getData(String verifier, String consumerKey, String consumerSecret,
			String accessToken, String tokenSecret) throws ThirdPartyAuthenticationException;

	/**
	 * Gets the twitter form data.
	 *
	 * @param consumerKey
	 *           the consumer key
	 * @param consumerSecret
	 *           the consumer secret
	 * @param callbackUrl
	 *           the callback url
	 * @return the twitter form data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public Optional<TwitterFormData> getTwitterFormData(String consumerKey, String consumerSecret, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	/**
	 * Verify third party access token.
	 *
	 * @param consumerKey
	 *           the consumer key
	 * @param consumerKeySecret
	 *           the consumer key secret
	 * @param id
	 *           the id
	 * @param token
	 *           the token
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public boolean verifyThirdPartyAccessToken(String consumerKey, String consumerKeySecret, String id, String token)
			throws ThirdPartyAuthenticationException;

}
