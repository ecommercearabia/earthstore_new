/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.service;

import java.util.Optional;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;

/**
 * The Interface GoogleBasicService.
 */
public interface GoogleBasicService
{

	/**
	 * Gets the data.
	 *
	 * @param token
	 *           the token
	 * @param clientId
	 *           the client id
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public Optional<ThirdPartyAuthenticationUserData> getData(String token, String clientId)
			throws ThirdPartyAuthenticationException;

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public boolean verifyThirdPartyAccessToken(String id, String token, String appSecret) throws ThirdPartyAuthenticationException;

}
