/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.dao.impl;

import com.earth.earththirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.earth.earththirdpartyauthentication.model.AppleAuthenticationProviderModel;


/**
 * @author monzer
 */
public class DefaultAppleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	*
	*/
	public DefaultAppleAuthenticationProviderDao()
	{
		super(AppleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AppleAuthenticationProviderModel._TYPECODE;
	}

}
