/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.List;
import java.util.Optional;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyUserData;
import com.earth.earththirdpartyauthentication.entry.TwitterFormData;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;


/**
 * @author monzer
 */
public interface ThirdPartyAuthenticationContext
{
	List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderData(CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

	List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderDataByCurrentStore()
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserDataByCurrentStore(Object data, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getThirdPartyFormData(Object data, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getThirdPartyFormDataByCurrentStore(Object data, ThirdPartyAuthenticationType type,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdPartyByCurrentSite(Object data, Object token, ThirdPartyAuthenticationType type,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyProfileByCurrentSite(Object data, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyProfileBySite(Object data, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyRegistrationProfileByCurrentSite(Object data, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyRegistrationProfileBySite(Object data, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

}
