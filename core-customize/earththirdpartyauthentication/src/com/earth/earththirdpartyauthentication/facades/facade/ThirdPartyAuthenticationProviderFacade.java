/**
 *
 */
package com.earth.earththirdpartyauthentication.facades.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Optional;

import com.earth.earththirdpartyauthentication.data.ThirdPartyUserData;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.thirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListData;
import com.earth.thirdpartyauthentication.provider.apple.AppleAuthenticationProviderData;
import com.earth.thirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderData;
import com.earth.thirdpartyauthentication.provider.google.GoogleAuthenticationProviderData;


/**
 * @author monzer
 *
 */
public interface ThirdPartyAuthenticationProviderFacade
{

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderByCurrentSite();

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySiteUid(String siteUid);

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySite(CMSSiteModel site);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderByCurrentSite();

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySiteUid(String siteUid);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderByCurrentSite();

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySiteUid(String siteUid);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersByCurrentSite();

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySiteUid(String siteUid);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySite(CMSSiteModel site);

	Optional<ThirdPartyUserData> getThirdPartyUserData(Object authData, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyToken(Object token, ThirdPartyAuthenticationType type, CMSSiteModel site)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyTokenByCurrentSite(Object token, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	void saveUser(CustomerData customer);

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationData(Object authData, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

}
