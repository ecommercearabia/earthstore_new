/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earththirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.TwitterAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.service.ThirdPartyAuthenticationProviderService;
import com.earth.earththirdpartyauthentication.strategy.AuthenticationProviderStrategy;


/**
 *
 */
public class DefaultTwitterAuthenticationProviderStrategy implements AuthenticationProviderStrategy
{
	@Resource(name = "thirdPartyAuthenticationProviderService")
	private ThirdPartyAuthenticationProviderService thirdPartyAuthenticationProviderService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getThirdPartyAuthenticationProviderService().get(cmsSiteUid, TwitterAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getThirdPartyAuthenticationProviderService().getActive(cmsSiteModel, TwitterAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProviderByCurrentSite()
	{
		return getThirdPartyAuthenticationProviderService().getActiveByCurrentSite(TwitterAuthenticationProviderModel.class);
	}

	public ThirdPartyAuthenticationProviderService getThirdPartyAuthenticationProviderService()
	{
		return thirdPartyAuthenticationProviderService;
	}
}

