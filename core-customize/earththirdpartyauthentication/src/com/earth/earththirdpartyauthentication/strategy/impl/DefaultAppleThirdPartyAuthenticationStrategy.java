/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.strategy.impl;

import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Date;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.util.ResourceUtils;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.entry.TwitterFormData;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.earth.earththirdpartyauthentication.model.AppleAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.service.AppleService;
import com.earth.earththirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;

import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;



/**
 * @author monzer
 */
public class DefaultAppleThirdPartyAuthenticationStrategy implements ThirdPartyAuthenticationStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultAppleThirdPartyAuthenticationStrategy.class);

	@Resource(name = "appleService")
	private AppleService appleService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "urlEncoderService")
	private UrlEncoderService urlEncoderService;

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (provider instanceof AppleAuthenticationProviderModel)
		{
			final String appId = ((AppleAuthenticationProviderModel) provider).getServiceId();
			final ThirdPartyAuthenticationProviderData thirdPartyAuthenticationProviderData = new ThirdPartyAuthenticationProviderData();
			thirdPartyAuthenticationProviderData.setId(appId);
			thirdPartyAuthenticationProviderData.setType(ThirdPartyAuthenticationType.APPLE.getCode());
			thirdPartyAuthenticationProviderData.setActive(provider.isActive());
			thirdPartyAuthenticationProviderData.setEnabledForMobile(provider.isActive() && provider.isEnabledForMobile());
			thirdPartyAuthenticationProviderData.setEnabledForStoreFront(provider.isActive() && provider.isEnabledForStoreFront());
			return Optional.ofNullable(thirdPartyAuthenticationProviderData);
		}
		else
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		if (!(provider instanceof AppleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final AppleAuthenticationProviderModel appleProvider = (AppleAuthenticationProviderModel) provider;

		final String jwtToken = generateJWTToken(appleProvider.getKeyId(), appleProvider.getTeamId(), appleProvider.getServiceId(),
				appleProvider.getCertificateFileName());

		LOG.info("getting user info from apple");
		final Optional<ThirdPartyAuthenticationUserData> thirdPartyAuthenticationUserData = appleService.getData((String) data,
				jwtToken, appleProvider.getServiceId());

		if (!thirdPartyAuthenticationUserData.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		return thirdPartyAuthenticationUserData;
	}

	@Override
	public Optional<TwitterFormData> getFormData(final Object data, final ThirdPartyAuthenticationProviderModel provider,
			final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED,
				ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED.getMsg());
	}

	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object id, final Object token,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		LOG.info("Verify user token");
		if (id == null || token == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		if (!(provider instanceof AppleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		final AppleAuthenticationProviderModel appleProvider = (AppleAuthenticationProviderModel) provider;
		final String jwtToken = generateJWTToken(appleProvider.getKeyId(), appleProvider.getTeamId(), appleProvider.getServiceId(),
				appleProvider.getCertificateFileName());
		return appleService.verifyThirdPartyAccessToken(String.valueOf(token), jwtToken, appleProvider.getServiceId(),
				String.valueOf(id));
	}

	protected String generateJWTToken(final String keyId, final String teamId, final String serviceId, final String filePath)
	{

		PrivateKey pKey = null;
		try
		{
			pKey = getPrivateKey(filePath);
		}
		catch (final IOException e)
		{
			LOG.error("DefaultAppleThirdPartyAuthenticationStrategy:IOException: " + e.getMessage());
		}
		if (pKey == null)
		{
			LOG.error(
					"DefaultAppleThirdPartyAuthenticationStrategy: Null private key, could not be extracted from the certificate file");
			return null;
		}
		return Jwts.builder().setHeaderParam(JwsHeader.KEY_ID, keyId).setIssuer(teamId).setAudience("https://appleid.apple.com")
				.setSubject(serviceId).setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 5)))
				.setIssuedAt(new Date(System.currentTimeMillis())).signWith(pKey, SignatureAlgorithm.ES256).compact();
	}

	/**
	 * @throws IOException
	 */
	protected PrivateKey getPrivateKey(final String fileURI) throws IOException
	{
		final File keyFile = ResourceUtils.getFile("classpath:earththirdpartyauthentication/" + fileURI);
		try (PEMParser pemParser = new PEMParser(new FileReader(keyFile)))
		{
			final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
			final PrivateKeyInfo object = PrivateKeyInfo.getInstance(pemParser.readObject());
			final PrivateKey pKey = converter.getPrivateKey(object);
			keyFile.deleteOnExit();
			return pKey;
		}
		catch (final IOException e)
		{
			LOG.error("An error occured while extracting the private key for Apple : " + e.getStackTrace());
		}
		return null;
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		if (!(provider instanceof AppleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final AppleAuthenticationProviderModel appleProvider = (AppleAuthenticationProviderModel) provider;

		final String jwtToken = generateJWTToken(appleProvider.getKeyId(), appleProvider.getTeamId(), appleProvider.getServiceId(),
				appleProvider.getCertificateFileName());

		LOG.info("getting user info from apple");
		final Optional<ThirdPartyAuthenticationUserData> thirdPartyAuthenticationUserData = appleService
				.getRegisterData((String) data, jwtToken, appleProvider.getServiceId());

		if (!thirdPartyAuthenticationUserData.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		return thirdPartyAuthenticationUserData;
	}

}
