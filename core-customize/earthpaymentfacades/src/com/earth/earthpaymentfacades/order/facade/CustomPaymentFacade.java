/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpaymentfacades.order.facade;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;

import java.util.Map;


/**
 * @author mnasro
 */
public interface CustomPaymentFacade extends PaymentFacade
{

	public PaymentSubscriptionResultData completePaymentCreateSubscription(Map<String, Object> orderInfoMap,
			boolean saveInAccount);
}
