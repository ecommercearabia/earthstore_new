/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpaymentfacades.constants;

/**
 * Global class for all Earthpaymentfacades constants. You can add global constants for your extension into this class.
 */
public final class EarthpaymentfacadesConstants extends GeneratedEarthpaymentfacadesConstants
{
	public static final String EXTENSIONNAME = "earthpaymentfacades";

	private EarthpaymentfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthpaymentfacadesPlatformLogo";
}
