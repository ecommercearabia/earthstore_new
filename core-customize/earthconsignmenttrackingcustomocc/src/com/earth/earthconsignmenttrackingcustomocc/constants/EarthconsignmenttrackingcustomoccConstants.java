/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomocc.constants;

/**
 * Global class for all earthconsignmenttrackingcustomocc constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthconsignmenttrackingcustomoccConstants extends GeneratedEarthconsignmenttrackingcustomoccConstants
{
	public static final String EXTENSIONNAME = "earthconsignmenttrackingcustomocc"; //NOSONAR

	private EarthconsignmenttrackingcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
