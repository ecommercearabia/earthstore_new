/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomocc.jalo;

import com.earth.earthconsignmenttrackingcustomocc.constants.EarthconsignmenttrackingcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthconsignmenttrackingcustomoccManager extends GeneratedEarthconsignmenttrackingcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthconsignmenttrackingcustomoccManager.class.getName() );
	
	public static final EarthconsignmenttrackingcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthconsignmenttrackingcustomoccManager) em.getExtension(EarthconsignmenttrackingcustomoccConstants.EXTENSIONNAME);
	}
	
}
