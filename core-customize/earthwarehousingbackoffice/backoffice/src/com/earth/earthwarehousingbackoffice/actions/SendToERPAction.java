/**
 *
 */
package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendToERPAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendToERPAction.class);

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();

		// TODO: Implementation

		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		if (ctx == null || ctx.getData() == null || ctx.getData().getOrder() == null || ctx.getData().getOrder().getSite() == null)
		{
			return false;
		}
		final CMSSiteModel cmsSite = (CMSSiteModel) ctx.getData().getOrder().getSite();
		return cmsSite.isSendOrdersToERPEnabled() && ConsignmentStatus.SHIPPED.equals(ctx.getData().getStatus());
	}



	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		// TODO: Change logic here
		final boolean doneBefore = false;
		if (doneBefore)
		{
			return "Do you want to resend to ERP system?";
		}
		else
		{
			return "Do you want to send to ERP system?";
		}
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
