/**
 *
 */
package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.ship.ConfirmShippedConsignmentAction;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zul.Messagebox;

import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.service.ERPIntegrationService;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;


/**
 * @author monzer
 *
 */
public class CustomConfirmShippmentAction extends ConfirmShippedConsignmentAction
{
	@Resource(name = "erpIntegrationService")
	private ERPIntegrationService erpIntegrationService;

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}
		final boolean canPerform = super.canPerform(actionContext);
		return canPerform && checkShipment(consignment);
	}

	/**
	 * @param data
	 * @return
	 */
	private boolean checkShipment(final ConsignmentModel consignment)
	{
		return consignment != null && consignment.getCarrierDetails() != null
				&& StringUtils.isNotBlank(consignment.getTrackingID());
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{

		if (sendSalesOrder(actionContext.getData()))
		{
			return super.perform(actionContext);
		}
		Messagebox.show("Error Message: " + "Cannot send to ERP system");

		return new ActionResult<>(ActionResult.ERROR);
	}

	/**
	 * @param consignment
	 */
	protected boolean sendSalesOrder(final Object data)
	{

		if (!(data instanceof ConsignmentModel))
		{
			return false;
		}

		final ConsignmentModel consignment = (ConsignmentModel) data;
		if (Objects.isNull(consignment.getOrder()))
		{
			return false;
		}

		try
		{
			return getErpIntegrationService().sendSalesOrder(consignment.getOrder().getStore(), consignment);
		}
		catch (final EarthERPException e)
		{
			return false;
		}


	}

	/**
	 * @return the erpIntegrationService
	 */
	protected ERPIntegrationService getErpIntegrationService()
	{
		return erpIntegrationService;
	}





}
