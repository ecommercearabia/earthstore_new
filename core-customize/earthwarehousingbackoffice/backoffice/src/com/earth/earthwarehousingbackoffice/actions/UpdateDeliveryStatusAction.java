/**
 *
 */
package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.earth.earthfulfillment.context.FulfillmentContext;
import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateDeliveryStatusAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final String PICKUP_IN_STORE_MODE = "pickup";
	private static final Logger LOG = LoggerFactory.getLogger(UpdateDeliveryStatusAction.class);
	private static final String COULD_NOT_UPDATE_SHIPMENT = "Could not update the Shipment status";


	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	public FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();

		Optional<ConsignmentStatus> updatedStatus = Optional.empty();
		try
		{
			updatedStatus = fulfillmentContext.updateStatusByCurrentStore(consignment);
		}
		catch (final FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
			return actionResult;
		}

		if (updatedStatus.isEmpty())
		{
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show("Could not fetch the shipment status from the fulfillment provider", "Error", Messagebox.OK,
					Messagebox.ERROR);
			return actionResult;
		}
		final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
		statusFlags.add(StatusFlag.OBJECT_MODIFIED);
		actionResult.setStatusFlags(statusFlags);
		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		if (ctx == null)
		{
			return false;
		}
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}
		if (consignment.getCarrierDetails() == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();

		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return provider.isPresent() && provider.get().getActive()
				&& org.apache.commons.lang3.StringUtils.isNotBlank(consignment.getTrackingID());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		return "Do you want to update shipment details for this consignment?";
	}


}
