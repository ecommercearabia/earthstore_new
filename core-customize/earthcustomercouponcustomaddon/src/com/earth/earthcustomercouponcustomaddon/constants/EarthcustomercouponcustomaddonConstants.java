/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponcustomaddon.constants;

/**
 * Earthcustomercouponcustomaddon constants
 */
public final class EarthcustomercouponcustomaddonConstants extends GeneratedEarthcustomercouponcustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthcustomercouponcustomaddon";

	private EarthcustomercouponcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
