/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthnotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthnotificationcustomaddon.constants.EarthnotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class EarthnotificationcustomaddonManager extends GeneratedEarthnotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthnotificationcustomaddonManager.class.getName() );
	
	public static final EarthnotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthnotificationcustomaddonManager) em.getExtension(EarthnotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
