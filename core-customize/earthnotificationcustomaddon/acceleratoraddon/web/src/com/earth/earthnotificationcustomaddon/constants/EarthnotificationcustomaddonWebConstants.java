/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthnotificationcustomaddon.constants;

/**
 * Global class for all Earthnotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class EarthnotificationcustomaddonWebConstants // NOSONAR
{
	private EarthnotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
