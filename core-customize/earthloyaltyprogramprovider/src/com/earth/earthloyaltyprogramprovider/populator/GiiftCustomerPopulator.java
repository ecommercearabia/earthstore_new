/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.giift.beans.Customer;
import com.earth.earthloyaltyprogramprovider.giift.enums.CustomerType;
import com.earth.earthloyaltyprogramprovider.giift.enums.Gender;



/**
 *
 */
public class GiiftCustomerPopulator implements Populator<CustomerModel, Customer>
{

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;



	@Override
	public void populate(final CustomerModel source, final Customer target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (!Objects.isNull(source.getBirthOfDate()))
		{
			target.setAge(source.getBirthOfDate().replace('/', '-'));
		}
		target.setGender(Gender.MALE);//TODO


		final Configuration configuration = getConfigurationService().getConfiguration();
		final String env = configuration.getString("loyalty.env");

		if (!StringUtils.isBlank(env))

		{
			final String id = source.getOriginalUid() + "_" + env;
			target.setId(id.toLowerCase());
		}
		else
		{
			target.setId(source.getOriginalUid());
		}
		target.setMobileNumber(source.getMobileNumber());
		target.setName(source.getName());
		target.setOccupation(source.getTitle().getName());//TODO
		target.setProvince(source.getMobileCountry().getIsocode());//TODO
		target.setType(CustomerType.NON_SHAREHOLDER);

	}



	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}



	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	protected void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}


}
