/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.beans.CustomerHistory;
import com.earth.earthloyaltyprogramprovider.giift.beans.Log;
import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 *
 */
public class GiiftCustomerHistoryLogPopulator implements Populator<Log, CustomerHistory>
{



	@Override
	public void populate(final Log source, final CustomerHistory target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCreated(source.getCreated());
		target.setData(source);
		target.setExpireDate(source.getExpireDate());
		target.setFinalAmount(source.getFinalAmount());
		target.setInitialAmount(source.getInitialAmount());
		target.setPoints(source.getPoints());
		target.setType(source.getType());
		target.setMetaData(getMetaData(source.getMetaData()));
		/*
		 * if (source.getMetaData() != null && source.getMetaData().getTransaction() != null) {
		 * target.setOrderNumber(source.getMetaData().getTransaction().getId()); }
		 */
	}

	/**
	 *
	 */
	private GiiftMetaInfo getMetaData(final String metaData)
	{
		if (Strings.isBlank(metaData))
		{
			return null;
		}

		final Gson gson = new GsonBuilder().create();
		return gson.fromJson(metaData, GiiftMetaInfo.class);

	}

}
