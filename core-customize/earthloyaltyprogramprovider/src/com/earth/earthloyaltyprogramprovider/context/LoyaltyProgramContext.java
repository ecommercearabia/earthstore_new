/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.context;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;



/**
 *
 */
public interface LoyaltyProgramContext
{

	public boolean isRegister(CustomerModel customer, BaseStoreModel baseStore) throws EarthLoyaltyException;

	public boolean isRegisterByCurrentBaseStore(CustomerModel customer) throws EarthLoyaltyException;

	public boolean isRegisterByCurrentBaseStoreAndCustomer() throws EarthLoyaltyException;

	public boolean registerCustomer(CustomerModel customer, BaseStoreModel baseStore) throws EarthLoyaltyException;

	public boolean registerCustomerByCurrentBaseStore(CustomerModel customer) throws EarthLoyaltyException;

	public boolean registerCustomerByCurrentBaseStoreAndCustomer() throws EarthLoyaltyException;


	public Optional<LoyaltyUsablePoints> getUsablePoints(AbstractOrderModel order, double orderTotalAmount)
			throws EarthLoyaltyException;

	public Optional<LoyaltyUsablePoints> getUsablePointsByOrder(AbstractOrderModel order)
			throws EarthLoyaltyException;

	public Optional<ValidateTransactionResult> validateTransaction(AbstractOrderModel order)
			throws EarthLoyaltyException;

	public boolean createTransaction(AbstractOrderModel order) throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyPagination pagination,
			BaseStoreModel baseStore) throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStore(CustomerModel customer,
			LoyaltyPagination pagination)
			throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStoreAndCustomer(LoyaltyPagination pagination)
			throws EarthLoyaltyException;


	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(CustomerModel customer, BaseStoreModel baseStore)
			throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStore(CustomerModel customer)
			throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStoreAndCustomer() throws EarthLoyaltyException;

	public boolean cancelTransaction(AbstractOrderModel order) throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(CustomerModel customer, BaseStoreModel baseStore)
			throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStore(CustomerModel customer)
			throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStoreAndCustomer()
			throws EarthLoyaltyException;

	public void reserve(AbstractOrderModel order, double orderTotalAmount)
			throws EarthLoyaltyException;

	public boolean isLoyaltyEnabled(AbstractOrderModel order);

	public boolean isLoyaltyEnabled(BaseStoreModel baseStoreModel, CustomerModel customer);

	public boolean isLoyaltyEnabledByCurrentBaseStore(CustomerModel customer);

	public boolean isLoyaltyEnabledByCurrentBaseStoreAndCurrentCustomer();




}
