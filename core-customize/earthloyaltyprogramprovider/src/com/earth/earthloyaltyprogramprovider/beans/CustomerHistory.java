/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.beans;

import java.util.Date;

import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;


/**
 *
 */
public class CustomerHistory
{
	private Date created;

	private Date expireDate;

	private double initialAmount;

	private double finalAmount;

	private double points;

	private String type;

	private Object data;

	private GiiftMetaInfo metaData;

	/**
	 * @return the created
	 */
	public Date getCreated()
	{
		return created;
	}

	/**
	 * @param created
	 *           the created to set
	 */
	public void setCreated(final Date created)
	{
		this.created = created;
	}

	/**
	 * @return the expireDate
	 */
	public Date getExpireDate()
	{
		return expireDate;
	}

	/**
	 * @param expireDate
	 *           the expireDate to set
	 */
	public void setExpireDate(final Date expireDate)
	{
		this.expireDate = expireDate;
	}

	/**
	 * @return the initialAmount
	 */
	public double getInitialAmount()
	{
		return initialAmount;
	}

	/**
	 * @param initialAmount
	 *           the initialAmount to set
	 */
	public void setInitialAmount(final double initialAmount)
	{
		this.initialAmount = initialAmount;
	}

	/**
	 * @return the finalAmount
	 */
	public double getFinalAmount()
	{
		return finalAmount;
	}

	/**
	 * @param finalAmount
	 *           the finalAmount to set
	 */
	public void setFinalAmount(final double finalAmount)
	{
		this.finalAmount = finalAmount;
	}

	/**
	 * @return the points
	 */
	public double getPoints()
	{
		return points;
	}

	/**
	 * @param points
	 *           the points to set
	 */
	public void setPoints(final double points)
	{
		this.points = points;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @param data
	 *           the data to set
	 */
	public void setData(final Object data)
	{
		this.data = data;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 *           the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * @return the metaData
	 */
	public GiiftMetaInfo getMetaData()
	{
		return metaData;
	}

	/**
	 * @param metaData
	 *           the metaData to set
	 */
	public void setMetaData(final GiiftMetaInfo metaData)
	{
		this.metaData = metaData;
	}





}
