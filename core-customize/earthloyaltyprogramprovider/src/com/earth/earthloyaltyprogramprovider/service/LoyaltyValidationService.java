/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;


/**
 *
 */
public interface LoyaltyValidationService
{
	public void validateLoyaltyEnabled(final CustomerModel customer, final BaseStoreModel baseStore) throws EarthLoyaltyException;

	public void validateLoyaltyEnabledByCurrentStore(final CustomerModel customer) throws EarthLoyaltyException;

	public void validateLoyaltyEnabledByCurrentStoreAndCurrentCustomer() throws EarthLoyaltyException;

	public void validateLoyaltyEnabledByOrder(final AbstractOrderModel order) throws EarthLoyaltyException;



}
