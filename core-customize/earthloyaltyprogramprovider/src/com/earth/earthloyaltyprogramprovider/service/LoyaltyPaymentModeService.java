/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;


/**
 *
 */
public interface LoyaltyPaymentModeService
{
	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentMode(String loyaltyPaymentModeTypeCode);

	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentMode(LoyaltyPaymentModeType loyaltyPaymentModeType);

	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentModeByStore(LoyaltyPaymentModeType loyaltyPaymentModeType,
			BaseStoreModel baseStoreModel);


	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentModeByCurrentBaseStore(
			LoyaltyPaymentModeType loyaltyPaymentModeType);


	public List<LoyaltyPaymentModeModel> getSupportedLoyaltyPaymentModes(BaseStoreModel baseStoreModel);

	public List<LoyaltyPaymentModeModel> getSupportedLoyaltyPaymentModesCurrentBaseStore();

}
