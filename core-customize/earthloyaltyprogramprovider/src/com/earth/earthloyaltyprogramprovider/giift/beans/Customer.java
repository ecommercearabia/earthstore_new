package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.earth.earthloyaltyprogramprovider.giift.enums.CustomerType;
import com.earth.earthloyaltyprogramprovider.giift.enums.Gender;
import com.google.gson.annotations.SerializedName;

public class Customer {

	@SerializedName("CustomerID")
	private String id;

	@SerializedName("Type")
	private CustomerType type;

	@SerializedName("Name")
	private String name;

	@SerializedName("MobileNumber")
	private String mobileNumber;

	@SerializedName("Province")
	private String province;

	@SerializedName("Gender")
	private Gender gender;

	@SerializedName("Age")
	private String age;

	@SerializedName("Occupation")
	private String occupation;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(final String province) {
		this.province = province;
	}

	public Gender getGender()
	{
		return gender;
	}

	public void setGender(final Gender gender)
	{
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(final String age) {
		this.age = age;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(final String occupation) {
		this.occupation = occupation;
	}

	public CustomerType getType() {
		return type;
	}

	public void setType(final CustomerType type) {
		this.type = type;
	}

}
