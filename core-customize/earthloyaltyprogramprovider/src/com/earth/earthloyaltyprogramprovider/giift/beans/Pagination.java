package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class Pagination {

	@SerializedName("PageSize")
	private int pageSize;

	@SerializedName("PageIndex")
	private int pageIndex;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(final int pageIndex) {
		this.pageIndex = pageIndex;
	}

}
