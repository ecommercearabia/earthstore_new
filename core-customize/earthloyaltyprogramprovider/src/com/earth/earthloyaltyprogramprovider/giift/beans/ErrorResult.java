package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResult {

	@SerializedName("ErrorCode")
	private String code;

	@SerializedName("ErrorType")
	private String type;

	@SerializedName("ErrorMessage")
	private String message;

	@SerializedName("ErrorTrace")
	private String trace;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(final String trace) {
		this.trace = trace;
	}



}
