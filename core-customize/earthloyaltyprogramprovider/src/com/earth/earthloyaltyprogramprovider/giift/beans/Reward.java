package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class Reward {

	@SerializedName("RewardID")
	private String id;

	@SerializedName("Value")
	private double value;

	@SerializedName("MinimalAmount")
	private double minimalAmount;

	@SerializedName("IssueAt")
	private String issueTime;

	@SerializedName("Expire")
	private String expireTime;

	@SerializedName("RewardType")
	private String rewardType;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public double getValue()
	{
		return value;
	}

	public void setValue(final double value)
	{
		this.value = value;
	}

	public double getMinimalAmount()
	{
		return minimalAmount;
	}

	public void setMinimalAmount(final double minimalAmount)
	{
		this.minimalAmount = minimalAmount;
	}

	public String getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(final String issueTime) {
		this.issueTime = issueTime;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(final String expireTime) {
		this.expireTime = expireTime;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(final String rewardType) {
		this.rewardType = rewardType;
	}

}
