package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class CustomerPointSummary {

	@SerializedName("CustomerID")
	private String customerID;

	@SerializedName("UserCardID")
	private String userCardID;

	@SerializedName("Points")
	private double points;

	@SerializedName("Value")
	private double value;

	@SerializedName("PointExchangeRate")
	private double pointExchangeRate;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(final String customerID) {
		this.customerID = customerID;
	}

	public String getUserCardID() {
		return userCardID;
	}

	public void setUserCardID(final String userCardID) {
		this.userCardID = userCardID;
	}

	public double getPoints() {
		return points;
	}

	public void setPoints(final double points) {
		this.points = points;
	}

	public double getValue() {
		return value;
	}

	public void setValue(final double value) {
		this.value = value;
	}

	public double getPointExchangeRate() {
		return pointExchangeRate;
	}

	public void setPointExchangeRate(final double pointExchangeRate) {
		this.pointExchangeRate = pointExchangeRate;
	}

}
