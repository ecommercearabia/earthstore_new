package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class MerchantAuthCodeResponse {
	@SerializedName("ResultCode")
	private String code;

	@SerializedName("Result")
	private MerchantAuthCodeResponseData result;

	@SerializedName("ErrorResult")
	private ErrorResult errorResult;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public MerchantAuthCodeResponseData getResult() {
		return result;
	}

	public void setResult(final MerchantAuthCodeResponseData result) {
		this.result = result;
	}

	public ErrorResult getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(final ErrorResult errorResult) {
		this.errorResult = errorResult;
	}

}
