/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.giift.beans;

/**
 *
 */
public class GiiftCredential
{
	private String baseURL;
	private String key;
	private String vector;
	private String merchantId;

	/**
	 * @return the baseURL
	 */
	public String getBaseURL()
	{
		return baseURL;
	}

	/**
	 * @param baseURL
	 *           the baseURL to set
	 */
	public void setBaseURL(final String baseURL)
	{
		this.baseURL = baseURL;
	}

	/**
	 * @return the key
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * @param key
	 *           the key to set
	 */
	public void setKey(final String key)
	{
		this.key = key;
	}

	/**
	 * @return the vector
	 */
	public String getVector()
	{
		return vector;
	}

	/**
	 * @param vector
	 *           the vector to set
	 */
	public void setVector(final String vector)
	{
		this.vector = vector;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId()
	{
		return merchantId;
	}

	/**
	 * @param merchantId
	 *           the merchantId to set
	 */
	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}

}
