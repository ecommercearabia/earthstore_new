package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.earth.earthloyaltyprogramprovider.giift.enums.CampaignType;
import com.google.gson.annotations.SerializedName;

public class Campaign {

	@SerializedName("CampaignID")
	private String id;

	@SerializedName("CampaignName")
	private String name;

	@SerializedName("CampaignURL")
	private String url;

	@SerializedName("CampaignType")
	private CampaignType type;

	@SerializedName("HasCampaign")
	private boolean hasCampaign;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public CampaignType getType() {
		return type;
	}

	public void setType(final CampaignType type) {
		this.type = type;
	}

	public boolean isHasCampaign() {
		return hasCampaign;
	}

	public void setHasCampaign(final boolean hasCampaign) {
		this.hasCampaign = hasCampaign;
	}

}
