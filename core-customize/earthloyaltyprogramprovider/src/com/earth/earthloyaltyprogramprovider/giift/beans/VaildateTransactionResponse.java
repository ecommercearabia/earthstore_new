package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VaildateTransactionResponse {

	@SerializedName("InitialAmount")
	private double initialAmount;

	@SerializedName("CustomerID")
	private String customerId;

	@SerializedName("FinalAmount")
	private double finalAmount;

	@SerializedName("Reward")
	@Expose
	private Reward reward;

	@SerializedName("Points")
	private Points points;

	@SerializedName("ValidateID")
	private String validateID;


	public double getInitialAmount()
	{
		return initialAmount;
	}

	public void setInitialAmount(final double initialAmount)
	{
		this.initialAmount = initialAmount;
	}

	public double getFinalAmount()
	{
		return finalAmount;
	}

	public void setFinalAmount(final double finalAmount)
	{
		this.finalAmount = finalAmount;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(final Reward reward) {
		this.reward = reward;
	}

	public Points getPoints() {
		return points;
	}

	public void setPoints(final Points points) {
		this.points = points;
	}

	public String getValidateID() {
		return validateID;
	}

	public void setValidateID(final String validateID) {
		this.validateID = validateID;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(final String customerId) {
		this.customerId = customerId;
	}



}
