package com.earth.earthloyaltyprogramprovider.giift.enums;

import com.google.gson.annotations.SerializedName;

public enum Gender {

	@SerializedName("Male")
	MALE("Male"),
	@SerializedName("Female")
	FEMALE("Female");

	private Gender(String value) {
		this.value = value;
	}

	private final String value;

	public String getValue() {
		return value;
	}
}
