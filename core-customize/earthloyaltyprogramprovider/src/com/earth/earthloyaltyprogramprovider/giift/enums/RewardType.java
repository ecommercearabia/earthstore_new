package com.earth.earthloyaltyprogramprovider.giift.enums;

public enum RewardType {

	RED_PACKET("RedPacket"), COUPON("Coupon");

	private String name;

	private RewardType(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
