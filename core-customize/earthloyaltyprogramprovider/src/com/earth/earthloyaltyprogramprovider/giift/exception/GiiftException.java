package com.earth.earthloyaltyprogramprovider.giift.exception;

import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.exception.type.EarthLoyaltyExceptionType;
import com.earth.earthloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;


public class GiiftException extends EarthLoyaltyException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 73715348954434564L;

	private final GiiftExceptionType type;


	public GiiftException(final GiiftExceptionType type, final String msg, final Object data)
	{
		super(msg, data, EarthLoyaltyExceptionType.GIIFT);
		this.type = type;

	}

	public GiiftExceptionType getType()
	{
		return type;
	}


}
