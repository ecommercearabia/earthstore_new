package com.earth.earthloyaltyprogramprovider.giift.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantAuthCodeResponse;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantAuthCodeResponseData;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantData;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantDataResponse;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantTier;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantTiersResponse;
import com.earth.earthloyaltyprogramprovider.giift.beans.QueryMerchantCampaignStatusData;
import com.earth.earthloyaltyprogramprovider.giift.beans.QueryMerchantCampaignStatusResponse;
import com.earth.earthloyaltyprogramprovider.giift.constant.GiiftConstants;
import com.earth.earthloyaltyprogramprovider.giift.exception.GiiftException;
import com.earth.earthloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;
import com.earth.earthloyaltyprogramprovider.giift.service.GiiftMerchantService;
import com.earth.earthloyaltyprogramprovider.giift.util.GiiftWebServiceApiUtil;


@Service
public class GiiftMerchantServiceImpl implements GiiftMerchantService
{

	private static final Logger LOG = LoggerFactory.getLogger(GiiftMerchantServiceImpl.class);

	private static final String URL_QUERY_MERCHANT_TIERS = "/api/Merchant/QueryMerchantTiers";
	private static final String URL_MERCHANT_CREATE = "/api/Merchant/Create";
	private static final String URL_MERCHANT_QUERY = "/api/Merchant/Query";
	private static final String URL_MERCHANT_UPDATE = "/api/Merchant/Update";
	private static final String URL_CAMPAIGN_QUERY_MERCHANT_CAMPAIGN_STATUS = "/api/Campaign/QueryMerchantCampaignStatus";
	private static final String URL_ACCOUNT_QUERY_MERCHANT_AUTH_CODE = "/api/Account/MerchantAuthCode";

	private static final String CONSTANT_MERCHANTID = "MerchantID";
	private static final String CONSTANT_TILLNUMBER = "TillNumber";
	private static final String CONSTANT_MERCHANTNAME = "MerchantName";
	private static final String CONSTANT_MERCHANTTYPE = "MerchantType";
	private static final String CONSTANT_CATEGORY = "Category";
	private static final String CONSTANT_PROVINCE = "Province";
	private static final String CONSTANT_ADDRESS = "Address";
	private static final String CONSTANT_PHONENUMBER = "PhoneNumber";
	private static final String CONSTANT_EMAIL = "Email";
	private static final String CONSTANT_CONTACTPERSON = "ContactPerson";
	private static final String CONSTANT_CONTACTPERSONPHONENUMBER = "ContactPersonPhoneNumber";
	private static final String CONSTANT_PARENTID = "ParentID";
	private static final String CONSTANT_TIER = "Tier";
	private static final String CONSTANT_DISCOUNT = "Discount";

	@Override
	public Optional<List<MerchantTier>> retrieveMerchantTiers(final String key, final String vector) throws GiiftException
	{
		LOG.debug("Start retrieveMerchantTiers");
		LOG.debug("send request starts");
		final MerchantTiersResponse response = GiiftWebServiceApiUtil
				.httpPOST(GiiftConstants.BASE_URL.concat(URL_QUERY_MERCHANT_TIERS), null, key, vector, MerchantTiersResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());

	}

	@Override
	public Optional<MerchantData> merchantOnboarding(final String key, final String vector, final MerchantData merchant)
			throws GiiftException
	{

		final Map<String, Object> params = prepareMerchantOnBoardingData(merchant);

		final MerchantDataResponse response = GiiftWebServiceApiUtil.httpPOST(GiiftConstants.BASE_URL.concat(URL_MERCHANT_CREATE),
				params, key, vector, MerchantDataResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());
	}

	private static Map<String, Object> prepareMerchantOnBoardingData(final MerchantData merchant)
	{
		final Map<String, Object> result = new LinkedHashMap<>();
		result.put(CONSTANT_MERCHANTID, merchant.getMerchantID());
		result.put(CONSTANT_TILLNUMBER, merchant.getTillNumber());
		result.put(CONSTANT_MERCHANTNAME, merchant.getMerchantName());
		result.put(CONSTANT_MERCHANTTYPE, merchant.getMerchantType());
		result.put(CONSTANT_CATEGORY, merchant.getCategory());
		result.put(CONSTANT_PROVINCE, merchant.getProvince());
		result.put(CONSTANT_ADDRESS, merchant.getAddress());
		result.put(CONSTANT_PHONENUMBER, merchant.getPhoneNumber());
		result.put(CONSTANT_EMAIL, merchant.getEmail());
		result.put(CONSTANT_CONTACTPERSON, merchant.getContactPerson());
		result.put(CONSTANT_CONTACTPERSONPHONENUMBER, merchant.getContactPersonPhoneNumber());
		result.put(CONSTANT_PARENTID, merchant.getParentID());
		result.put(CONSTANT_TIER, merchant.getTier());
		result.put(CONSTANT_DISCOUNT, merchant.getDiscount());
		return result;
	}

	@Override
	public Optional<MerchantData> retrieveMerchantInformation(final String key, final String vector, final String merchantID)
			throws GiiftException
	{

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CONSTANT_MERCHANTID, merchantID);

		final MerchantDataResponse response = GiiftWebServiceApiUtil.httpPOST(GiiftConstants.BASE_URL.concat(URL_MERCHANT_QUERY),
				params, key, vector, MerchantDataResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());
	}

	@Override
	public Optional<MerchantData> updateMerchant(final String key, final String vector, final MerchantData merchant)
			throws GiiftException
	{

		final Map<String, Object> params = prepareMerchantOnBoardingData(merchant);

		final MerchantDataResponse response = GiiftWebServiceApiUtil.httpPOST(GiiftConstants.BASE_URL.concat(URL_MERCHANT_UPDATE),
				params, key, vector, MerchantDataResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());
	}

	@Override
	public Optional<QueryMerchantCampaignStatusData> checkRewardsProgramStatus(final String key, final String vector,
			final String merchantID) throws GiiftException
	{

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CONSTANT_MERCHANTID, merchantID);

		final QueryMerchantCampaignStatusResponse response = GiiftWebServiceApiUtil.httpPOST(
				GiiftConstants.BASE_URL.concat(URL_CAMPAIGN_QUERY_MERCHANT_CAMPAIGN_STATUS), null, key, vector,
				QueryMerchantCampaignStatusResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());
	}

	@Override
	public Optional<MerchantAuthCodeResponseData> retrieveMerchantAccountAuthorizationCode(final String key, final String vector,
			final String merchantID) throws GiiftException
	{

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CONSTANT_MERCHANTID, merchantID);

		final MerchantAuthCodeResponse response = GiiftWebServiceApiUtil.httpPOST(
				GiiftConstants.BASE_URL.concat(URL_ACCOUNT_QUERY_MERCHANT_AUTH_CODE), null, key, vector,
				MerchantAuthCodeResponse.class);

		if (response.getCode().equals("0"))
		{
			final GiiftExceptionType giiftExceptionType = GiiftExceptionType
					.getExceptionFromCode(response.getErrorResult().getCode());
			final GiiftException ex = new GiiftException(giiftExceptionType, giiftExceptionType.getMsg(), null);

			LOG.error(response.getErrorResult().getMessage());
			throw ex;
		}

		return Optional.ofNullable(response.getResult());
	}

}
