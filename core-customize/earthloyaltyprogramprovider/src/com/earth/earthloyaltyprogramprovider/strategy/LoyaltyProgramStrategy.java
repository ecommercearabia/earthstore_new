/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;

/**
 *
 */
public interface LoyaltyProgramStrategy
{

	public boolean isRegister(CustomerModel customer, LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public boolean registerCustomer(CustomerModel customer, LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<LoyaltyUsablePoints> getUsablePoints(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public Optional<ValidateTransactionResult> validateTransactionByOrder(AbstractOrderModel order,
			LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public boolean createTransaction(AbstractOrderModel order, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyPagination pagination,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public boolean cancelTranscation(AbstractOrderModel order, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	public void reserve(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel loyaltyProgramProviderModel) throws EarthLoyaltyException;

	/**
	 *
	 */
	Optional<ValidateTransactionResult> validateTransaction(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;


}
