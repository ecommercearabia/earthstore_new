/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthloyaltyprogramprovider.beans.CustomerHistory;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.exception.type.EarthLoyaltyExceptionType;
import com.earth.earthloyaltyprogramprovider.giift.beans.Customer;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerPointLogs;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerPointSummary;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.earth.earthloyaltyprogramprovider.giift.beans.ExchangeRate;
import com.earth.earthloyaltyprogramprovider.giift.beans.GiiftCredential;
import com.earth.earthloyaltyprogramprovider.giift.beans.Log;
import com.earth.earthloyaltyprogramprovider.giift.beans.Pagination;
import com.earth.earthloyaltyprogramprovider.giift.beans.Points;
import com.earth.earthloyaltyprogramprovider.giift.beans.Transaction;
import com.earth.earthloyaltyprogramprovider.giift.beans.TransactionResponse;
import com.earth.earthloyaltyprogramprovider.giift.beans.VaildateTransactionResponse;
import com.earth.earthloyaltyprogramprovider.giift.enums.CancelStatusOption;
import com.earth.earthloyaltyprogramprovider.giift.exception.GiiftException;
import com.earth.earthloyaltyprogramprovider.giift.service.GiiftCustomerService;
import com.earth.earthloyaltyprogramprovider.model.GiiftLoyaltyProgramProviderModel;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyValidationService;
import com.earth.earthloyaltyprogramprovider.strategy.LoyaltyProgramStrategy;
import com.google.common.base.Preconditions;




/**
 *
 */
public class DefaultGiiftLoyaltyStrategy implements LoyaltyProgramStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultGiiftLoyaltyStrategy.class);
	private static final String LOYALTY_PROGRAM_PROVIDER_NULL_MSG = "loyalty program provider is null";
	private static final String ABSTRACT_ORDER_NULL_MSG = "order is null";
	private static final String CUSTOMER_ID_EMPTY_MSG = "customerId  is null or empty";
	private static final String TRANSACTION_VAILDATE_ID = "transactionVaildateId  is null or empty";
	private static final String BASE_STORE_NULL_MSG = "baseStore  is null";

	private static final String CUSTOMER_NULL_MSG = "customer  is null";
	private static final String LOG_ERROR_MSG = "[DefaultGiiftLoyaltyStrategy]: {}";


	@Resource(name = "giiftCustomerService")
	private GiiftCustomerService giiftCustomerService;

	@Resource(name = "giiftCustomerConverter")
	private Converter<CustomerModel, Customer> customerConverter;

	@Resource(name = "giiftTransactionConverter")
	private Converter<AbstractOrderModel, Transaction> transactionConverter;

	@Resource(name = "giiftCustomerHistoryLogConverter")
	private Converter<Log, CustomerHistory> customerHistoryLogConverter;

	@Resource(name = "loyaltyValidationService")
	private LoyaltyValidationService loyaltyValidationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;


	@Override
	public boolean isRegister(final CustomerModel customer, final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final Customer giiftCustomer = customerConverter.convert(customer);
		return giiftCustomerService.isCustomerRegistered(getGiiftCredential(giiftProvider), giiftCustomer.getId());
	}

	/**
	 *
	 */
	private GiiftCredential getGiiftCredential(final GiiftLoyaltyProgramProviderModel giiftProvider)
	{
		final GiiftCredential giiftCredential = new GiiftCredential();
		giiftCredential.setBaseURL(giiftProvider.getBaseUrl());
		giiftCredential.setKey(giiftProvider.getKey());
		giiftCredential.setVector(giiftProvider.getVector());
		giiftCredential.setMerchantId(giiftProvider.getMerchantId());

		return giiftCredential;

	}

	@Override
	public boolean registerCustomer(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final Customer giiftCustomer = customerConverter.convert(customer);

		return giiftCustomerService.registerCustomer(getGiiftCredential(giiftProvider), giiftCustomer);
	}

	@Override
	public Optional<LoyaltyUsablePoints> getUsablePoints(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);


		final Transaction transaction = transactionConverter.convert(order);
		transaction.setInitialAmount(orderTotalAmount);
		final Optional<Points> usablePoints = giiftCustomerService.getUsablePoints(getGiiftCredential(giiftProvider), transaction);
		final Optional<ExchangeRate> pointExchangeRate = giiftCustomerService
				.getPointExchangeRate(getGiiftCredential(giiftProvider));


		if (usablePoints.isEmpty())
		{
			return Optional.empty();
		}
		if (pointExchangeRate.isPresent())
		{
			order.setLoyaltyExchangeRate(pointExchangeRate.get().getPointValue());
		}

		validateUsablePoints(order, giiftProvider, usablePoints.get());
		modelService.save(order);
		return Optional.ofNullable(getLoyaltyUsablePoints(usablePoints.get()));
	}

	private void validateUsablePoints(final AbstractOrderModel order, final GiiftLoyaltyProgramProviderModel giiftProvider,
			final Points usablePoints) throws EarthLoyaltyException
	{
		final Optional<LoyaltyBalance> loyaltyBalance = getLoyaltyBalance((CustomerModel) order.getUser(), giiftProvider);

		if (loyaltyBalance.isPresent() && equalToZero(usablePoints.getRedeemAmount()) && order.getLoyaltyAmount() > 0.0
				&& loyaltyBalance.get().getPoints() > order.getLoyaltyAmount())
		{
			order.setUsableLoyaltyRedeemPoints(loyaltyBalance.get().getPoints() - order.getRedeemedLoyaltyPoints());
			order.setUsableLoyaltyRedeemAmount(loyaltyBalance.get().getValue() - order.getLoyaltyAmount());
		}
		else if (equalToZero(order.getTotalPrice().doubleValue()) && order.getLoyaltyAmount() > 0)
		{
			order.setUsableLoyaltyRedeemAmount(0d);
			order.setUsableLoyaltyRedeemPoints(0d);
		}
		else
		{
			order.setUsableLoyaltyRedeemAmount(usablePoints.getRedeemValue());
			order.setUsableLoyaltyRedeemPoints(usablePoints.getRedeemAmount());
		}
	}

	private boolean equalToZero(final double value)
	{
		final double epsilon = 0.0000001;
		return value < epsilon;
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransaction(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);


		if (Strings.isNotBlank(order.getGiiftValidateId()))
		{
			cancelTranscation(order, giiftProvider);
		}

		Optional<VaildateTransactionResponse> validateTransaction;
		final Transaction transaction = transactionConverter.convert(order);
		transaction.setInitialAmount(orderTotalAmount);
		try
		{
			validateTransaction = giiftCustomerService.validateTransaction(getGiiftCredential(giiftProvider), transaction);
		}
		catch (final GiiftException e)
		{
			order.setExpectedAddedPoint(0.0);
			modelService.save(order);
			throw new EarthLoyaltyException(e.getMessage(), e.getData(), EarthLoyaltyExceptionType.GIIFT);
		}
		if (validateTransaction.isEmpty() || validateTransaction.get().getPoints() == null)
		{

			return Optional.empty();
		}

		order.setGiiftValidateId(validateTransaction.get().getValidateID());
		final Points points = validateTransaction.get().getPoints();
		order.setExpectedAddedPoint(points.getIssueAmount());
		order.setExpectedLoyaltyRedeemAmount(points.getRedeemValue());
		order.setRedeemedLoyaltyPoints(points.getRedeemAmount());
		order.setLoyaltyAmount(points.getRedeemValue());
		modelService.save(order);
		return Optional.ofNullable(getValidateTransactionResult(validateTransaction.get()));
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransactionByOrder(final AbstractOrderModel order,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{

		return validateTransaction(order, order.getTotalPrice(), provider);
	}

	@Override
	public boolean createTransaction(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);



		final Optional<TransactionResponse> createTransaction;
		final Optional<ValidateTransactionResult> validateTransaction = validateTransaction(order,
				order.getTotalPrice()
						+ (order.getExpectedLoyaltyRedeemAmount() != null ? order.getExpectedLoyaltyRedeemAmount() : 0d),
				giiftProvider);
		if (validateTransaction.isEmpty())
		{
			return false;
		}
		final ValidateTransactionResult validateTransactionResult = validateTransaction.get();
		order.setTotalPriceAfterRedeem(validateTransactionResult.getFinalAmount());
		if (!Objects.isNull(validateTransactionResult.getPoints()))
		{
			order.setRedeemedLoyaltyPoints(validateTransactionResult.getPoints().getRedeemAmount());
			order.setExpectedLoyaltyRedeemAmount(validateTransactionResult.getPoints().getRedeemValue());
		}
		order.setGiiftValidateId(validateTransactionResult.getValidateID());
		final Transaction transaction = transactionConverter.convert(order);
		transaction.setInitialAmount(order.getTotalPrice()
				+ (order.getExpectedLoyaltyRedeemAmount() != null ? order.getExpectedLoyaltyRedeemAmount() : 0d));
		try
		{
			createTransaction = giiftCustomerService.createTransaction(getGiiftCredential(giiftProvider), transaction);
		}
		catch (final GiiftException e)
		{
			order.setLoyaltyFailureReason(e.getMessage());
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.FAILED);
			modelService.save(order);
			throw e;
		}

		if (createTransaction.isPresent())
		{
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.SUCCESS);
			modelService.save(order);
			return true;
		}
		return false;
	}

	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(final CustomerModel customer, final LoyaltyPagination pagination,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Optional<CustomerPointLogs> customerPointLogs = Optional.empty();
		final Customer giiftCustomer = customerConverter.convert(customer);

		Optional<LoyaltyBalance> loyaltyBalance = Optional.empty();
		try
		{
			loyaltyBalance = getLoyaltyBalance(customer, giiftProvider);
		}
		catch (final EarthLoyaltyException ex)
		{
			LOG.warn(ex.getMessage());
		}
		try
		{
		customerPointLogs = giiftCustomerService.getCustomerPointLogs(getGiiftCredential(giiftProvider), giiftCustomer.getId(),
				getGiiftPagintation(pagination));
	}
		catch (final EarthLoyaltyException ex)
		{
			LOG.warn(ex.getMessage());
		}

		final LoyaltyCustomerInfo loyaltyCustomerInfoBean = getLoyaltyCustomerInfoBean(customerPointLogs, loyaltyBalance);
		loyaltyCustomerInfoBean.setCustomerId(giiftCustomer.getId());
		return Optional.ofNullable(loyaltyCustomerInfoBean);

	}

	/**
	 *
	 */
	private Pagination getGiiftPagintation(final LoyaltyPagination pagination)
	{
		if (pagination == null)
		{
			return null;
		}
		final Pagination giifPagination = new Pagination();
		giifPagination.setPageIndex(pagination.getPageIndex());
		giifPagination.setPageSize(pagination.getPageSize());
		return giifPagination;
	}

	/**
	 *
	 */
	private LoyaltyCustomerInfo getLoyaltyCustomerInfoBean(final Optional<CustomerPointLogs> customerPointLogs,
			final Optional<LoyaltyBalance> loyaltyBalance)
	{
		final LoyaltyCustomerInfo customerInfo = new LoyaltyCustomerInfo();
		if (loyaltyBalance.isPresent())
		{
			customerInfo.setBalance(loyaltyBalance.get());
		}

		if (customerPointLogs.isPresent())
		{
			final CustomerPointLogs pointLogs = customerPointLogs.get();
			final List<Log> logs = pointLogs.getLogs();
			customerInfo.setHistories(customerHistoryLogConverter.convertAll(logs));
			customerInfo.setPageIndex(pointLogs.getPageIndex());
			customerInfo.setPageSize(pointLogs.getPageSize());
			customerInfo.setTotalCount(pointLogs.getTotalCount());
			customerInfo.setTotalPage(pointLogs.getTotalPage());

		}

		return customerInfo;
	}

	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(final CustomerModel customer,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);

		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Optional<CustomerQRCode> customerQRCode;
		final Customer giiftCustomer = customerConverter.convert(customer);

		customerQRCode = giiftCustomerService.getCustomerQRCode(getGiiftCredential(giiftProvider), giiftCustomer.getId());


		return Optional.ofNullable(getCustomerCode(customerQRCode));
	}


	/**
	 *
	 */
	private LoyaltyCustomerCode getCustomerCode(final Optional<CustomerQRCode> customerQRCode)
	{
		final LoyaltyCustomerCode customerCode = new LoyaltyCustomerCode();
		if (customerQRCode.isPresent())
		{
			customerCode.setCustomerCode(customerQRCode.get().getCustomerCode());
			customerCode.setCustomerID(customerQRCode.get().getCustomerID());
			customerCode.setQrCode(customerQRCode.get().getQrCode());
		}
		return customerCode;

	}

	private GiiftLoyaltyProgramProviderModel getGiiftProvider(final LoyaltyProgramProviderModel providerModel)
	{
		if (!(providerModel instanceof GiiftLoyaltyProgramProviderModel))
		{
			throw new NotSupportedException("");
		}
		return (GiiftLoyaltyProgramProviderModel) providerModel;
	}

	protected LoyaltyUsablePoints getLoyaltyUsablePoints(final Points points)
	{
		final LoyaltyUsablePoints loyaltyUsablePoints = new LoyaltyUsablePoints();
		loyaltyUsablePoints.setBalancePoints(points.getBalancePoints());
		loyaltyUsablePoints.setIssueAmount(points.getIssueAmount());
		loyaltyUsablePoints.setIssueValue(points.getIssueValue());
		loyaltyUsablePoints.setRedeemAmount(points.getRedeemAmount());
		loyaltyUsablePoints.setRedeemValue(points.getRedeemValue());
		return loyaltyUsablePoints;
	}

	protected ValidateTransactionResult getValidateTransactionResult(final VaildateTransactionResponse response)
	{
		final ValidateTransactionResult result = new ValidateTransactionResult();
		result.setCustomerId(response.getCustomerId());
		result.setFinalAmount(response.getFinalAmount());
		result.setInitialAmount(response.getInitialAmount());
		result.setValidateID(response.getValidateID());
		result.setPoints(getLoyaltyUsablePoints(response.getPoints()));
		return result;
	}

	@Override
	public boolean cancelTranscation(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(order.getGiiftValidateId()), TRANSACTION_VAILDATE_ID);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);

		try
		{
			final boolean cancelTransaction = giiftCustomerService.cancelTransaction(getGiiftCredential(giiftProvider),
					order.getGiiftValidateId(), CancelStatusOption.CANCEL);

			if (cancelTransaction)
			{
				order.setGiiftValidateId(Strings.EMPTY);
				modelService.save(order);
			}

			return cancelTransaction;

		}
		catch (final GiiftException e)
		{
			LOG.error(LOG_ERROR_MSG, e.getMessage());
			return false;
		}

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final Customer giiftCustomer = customerConverter.convert(customer);

		final Optional<CustomerPointSummary> customerPointSummary = giiftCustomerService
				.getCustomerPointSummary(getGiiftCredential(giiftProvider), giiftCustomer.getId());

		return customerPointSummary.isPresent() ? Optional.ofNullable(getBalanceBean(customerPointSummary.get()))
				: Optional.empty();
	}

	/**
	 *
	 */
	private LoyaltyBalance getBalanceBean(final CustomerPointSummary customerPointSummary)
	{
		final LoyaltyBalance balance = new LoyaltyBalance();
		balance.setPoints(customerPointSummary.getPoints());
		balance.setValue(customerPointSummary.getValue());
		balance.setPointExchangeRate(customerPointSummary.getPointExchangeRate());
		return balance;
	}

	@Override
	public void reserve(final AbstractOrderModel order, final double orderTotalAmount, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);

		if (LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType()))
		{
			return;
		}
		try
		{
			cancelTranscation(order, giiftProvider);
		}
		catch (final Exception ex)
		{
			LOG.warn(ex.getMessage());
		}

		validateAmount(order, orderTotalAmount, provider);

		validateTransaction(order, orderTotalAmount, giiftProvider);
		modelService.refresh(order);
		order.setLoyaltyPaymentType(LoyaltyPaymentType.AUTHORIZE);
		modelService.save(order);

	}

	private void validateAmount(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		final boolean isSpecific = (order.getLoyaltyPaymentMode() != null
				&& LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()));
		final boolean isFull = (order.getLoyaltyPaymentMode() != null
				&& LoyaltyPaymentModeType.REDEEM_FULL_AMOUNT.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()));

		final Optional<LoyaltyUsablePoints> usablePoints = getUsablePoints(order, orderTotalAmount, provider);
		if (usablePoints.isEmpty() || (isSpecific && equalToZero(order.getLoyaltyAmountSelected()))
				|| order.getLoyaltyPaymentMode() == null
				|| LoyaltyPaymentModeType.REDEEM_NONE.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()))
		{
			redeemNon(order);
		}
		else if ((order.getLoyaltyAmountSelected() >= usablePoints.get().getRedeemValue() && isSpecific)
				|| (isFull && equalToZero(order.getLoyaltyAmountSelected())))
		{
			redeemFull(order, usablePoints.get());
		}
		else if (isFull)
		{
			redeemFull(order, usablePoints.get());
		}
	}

	private void redeemNon(final AbstractOrderModel order)
	{
		order.setLoyaltyAmountSelected(0d);
		order.setLoyaltyAmount(0d);
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = loyaltyPaymentModeService
				.getLoyaltyPaymentModeByCurrentBaseStore(LoyaltyPaymentModeType.REDEEM_NONE);

		order.setLoyaltyPaymentMode(loyaltyPaymentMode.isPresent() ? loyaltyPaymentMode.get() : null);
	}

	private void redeemFull(final AbstractOrderModel order, final LoyaltyUsablePoints usablePoints)
	{
		order.setLoyaltyAmountSelected(usablePoints.getRedeemValue());
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = loyaltyPaymentModeService
				.getLoyaltyPaymentModeByCurrentBaseStore(LoyaltyPaymentModeType.REDEEM_FULL_AMOUNT);
		order.setLoyaltyPaymentMode(loyaltyPaymentMode.isPresent() ? loyaltyPaymentMode.get() : null);
	}

}
