/**
 *
 */
package com.earth.earthloyaltyprogramprovider.exception.type;

/**
 * @author yhammad
 *
 */
public enum LoyaltyPaymentExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
