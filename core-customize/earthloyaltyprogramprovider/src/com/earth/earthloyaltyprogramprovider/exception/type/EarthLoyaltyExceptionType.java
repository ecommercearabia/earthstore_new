/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.exception.type;

/**
 *
 */
public enum EarthLoyaltyExceptionType
{

	GIIFT("Giift Exception"), LOYALTY_IS_NOT_ENABLED("loyalty is not enabled"), CUSTOMER_NOT_INVOLVED(
			"customer is not involved in loyalty program");

	private final String msg;

	/**
	 *
	 */
	private EarthLoyaltyExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}



}
