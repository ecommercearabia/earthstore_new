/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.exception;

import com.earth.earthloyaltyprogramprovider.exception.type.EarthLoyaltyExceptionType;


/**
 *
 */
public class EarthLoyaltyException extends Exception
{
	private final Object data;

	private final EarthLoyaltyExceptionType exceptionType;

	/**
	 *
	 */
	public EarthLoyaltyException(final String msg, final Object data, final EarthLoyaltyExceptionType exceptionType)
	{
		super(msg);
		this.data = data;
		this.exceptionType = exceptionType;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @return the exceptionType
	 */
	public EarthLoyaltyExceptionType getExceptionType()
	{
		return exceptionType;
	}

}
