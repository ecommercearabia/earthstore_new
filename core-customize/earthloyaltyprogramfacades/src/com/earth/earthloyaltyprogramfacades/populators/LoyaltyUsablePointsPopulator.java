/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyUsablePointData;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyUsablePointsPopulator implements Populator<LoyaltyUsablePoints, LoyaltyUsablePointData>
{


	@Override
	public void populate(final LoyaltyUsablePoints source, final LoyaltyUsablePointData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setRedeemAmount(source.getRedeemValue());
		target.setRedeemPoints(source.getRedeemAmount());
	}
}
