/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyPointCartPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Resource(name = "loyaltyPaymentModeConverter")
	private Converter<LoyaltyPaymentModeModel, LoyaltyPaymentModeData> loyaltyPaymentModeConverter;



	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;


	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		if (source == null)
		{
			return;
		}
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setLoyaltyValueSelected(source.getLoyaltyAmountSelected());

		target.setLoyaltyAmount(
				getPriceData(BigDecimal.valueOf(source.getLoyaltyAmount()), PriceDataType.BUY, source.getCurrency()));


		if (!Objects.isNull(source.getLoyaltyPaymentMode()))
		{
			target.setLoyaltyPaymentMode(loyaltyPaymentModeConverter.convert(source.getLoyaltyPaymentMode()));
		}
		if (source.getExpectedLoyaltyRedeemAmount() != null)
		{

			target.setExpectedLoyaltyRedeemAmount(
					getPriceData(BigDecimal.valueOf(source.getExpectedLoyaltyRedeemAmount().doubleValue()), PriceDataType.BUY,
							source.getCurrency()));
		}

		target.setRedeemedLoyaltyPoints(source.getRedeemedLoyaltyPoints());
		if (source.getUsableLoyaltyRedeemAmount() != null)
		{

			target.setUsableLoyaltyRedeemAmount(getPriceData(
					BigDecimal.valueOf(source.getUsableLoyaltyRedeemAmount().doubleValue() + source.getLoyaltyAmountSelected()),
					PriceDataType.BUY, source.getCurrency()));
		}

		if (source.getUsableLoyaltyRedeemPoints() != null)
		{
			target.setUsableLoyaltyRedeemPoints(source.getUsableLoyaltyRedeemPoints() + source.getRedeemedLoyaltyPoints());
		}
		target.setTotalPriceAfterRedeem(source.getTotalPriceAfterRedeem());
	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}
}
