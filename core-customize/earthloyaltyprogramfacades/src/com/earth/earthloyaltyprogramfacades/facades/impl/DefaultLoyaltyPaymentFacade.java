/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramfacades.facades.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaginationData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyUsablePointData;
import com.earth.earthloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultLoyaltyPaymentFacade implements LoyaltyPaymentFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultLoyaltyPaymentFacade.class);
	private static final String CART_NULL_MSG = "Cart is null";
	private static final String CUSTOMER_NULL_MSG = "Customer is null";
	private static final String BASE_STORE_NULL_MSG = "baseStoreModel is null";


	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Resource(name = "loyaltyUsablePointsConverter")
	private Converter<LoyaltyUsablePoints, LoyaltyUsablePointData> loyaltyUsablePointsConverter;

	@Resource(name = "loyaltyBalanceConverter")
	private Converter<LoyaltyBalance, LoyaltyBalanceData> loyaltyBalanceConverter;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;


	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "loyaltyCustomerInfoConverter")
	private Converter<LoyaltyCustomerInfo, LoyaltyCustomerInfoData> loyaltyCustomerInfoConverter;

	@Resource(name = "loyaltyCustomerQrConverter")
	private Converter<LoyaltyCustomerCode, LoyaltyCustomerQrData> loyaltyCustomerQrConverter;


	@Override
	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePoints(final CartModel cart) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(cart), CART_NULL_MSG);
		if (!isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer())
		{
			return Optional.empty();
		}
		final Optional<LoyaltyUsablePoints> usablePoints = loyaltyProgramContext.getUsablePointsByOrder(cart);
		if (usablePoints.isEmpty())
		{
			LOG.error("usable point is empty");
			return Optional.empty();
		}

		return Optional.ofNullable(loyaltyUsablePointsConverter.convert(usablePoints.get()));
	}

	@Override
	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePointsByCurrentCart() throws EarthLoyaltyException
	{
		return getLoyaltyUsablePoints(cartService.getSessionCart());
	}

	@Override
	public Optional<LoyaltyBalanceData> getBalance(final CustomerModel customerModel, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(!Objects.isNull(customerModel), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);
		Optional<LoyaltyBalance> loyaltyBalance = Optional.empty();
		if (!isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer())
		{
			return Optional.empty();
		}
		try
		{
			loyaltyBalance = loyaltyProgramContext.getLoyaltyBalance(customerModel, baseStoreModel);
		}
		catch (final EarthLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}

		return loyaltyBalance.isPresent() ? Optional.ofNullable(loyaltyBalanceConverter.convert(loyaltyBalance.get()))
				: Optional.empty();

	}

	@Override
	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStore(final CustomerModel customerModel)
	{
		return getBalance(customerModel, baseStoreService.getCurrentBaseStore());

	}

	@Override
	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStoreAndCustomer()
	{
		return getBalanceByCurrentBaseStore(getCurrentCustomer());
	}



	private CustomerModel getCurrentCustomer()
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser instanceof CustomerModel)
		{
			return (CustomerModel) currentUser;
		}
		return null;
	}

	@Override
	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomer(final CustomerModel customer, final BaseStoreModel baseStoreModel,
			final LoyaltyPaginationData paginationData)
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);
		//	Preconditions.checkArgument(!Objects.isNull(paginationData), "PagintationData is null");
		if (!isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer())
		{
			return Optional.empty();
		}
		try
		{
			final Optional<LoyaltyCustomerInfo> loyaltyCustomerInfo = loyaltyProgramContext.getLoyaltyCustomerInfo(customer,
					getPagination(paginationData), baseStoreModel);

			return loyaltyCustomerInfo.isPresent()
					? Optional.ofNullable(loyaltyCustomerInfoConverter.convert(loyaltyCustomerInfo.get()))
					: Optional.empty();
		}
		catch (final EarthLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}
		return Optional.empty();

	}

	/**
	 *
	 */
	private LoyaltyPagination getPagination(final LoyaltyPaginationData paginationData)
	{
		if (paginationData == null)
		{
			return null;
		}

		final LoyaltyPagination pagination = new LoyaltyPagination();
		pagination.setPageIndex(paginationData.getPageIndex());
		pagination.setPageSize(paginationData.getPageSize());
		return pagination;
	}

	@Override
	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStore(final CustomerModel customer,
			final LoyaltyPaginationData paginationData)
	{

		return getLoyaltyCustomer(customer, baseStoreService.getCurrentBaseStore(), paginationData);
	}

	@Override
	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(
			final LoyaltyPaginationData paginationData)
	{
		return getLoyaltyCustomerByCurrentBaseStore(getCurrentCustomer(), paginationData);
	}

	@Override
	public boolean isEnabledForCustomer(final CustomerModel customer, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);

		return loyaltyProgramContext.isLoyaltyEnabled(baseStoreModel, customer);
	}

	@Override
	public boolean isEnabledForCustomerByCurrentBaseStore(final CustomerModel customer)
	{

		return isEnabledForCustomer(customer, baseStoreService.getCurrentBaseStore());
	}

	@Override
	public boolean isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer()
	{
		return isEnabledForCustomerByCurrentBaseStore(getCurrentCustomer());
	}

	@Override
	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCode(final CustomerModel customer,
			final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);
		Optional<LoyaltyCustomerCode> loyaltyCustomerCode = Optional.empty();
		if (!isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer())
		{
			return Optional.empty();
		}
		try
		{
			loyaltyCustomerCode = loyaltyProgramContext.getLoyaltyCustomerCode(customer, baseStoreModel);
		}
		catch (final EarthLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}
		return loyaltyCustomerCode.isPresent() ? Optional.ofNullable(loyaltyCustomerQrConverter.convert(loyaltyCustomerCode.get()))
				: Optional.empty();
	}

	@Override
	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStore(final CustomerModel customer)
	{

		return getLoyaltyCustomerQrCode(customer, baseStoreService.getCurrentBaseStore());
	}

	@Override
	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStoreAndCustomer()
	{

		return getLoyaltyCustomerQrCodeByCurrentBaseStore(getCurrentCustomer());
	}

	@Override
	public boolean isEnabledOnStore(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);

		return baseStoreModel.isLoyaltyProgramEnabled();
	}

	@Override
	public boolean isEnabledOnStoreByCurrentBaseStore()
	{
		return isEnabledOnStore(baseStoreService.getCurrentBaseStore());
	}

}
