/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthfulfillmentbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class EarthfulfillmentbackofficeConstants extends GeneratedEarthfulfillmentbackofficeConstants
{
	public static final String EXTENSIONNAME = "earthfulfillmentbackoffice";

	private EarthfulfillmentbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
