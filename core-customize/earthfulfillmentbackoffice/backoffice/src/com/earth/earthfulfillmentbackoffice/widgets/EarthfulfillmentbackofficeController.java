/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthfulfillmentbackoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.earth.earthfulfillmentbackoffice.services.EarthfulfillmentbackofficeService;


public class EarthfulfillmentbackofficeController extends DefaultWidgetController
{
	private static final long serialVersionUID = 1L;
	private Label label;

	@WireVariable
	private transient EarthfulfillmentbackofficeService earthfulfillmentbackofficeService;

	@Override
	public void initialize(final Component comp)
	{
		super.initialize(comp);
		label.setValue(earthfulfillmentbackofficeService.getHello() + " EarthfulfillmentbackofficeController");
	}
}
