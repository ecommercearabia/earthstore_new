/**
 *
 */
package com.earth.earthstorecredit.exception.enums;

/**
 * @author yhammad
 *
 */
public enum StoreCreditExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
