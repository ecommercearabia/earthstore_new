/**
 *
 */
package com.earth.earthstorecredit.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.earth.earthstorecredit.enums.StoreCreditModeType;
import com.earth.earthstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeService
{
	public StoreCreditModeModel getStoreCreditMode(String storeCreditModeTypeCode);

	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);

	public List<StoreCreditModeModel> getSupportedStoreCreditModes(BaseStoreModel baseStoreModel);

	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore();
}
