/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomaddon.jalo;

import com.earth.earthconsignmenttrackingcustomaddon.constants.EarthconsignmenttrackingcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthconsignmenttrackingcustomaddonManager extends GeneratedEarthconsignmenttrackingcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthconsignmenttrackingcustomaddonManager.class.getName() );
	
	public static final EarthconsignmenttrackingcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthconsignmenttrackingcustomaddonManager) em.getExtension(EarthconsignmenttrackingcustomaddonConstants.EXTENSIONNAME);
	}
	
}
