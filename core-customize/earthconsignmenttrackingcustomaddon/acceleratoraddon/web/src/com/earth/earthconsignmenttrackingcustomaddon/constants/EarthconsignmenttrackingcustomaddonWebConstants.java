/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomaddon.constants;

/**
 * Global class for all Earthconsignmenttrackingcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class EarthconsignmenttrackingcustomaddonWebConstants // NOSONAR
{
	private EarthconsignmenttrackingcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
