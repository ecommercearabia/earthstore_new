/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartheventtrackingwscustomaddon.constants;

/**
 * Global class for all Eartheventtrackingwscustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class EartheventtrackingwscustomaddonConstants extends GeneratedEartheventtrackingwscustomaddonConstants  //NOSONAR
{
	public static final String EXTENSIONNAME = "eartheventtrackingwscustomaddon";

	private EartheventtrackingwscustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
