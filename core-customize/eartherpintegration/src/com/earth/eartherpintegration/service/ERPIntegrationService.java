package com.earth.eartherpintegration.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.earth.eartherpintegration.beans.ERPItemBarcode;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.beans.ERPLocation;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.model.EarthERPProductProviderModel;


public interface ERPIntegrationService
{

	public List<ERPItemPrice> getItemPriceList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime, final String itemId) throws EarthERPException;

	public List<ERPItemPrice> getItemsPriceList(final EarthERPProductProviderModel earthERPProductProviderModel,
			String modifiedDateTime) throws EarthERPException;

	public List<ERPItemBarcode> getItemBarcodeList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime, final String itemId) throws EarthERPException;

	public List<ERPItemBarcode> getItemsBarcodeList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime) throws EarthERPException;

	public List<ERPItemInventory> getItemInventoryList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String inventoryLocation, final String itemId) throws EarthERPException;

	public List<ERPItemInventory> getItemsInventoryList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String inventoryLocation) throws EarthERPException;

	public List<ERPLocation> getLocations(final EarthERPProductProviderModel earthERPProductProviderModel)
			throws EarthERPException;


	public List<ERPItemPrice> getItemPriceList(final BaseStoreModel baseStoreModel, final String itemId) throws EarthERPException;

	public List<ERPItemPrice> getItemsPriceList(final BaseStoreModel baseStoreModel) throws EarthERPException;

	public List<ERPItemBarcode> getItemBarcodeList(final BaseStoreModel baseStoreModel, final String itemId)
			throws EarthERPException;

	public List<ERPItemBarcode> getItemsBarcodeList(final BaseStoreModel baseStoreModel) throws EarthERPException;

	public List<ERPItemInventory> getItemInventoryList(final BaseStoreModel baseStoreModel, String inventoryLocation,
			final String itemId) throws EarthERPException;

	public List<ERPItemInventory> getItemsInventoryList(final BaseStoreModel baseStoreModel, String inventoryLocation)
			throws EarthERPException;

	public List<ERPLocation> getLocations(final BaseStoreModel baseStoreModel) throws EarthERPException;

	public boolean sendSalesOrder(final BaseStoreModel baseStoreModel, final ConsignmentModel consignment)
			throws EarthERPException;

	public boolean sendSalesOrderByCurrentBaseStore(final ConsignmentModel consignment) throws EarthERPException;

}
