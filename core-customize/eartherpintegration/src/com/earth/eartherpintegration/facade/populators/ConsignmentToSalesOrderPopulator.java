/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.model.EarthERPProviderModel;
import com.earth.eartherpintegration.model.EarthERPSalesOrderProviderModel;
import com.earth.eartherpintegration.service.EarthERPProviderService;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderPopulator implements Populator<ConsignmentModel, ERPSalesOrder>
{

	@Resource(name = "erpConsignmentEntryToSalesLineConverter")
	Converter<ConsignmentEntryModel, ERPSalesLine> erpConsignmentEntryToSalesLineConverter;

	@Resource(name = "earthERPProviderService")
	private EarthERPProviderService earthERPProviderService;

	@Override
	public void populate(final ConsignmentModel source, final ERPSalesOrder target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "source Is null");
		Preconditions.checkNotNull(target, "target Is null");

		target.setDeliveryStatus(1);
		if (Objects.nonNull(source.getWarehouse()))
		{
			target.setStoreCode(source.getWarehouse().getErpLocationCode());

		}
		if (CollectionUtils.isNotEmpty(source.getConsignmentEntries()))
		{
			target.setNoOfItems(source.getConsignmentEntries().size());
			target.getSalesLines().addAll(getErpConsignmentEntryToSalesLineConverter().convertAll(source.getConsignmentEntries()));
		}
		if (Objects.isNull(source.getOrder()))
		{
			return;
		}

		final AbstractOrderModel order = source.getOrder();

		final Optional<EarthERPProviderModel> provider = getEarthERPProviderService().getActiveProvider(order.getStore(),
				EarthERPSalesOrderProviderModel.class);

		if (provider.isPresent())
		{
			target.setCompanyCode(provider.get().getCompanyCode());
		}

		target.setAmount(order.getTotalPrice());
		target.setAmountIncVat(order.getTotalTax());
		if (Objects.nonNull(order.getCurrency()))
		{
			target.setCurrencyCode(order.getCurrency().getIsocode());
		}
		if (Objects.nonNull(order.getDate()))
		{
			target.setOrderDate(getDate(order.getDate()));
		}
		target.setOrderNo(order.getCode());

	}

	/**
	 * @return the erpConsignmentEntryToSalesLineConverter
	 */
	protected Converter<ConsignmentEntryModel, ERPSalesLine> getErpConsignmentEntryToSalesLineConverter()
	{
		return erpConsignmentEntryToSalesLineConverter;
	}

	/**
	 * @return the earthERPProviderService
	 */
	protected EarthERPProviderService getEarthERPProviderService()
	{
		return earthERPProviderService;
	}

	protected String getDate(final Date date)
	{
		final SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return formater.format(date);
	}

}
