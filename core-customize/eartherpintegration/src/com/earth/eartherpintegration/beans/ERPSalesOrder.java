/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.beans;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ERPSalesOrder
{
	@SerializedName("Amount")
	@Expose
	private double amount;
	@SerializedName("AmountIncVat")
	@Expose
	private double amountIncVat;
	@SerializedName("CompanyCode")
	@Expose
	private String companyCode;
	@SerializedName("CurrencyCode")
	@Expose
	private String currencyCode;
	@SerializedName("DeliveryStatus")
	@Expose
	private int deliveryStatus;
	@SerializedName("OrderDate")
	@Expose
	private String orderDate;
	@SerializedName("OrderNo")
	@Expose
	private String orderNo;
	@SerializedName("StoreCode")
	@Expose
	private String storeCode;
	@SerializedName("NoOfItems")
	@Expose
	private int noOfItems;
	@SerializedName("SalesLine")
	@Expose
	private List<ERPSalesLine> salesLines = new ArrayList<>();

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public double getAmountIncVat()
	{
		return amountIncVat;
	}

	public void setAmountIncVat(final double amountIncVat)
	{
		this.amountIncVat = amountIncVat;
	}

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(final String companyCode)
	{
		this.companyCode = companyCode;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public int getDeliveryStatus()
	{
		return deliveryStatus;
	}

	public void setDeliveryStatus(final int deliveryStatus)
	{
		this.deliveryStatus = deliveryStatus;
	}

	public String getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	public String getOrderNo()
	{
		return orderNo;
	}

	public void setOrderNo(final String orderNo)
	{
		this.orderNo = orderNo;
	}

	public String getStoreCode()
	{
		return storeCode;
	}

	public void setStoreCode(final String storeCode)
	{
		this.storeCode = storeCode;
	}

	public int getNoOfItems()
	{
		return noOfItems;
	}

	public void setNoOfItems(final int noOfItems)
	{
		this.noOfItems = noOfItems;
	}

	public List<ERPSalesLine> getSalesLines()
	{
		return salesLines;
	}

	public void setSalesLines(final List<ERPSalesLine> salesLines)
	{
		this.salesLines = salesLines;
	}

	@Override
	public String toString()
	{
		return "SalesOrderBean [amount=" + amount + ", amountIncVat=" + amountIncVat + ", companyCode=" + companyCode
				+ ", currencyCode=" + currencyCode + ", deliveryStatus=" + deliveryStatus + ", orderDate=" + orderDate + ", orderNo="
				+ orderNo + ", storeCode=" + storeCode + ", noOfItems=" + noOfItems + ", salesLine=" + salesLines + "]";
	}
}
