package com.earth.eartherpintegration.beans;

import org.apache.logging.log4j.util.Strings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ERPItemPrice
{
	@SerializedName("Amount")
	@Expose
	private double amount;
	@SerializedName("FromDate")
	@Expose
	private String fromDate;
	@SerializedName("ItemId")
	@Expose
	private String itemId;
	@SerializedName("PriceGroup")
	@Expose
	private String priceGroup;
	@SerializedName("ToDate")
	@Expose
	private String toDate;
	@SerializedName("UnitId")
	@Expose
	private String unitId;

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(final String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getItemId()
	{
		return itemId;
	}

	public void setItemId(final String itemId)
	{
		this.itemId = itemId;
	}

	public String getPriceGroup()
	{
		return priceGroup;
	}

	public void setPriceGroup(final String priceGroup)
	{
		this.priceGroup = priceGroup;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(final String toDate)
	{
		this.toDate = toDate;
	}

	public String getUnitId()
	{
		return Strings.isBlank(unitId) ? Strings.EMPTY : unitId.toUpperCase();
	}

	public void setUnitId(final String unitId)
	{
		this.unitId = unitId;
	}
}
