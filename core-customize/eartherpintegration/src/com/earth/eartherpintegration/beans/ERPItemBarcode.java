package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ERPItemBarcode {

	@SerializedName("Barcode")
	@Expose
	private String barcode;
	@SerializedName("BarcodeUnitId")
	@Expose
	private String barcodeUnitId;
	@SerializedName("BaseUnitId")
	@Expose
	private String baseUnitId;
	@SerializedName("CompanyCode")
	@Expose
	private String companyCode;
	@SerializedName("ConsignmentItem")
	@Expose
	private int consignmentItem;
	@SerializedName("Description")
	@Expose
	private String description;
	@SerializedName("Factor")
	@Expose
	private double factor;
	@SerializedName("ItemId")
	@Expose
	private String itemId;
	@SerializedName("PLUCode")
	@Expose
	private String pLUCode;
	@SerializedName("ScaleItem")
	@Expose
	private int scaleItem;

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	public String getBarcodeUnitId() {
		return barcodeUnitId;
	}

	public void setBarcodeUnitId(final String barcodeUnitId) {
		this.barcodeUnitId = barcodeUnitId;
	}

	public String getBaseUnitId() {
		return baseUnitId;
	}

	public void setBaseUnitId(final String baseUnitId) {
		this.baseUnitId = baseUnitId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(final String companyCode) {
		this.companyCode = companyCode;
	}

	public int getConsignmentItem() {
		return consignmentItem;
	}

	public void setConsignmentItem(final int consignmentItem) {
		this.consignmentItem = consignmentItem;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(final double factor) {
		this.factor = factor;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(final String itemId) {
		this.itemId = itemId;
	}

	public String getPLUCode() {
		return pLUCode;
	}

	public void setPLUCode(final String pLUCode) {
		this.pLUCode = pLUCode;
	}

	public int getScaleItem() {
		return scaleItem;
	}

	public void setScaleItem(final int scaleItem) {
		this.scaleItem = scaleItem;
	}

}
