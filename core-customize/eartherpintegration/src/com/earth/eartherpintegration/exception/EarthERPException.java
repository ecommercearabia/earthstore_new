/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.exception;

import com.earth.eartherpintegration.exception.type.EarthERPExceptionType;

/**
 *
 */
public class EarthERPException extends Exception
{
	private final Object data;

	private final EarthERPExceptionType exceptionType;

	/**
	 *
	 */
	public EarthERPException(final String msg, final Object data, final EarthERPExceptionType exceptionType)
	{
		super(msg);
		this.data = data;
		this.exceptionType = exceptionType;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @return the exceptionType
	 */
	public EarthERPExceptionType getExceptionType()
	{
		return exceptionType;
	}

}
