/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.exception.type;

/**
 *
 */
public enum EarthERPExceptionType
{

	ERP_SERVER_ISSUE("erp server issue"), ERP_PARSING_ISSUE("erp parsing issue"), SEND_ORDER_ISSUE("send order issue");

	private final String msg;

	/**
	 *
	 */
	private EarthERPExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}



}
