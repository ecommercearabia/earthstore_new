/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.stock.StockService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.cronjobs.model.ERPProductCronJobModel;
import com.earth.earthcore.service.CustomWarehouseService;
import com.earth.eartherpintegration.beans.ERPItemBarcode;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.service.ERPIntegrationService;


/**
 *
 */
public abstract class AbstractERPProductJob extends AbstractJobPerformable<ERPProductCronJobModel>
{

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractERPProductJob.class);

	private static final String STAGED = "Staged";
	private static final String ONLINE = "Online";
	protected static final String KG_UNIT_SYMBOL = "KG";


	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Resource(name = "indexerService")
	private IndexerService indexerService;

	@Resource(name = "facetSearchConfigService")
	private FacetSearchConfigService facetSearchConfigService;

	@Resource(name = "stockService")
	private StockService stockService;

	@Resource(name = "erpIntegrationService")
	private ERPIntegrationService erpIntegrationService;

	@Resource(name = "unitService")
	private UnitService unitService;

	@Resource(name = "customWarehouseService")
	private CustomWarehouseService warehouseService;

	protected abstract PerformResult performProductJob(final ERPProductCronJobModel cronjob);

	@Override
	public final PerformResult perform(final ERPProductCronJobModel cronjob)
	{
		if (cronjob == null)
		{
			LOG.error("Cronjob model is empty!");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		final PerformResult performResult = performProductJob(cronjob);

		synchronizeCatalog(cronjob);

		indexSolrFacetSearchIndesies(cronjob);

		return performResult;
	}

	/**
	 *
	 */
	private void indexSolrFacetSearchIndesies(final ERPProductCronJobModel cronjob)
	{
		if (!Boolean.TRUE.equals(cronjob.getIsIndexing()))
		{
			LOG.info("Cronjob is not indexable, enable isIndexing property to index the SOLR Facets");
			return;
		}

		if (cronjob.getSolrFacetSearchConfig() == null)
		{
			LOG.error("Cronjob SolrFacetSearchConfig is empty!");
			return;
		}

		final FacetSearchConfig facetSearchConfig = getFacetSearchConfig(cronjob.getSolrFacetSearchConfig());
		if (facetSearchConfig != null)
		{
			try
			{
				getIndexerService().performFullIndex(facetSearchConfig);
			}
			catch (final IndexerException e)
			{
				LOG.error(e.getMessage());
				return;
			}
		}

	}

	private FacetSearchConfig getFacetSearchConfig(final SolrFacetSearchConfigModel facetSearchConfigModel)
	{
		try
		{
			return getFacetSearchConfigService().getConfiguration(facetSearchConfigModel.getName());
		}
		catch (final FacetConfigServiceException var3)
		{
			LOG.error("Configuration service could not load configuration with name: " + facetSearchConfigModel.getName(), var3);
			return null;
		}
	}

	protected void synchronizeCatalog(final CatalogVersionModel sourceVersion, final CatalogVersionModel targetVersion)
	{
		cronJobService.performCronJob(createSyncCronJob(createCatalogVersionSyncJobModel(sourceVersion, targetVersion)), true);
	}

	private void synchronizeCatalog(final ERPProductCronJobModel cronJobModel)
	{
		if (!cronJobModel.getIsSync())
		{
			LOG.info("Cronjob is not syncable, enable isSync property to sync the catalog version");
			return;
		}

		final CatalogVersionModel sourceVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
		final CatalogVersionModel targetVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), ONLINE);
		catalogVersionService.setSessionCatalogVersion(targetVersion.getCatalog().getId(), ONLINE);
		synchronizeCatalog(sourceVersion, targetVersion);
		LOG.info("Finished synchronization of product catalog: " + targetVersion.getCatalog().getId());
	}

	private CatalogVersionSyncJobModel createCatalogVersionSyncJobModel(final CatalogVersionModel sourceVersion,
			final CatalogVersionModel targetVersion)
	{
		final String jobCode = "sync " + sourceVersion.getCatalog().getId() + ":Staged->Online";
		final JobModel job = cronJobService.getJob(jobCode);
		CatalogVersionSyncJobModel syncJob = null;
		if (job == null)
		{
			syncJob = modelService.create(CatalogVersionSyncJobModel.class);
			syncJob.setCode(jobCode);
			syncJob.setSourceVersion(sourceVersion);
			syncJob.setTargetVersion(targetVersion);
			modelService.save(syncJob);

			modelService.refresh(syncJob);

			return syncJob;
		}
		syncJob = (CatalogVersionSyncJobModel) job;
		return syncJob;
	}

	private CatalogVersionSyncCronJobModel createSyncCronJob(final CatalogVersionSyncJobModel catalogVersionSyncJobModel)
	{
		final CatalogVersionSyncCronJobModel cronJobModel = modelService.create(CatalogVersionSyncCronJobModel.class);
		cronJobModel.setJob(catalogVersionSyncJobModel);
		cronJobModel.setCode("update_product_Job_" + UUID.randomUUID().toString());

		modelService.save(cronJobModel);
		return cronJobModel;
	}

	protected Set<String> getERPUpdatedProductInfoCodes(final ERPProductCronJobModel cronjob) throws EarthERPException
	{
		final Set<String> set = new HashSet<>();
		for (final ERPItemBarcode erpItemBarcode : getErpIntegrationService().getItemsBarcodeList(cronjob.getStore()))
		{
			set.add(erpItemBarcode.getItemId());
		}
		return set;
	}

	protected List<ERPItemBarcode> getERPUpdatedProductInfosByCode(final ERPProductCronJobModel cronjob, final String itemCode)
			throws EarthERPException
	{
		return getErpIntegrationService().getItemBarcodeList(cronjob.getStore(), itemCode);
	}

	protected Set<String> getERPUpdatedProductPriceCodes(final ERPProductCronJobModel cronjob) throws EarthERPException
	{
		final Set<String> set = new HashSet<>();
		for (final ERPItemPrice itemPrice : getErpIntegrationService().getItemsPriceList(cronjob.getStore()))
		{
			set.add(itemPrice.getItemId());
		}
		return set;
	}

	protected List<ERPItemPrice> getERPUpdatedProductPricesByCode(final ERPProductCronJobModel cronjob, final String itemCode)
			throws EarthERPException
	{
		return getErpIntegrationService().getItemPriceList(cronjob.getStore(), itemCode);
	}

	protected Map<String, Set<String>> getERPUpdatedProductInventoryCodes(final ERPProductCronJobModel cronjob)
			throws EarthERPException
	{
		final Set<String> locations = getInventoryLocations();
		if (CollectionUtils.isEmpty(locations))
		{
			return Collections.emptyMap();
		}

		final Map<String, Set<String>> map = new HashMap<>();

		for (final String location : locations)
		{
			map.put(location, new HashSet());
			for (final ERPItemInventory itemInventory : getErpIntegrationService().getItemsInventoryList(cronjob.getStore(),
					location))
			{
				map.get(location).add(itemInventory.getItemCode());
			}
		}
		return map;
	}

	/**
	 *
	 */
	protected Set<String> getInventoryLocations()
	{

		final List<WarehouseModel> warehouses = getWarehouseService().getWarehouseWithERPLocationCode();

		if (CollectionUtils.isEmpty(warehouses))
		{
			LOG.warn("No erp warehouses");
			return Collections.emptySet();
		}
		return warehouses.stream().map(WarehouseModel::getErpLocationCode).collect(Collectors.toSet());
	}

	protected List<ERPItemInventory> getERPUpdatedProductInventoriesByCode(final ERPProductCronJobModel cronjob,
			final String inventoryLocation, final String itemCode) throws EarthERPException
	{
		return getErpIntegrationService().getItemInventoryList(cronjob.getStore(), inventoryLocation, itemCode);
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @return the indexerService
	 */
	protected IndexerService getIndexerService()
	{
		return indexerService;
	}

	/**
	 * @return the facetSearchConfigService
	 */
	protected FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}

	/**
	 * @return the stockService
	 */
	protected StockService getStockService()
	{
		return stockService;
	}

	/**
	 * @return the erpIntegrationService
	 */
	protected ERPIntegrationService getErpIntegrationService()
	{
		return erpIntegrationService;
	}

	/**
	 * @return the unitService
	 */
	protected UnitService getUnitService()
	{
		return unitService;
	}

	protected Date getDateFromString(final String dateStr)
	{
		try
		{
			return new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
		}
		catch (final ParseException e)
		{
			LOG.warn("Invalid Date format {}", dateStr);
			return null;
		}
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	protected ProductModel getProduct(final ERPProductCronJobModel cronJobModel, final String code)
	{
		try
		{
			return getProductService().getProductForCode(getActiveCatalogVersion(cronJobModel), code);
		}
		catch (final Exception ex)
		{
			LOG.info("product wiht code {} is not found due to ex {} ", code, ex.getMessage());
			return null;
		}
	}


	protected CatalogVersionModel getActiveCatalogVersion(final ERPProductCronJobModel cronJobModel)
	{
		return getCatalogVersionService().getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
	}

	protected Optional<ProductModel> getVariantProduct(final ProductModel product, final String unitId,
			final ERPProductCronJobModel cronJobModel)
	{
		if (Strings.isBlank(unitId))
		{
			LOG.warn("unit is empty");
			return Optional.empty();
		}
		final String code = product.getCode();

		if (KG_UNIT_SYMBOL.equalsIgnoreCase(unitId))
		{
			LOG.info("product with code {} is weighted", code);
			return Optional.ofNullable(product);
		}

		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			LOG.info("product with code {} is a base product", code);
			return unitId.equalsIgnoreCase(product.getUnit().getCode()) ? Optional.ofNullable(product) : Optional.empty();
		}

		final String productCode = code.concat("_").concat(unitId.toUpperCase());
		LOG.info("variant product code is {} ", productCode);

		return Optional.ofNullable(getProduct(cronJobModel, productCode));


	}

	/**
	 * @return the warehouseService
	 */
	protected CustomWarehouseService getWarehouseService()
	{
		return warehouseService;
	}

}
