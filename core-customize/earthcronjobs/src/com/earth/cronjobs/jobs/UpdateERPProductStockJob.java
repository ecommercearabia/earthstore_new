/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.model.ERPProductCronJobModel;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.exception.EarthERPException;


/**
 *
 */
public class UpdateERPProductStockJob extends AbstractERPProductJob
{

	/**
	 *
	 */
	/**
	 *
	 */
	private static final String UPDATE_QUANTITY_MSG = "updateQuantity={}";
	protected static final Logger LOG = LoggerFactory.getLogger(UpdateERPProductStockJob.class);

	@Override
	protected PerformResult performProductJob(final ERPProductCronJobModel cronjob)
	{
		try
		{
			final Map<String, Set<String>> codesByInventory = getERPUpdatedProductInventoryCodes(cronjob);
			if (MapUtils.isEmpty(codesByInventory))
			{
				LOG.info("No items to update stocks");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}

			for (final Map.Entry<String, Set<String>> mapEntry : codesByInventory.entrySet())
			{
				updateWarehouse(cronjob, mapEntry.getKey(), mapEntry.getValue());
			}

		}
		catch (final Exception e)
		{
			LOG.error("Cannot Update stock due to excetpion {}", e.getMessage());
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * @param set
	 * @param string
	 * @param cronjob
	 *
	 */
	private void updateWarehouse(final ERPProductCronJobModel cronjob, final String warehouseCode, final Set<String> productCodes)
	{
		if (CollectionUtils.isEmpty(productCodes))
		{
			LOG.info("Cannot Update Warehouse {} because no productCodes found", warehouseCode);
			return;

		}

		Optional<WarehouseModel> warehouseForCode = null;
		try
		{
			warehouseForCode = getWarehouseService().getWarehouseByERPLocationCode(warehouseCode);

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.error("NO warehouse found for code {}", warehouseCode);
			return;
		}

		if (warehouseForCode.isEmpty())
		{
			LOG.info("Cannot Update Warehouse {} because no warehouse exist", warehouseCode);
			return;
		}

		for (final String code : productCodes)
		{
			updateProductStockInWarehouse(cronjob, warehouseForCode.get(), code);
		}

	}

	/**
	 * @param cronjob
	 *
	 */
	private void updateProductStockInWarehouse(final ERPProductCronJobModel cronjob, final WarehouseModel warehouseForCode,
			final String code)
	{
		final ProductModel product = getProduct(cronjob, code);
		if (Objects.isNull(product))
		{
			LOG.warn("Cannot update stock becasue no product exist with code {}", code);
			return;
		}

		List<ERPItemInventory> itemInventoryList;
		try
		{
			itemInventoryList = getErpIntegrationService().getItemInventoryList(cronjob.getStore(),
					warehouseForCode.getErpLocationCode(),
					code);
		}
		catch (final EarthERPException e)
		{
			LOG.error("Cannot Update stock due to exception {}", e.getMessage());
			return;
		}

		if (CollectionUtils.isEmpty(itemInventoryList))
		{
			LOG.error("Cannot Update stock {} because no items available", code);
			return;
		}
		for (final ERPItemInventory erpItemInventory : itemInventoryList)
		{
			updateProductStocks(cronjob, warehouseForCode, product, erpItemInventory);
		}

	}

	/**
	 *
	 */
	private void updateProductStocks(final ERPProductCronJobModel cronjob, final WarehouseModel warehouse,
			final ProductModel product, final ERPItemInventory erpItemInventory)
	{
		final UnitModel unitForCode = getUnitService().getUnitForCode(erpItemInventory.getUnitID());

		if (Objects.isNull(unitForCode))
		{
			LOG.warn("Cannot Update Product Stocks for {} becaue no unit {} available", product.getCode(),
					erpItemInventory.getUnitID());
			return;
		}

		final Optional<ProductModel> variantProduct = getVariantProduct(product, unitForCode.getCode(), cronjob);
		if (variantProduct.isEmpty())
		{
			LOG.warn("NO Product with code {} and unit {}", product.getCode(), unitForCode.getCode());
			return;
		}

		final int quantity = updateQuantity(variantProduct.get(), cronjob, (int) erpItemInventory.getQuantity());

		final StockLevelModel stockLevel = getStockService().getStockLevel(variantProduct.get(), warehouse);
		if (Objects.isNull(stockLevel))
		{
			createStockLevel(variantProduct.get(), quantity, warehouse);
		}
		else
		{
			updateStockLevel(cronjob, variantProduct.get(), quantity, stockLevel);
			updateInventoryEvents(cronjob, stockLevel);

		}

	}

	/**
	 * @param cronjob
	 * @param quantity
	 *
	 */
	private void updateStockLevel(final ERPProductCronJobModel cronjob, final ProductModel product, final int quantity,
			final StockLevelModel stockLevel)
	{
		if (cronjob.isResetStockReserved())
		{
			stockLevel.setReserved(0);
		}
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		getModelService().save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: {}", product.getCode());

	}

	/**
	 * @param quantity
	 *
	 */
	private void createStockLevel(final ProductModel product, final int quantity, final WarehouseModel warehouse)
	{
		final StockLevelModel newStock = getModelService().create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);

		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(0);
		newStock.setProductCode(product.getCode());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		getModelService().save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE:{} ", product.getCode());

	}


	protected int updateQuantity(final ProductModel product, final ERPProductCronJobModel cronJob, final int quantity)
	{
		LOG.info("isUpdateFixedStock={}", cronJob.isUpdateFixedStock());
		if (quantity < 0)
		{
			LOG.info(UPDATE_QUANTITY_MSG, 0);
			return 0;
		}
		if (cronJob.isUpdateFixedStock())
		{
			LOG.info(UPDATE_QUANTITY_MSG, cronJob.getFixedStock());

			return (int) cronJob.getFixedStock();
		}
		else if (KG_UNIT_SYMBOL.equalsIgnoreCase(product.getUnit().getCode()))
		{
			LOG.info(UPDATE_QUANTITY_MSG, quantity * 1000);
			return quantity * 1000;
		}
		else
		{
			LOG.info(UPDATE_QUANTITY_MSG, quantity);
			return quantity;
		}
	}

	protected void updateInventoryEvents(final ERPProductCronJobModel cronJob, final StockLevelModel stockLevel)
	{
		if (cronJob.getRemoveInventoryEvents() == null || Boolean.TRUE.equals(cronJob.getRemoveInventoryEvents()))
		{
			LOG.info("Delete inventory events where product code={}", stockLevel.getProductCode());
			stockLevel.setInventoryEvents(null);
			getModelService().save(stockLevel);
		}
	}

}
