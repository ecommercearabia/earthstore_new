/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.enums.ERPProductPriceGroup;
import com.earth.cronjobs.model.ERPProductCronJobModel;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.exception.EarthERPException;


/**
 *
 */
public class UpdateERPProductPriceJob extends AbstractERPProductJob
{
	protected static final Logger LOG = LoggerFactory.getLogger(UpdateERPProductPriceJob.class);


	@SuppressWarnings("removal")
	@Override
	protected PerformResult performProductJob(final ERPProductCronJobModel cronjob)
	{
		Set<String> codes;
		try
		{
			codes = getERPUpdatedProductPriceCodes(cronjob);

			if (CollectionUtils.isEmpty(codes))
			{
				LOG.info("No items available to update price");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}


		}
		catch (final Exception e)
		{
			LOG.info("Cannot Update Prices due to exception {}", e.getMessage());
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}


		for (final String code : codes)
		{
			try
			{
				updatePrice(cronjob, code);
			}
			catch (final Exception ex)
			{
				LOG.info("Cannot Update Prices for code {} due to exception {}", code, ex.getMessage());
			}
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	/**
	 * @throws EarthERPException
	 *
	 */
	private void updatePrice(final ERPProductCronJobModel cronjob, final String code) throws EarthERPException
	{
		final ProductModel product = getProduct(cronjob, code);

		if (Objects.isNull(product))
		{
			LOG.info("No Product with code {} available to update price", code);
			return;
		}

		final List<ERPItemPrice> itemPriceList = getErpIntegrationService().getItemPriceList(cronjob.getStore(), code);

		if (CollectionUtils.isEmpty(itemPriceList))
		{
			LOG.warn("No item prices for product {}", code);
			return;
		}

		final Set<ERPItemPrice> onlineItemPrices = getItemPrices(itemPriceList, cronjob);

		if (CollectionUtils.isEmpty(onlineItemPrices))
		{
			LOG.warn("No Online item prices for product {}", code);
			return;
		}

		for (final ERPItemPrice erpItemPrice : onlineItemPrices)
		{
			UnitModel unitForCode = null;
			try
			{
				unitForCode = getUnitService().getUnitForCode(erpItemPrice.getUnitId());
			}
			catch (final UnknownIdentifierException ex)
			{
				LOG.warn("No unit with code {} exist", erpItemPrice.getUnitId());
			}
			if (Objects.isNull(unitForCode))
			{
				LOG.warn("Cannot Update product {} because unit {} is not found", code, erpItemPrice.getUnitId());
				continue;
			}

			if (isValidDate(erpItemPrice.getToDate()))
			{
				updatePriceRow(cronjob, product, erpItemPrice, unitForCode);
			}
			else
			{
				LOG.warn("Cannot Update Product {}, because toDate {} is invalid", code, erpItemPrice.getToDate());
			}

		}

	}


	private Set<ERPItemPrice> getItemPrices(final List<ERPItemPrice> itemPriceList, final ERPProductCronJobModel cronjob)
	{
		final Set<ERPItemPrice> onlineItemPrices = itemPriceList.stream().filter(item -> "Online".equals(item.getPriceGroup()))
				.collect(Collectors.toSet());

		if (CollectionUtils.isEmpty(onlineItemPrices) && ERPProductPriceGroup.ALL.equals(cronjob.getErpProductPriceGroup()))
		{
			return itemPriceList.stream().collect(Collectors.toSet());
		}
		return onlineItemPrices;
	}


	/**
	 *
	 */
	private void updatePriceRow(final ERPProductCronJobModel cronJobModel, final ProductModel product,
			final ERPItemPrice erpItemPrice, final UnitModel unitForCode)
	{
		final Optional<ProductModel> variantProduct = getVariantProduct(product, erpItemPrice.getUnitId(), cronJobModel);
		if (variantProduct.isEmpty())
		{
			LOG.warn("Cannot update price for product {} with unit {}", product.getCode(), erpItemPrice.getUnitId());
			return;
		}
		final Collection<PriceRowModel> europe1Prices = variantProduct.get().getEurope1Prices();
		if (CollectionUtils.isEmpty(europe1Prices))
		{
			LOG.info("No price rows for product {}", variantProduct.get().getCode());
			createPriceRowForProduct(cronJobModel, variantProduct.get(), erpItemPrice, unitForCode);
			return;
		}
		final PriceRowModel priceRowModel = findPriceRowsByCurrencyAndDates(europe1Prices, erpItemPrice);

		if (Objects.isNull(priceRowModel))
		{
			LOG.info("No price rows for product {}", variantProduct.get().getCode());
			createPriceRowForProduct(cronJobModel, variantProduct.get(), erpItemPrice, unitForCode);
			return;
		}

		LOG.info("Update Price Row for product {}", variantProduct.get().getCode());
		priceRowModel.setPrice(erpItemPrice.getAmount());
		getModelService().save(priceRowModel);
	}

	/**
	 * @param europe1Prices
	 *
	 */
	private PriceRowModel findPriceRowsByCurrencyAndDates(final Collection<PriceRowModel> europe1Prices,
			final ERPItemPrice erpItemPrice)
	{
		final Date startDate = getDateFromString(erpItemPrice.getFromDate());
		final Date endDate = getDateFromString(erpItemPrice.getToDate());
		final List<PriceRowModel> priceRowModels = europe1Prices.stream().filter(e -> isRangeExist(e, startDate, endDate))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(priceRowModels))
		{
			return null;
		}

		return priceRowModels.get(0);
	}


	/**
	 *
	 */
	private boolean isRangeExist(final PriceRowModel e, final Date startDate, final Date endDate)
	{

		return startDate.equals(e.getStartDate()) && endDate.equals(e.getEndDate());
	}



	/**
	 *
	 */
	private void createPriceRowForProduct(final ERPProductCronJobModel cronJobModel, final ProductModel product,
			final ERPItemPrice erpItemPrice, final UnitModel unitForCode)
	{
		LOG.info("Create a new price row for product {}", product.getCode());
		final PriceRowModel priceRowModel = getModelService().create(PriceRowModel.class);
		priceRowModel.setUnit(unitForCode);
		priceRowModel.setPrice(erpItemPrice.getAmount());
		priceRowModel.setCurrency(cronJobModel.getStore().getDefaultCurrency());
		priceRowModel.setStartDate(getDateFromString(erpItemPrice.getFromDate()));
		priceRowModel.setEndDate(getDateFromString(erpItemPrice.getToDate()));
		priceRowModel.setProduct(product);
		priceRowModel.setUnitFactor(KG_UNIT_SYMBOL.equalsIgnoreCase(unitForCode.getCode()) ? 1000 : 1);
		getModelService().save(priceRowModel);
	}


	/**
	 *
	 */
	private boolean isValidDate(final String toDate)
	{
		final Date date = getDateFromString(toDate);

		if (Objects.isNull(date))
		{
			return false;
		}
		final Date today = new Date();
		return today.before(date);
	}
}
