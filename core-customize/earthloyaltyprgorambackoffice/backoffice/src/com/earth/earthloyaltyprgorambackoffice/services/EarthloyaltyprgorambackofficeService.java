/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthloyaltyprgorambackoffice.services;

/**
 * Hello World EarthloyaltyprgorambackofficeService
 */
public class EarthloyaltyprgorambackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
