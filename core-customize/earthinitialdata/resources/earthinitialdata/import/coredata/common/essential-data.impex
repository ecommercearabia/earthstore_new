# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
# Import essential data for the Accelerator
#
# Includes:
# * Languages
# * Currencies
# * Titles
# * Vendors
# * Warehouses
# * Supported Credit/Debit cards
# * User Groups
# * DistanceUnits for Storelocator
# * MediaFolders
# * MediaFormats
# * Tax & Tax Groups
# * Jobs
 
# Languages
INSERT_UPDATE Language ; isocode[unique=true] ; fallbackLanguages(isocode) ; active[default=true] 
                       ; en                   ;                            ;                      ;  
                       ; ar                   ; en                         ;                      ;  
                       
# Currencies           
INSERT_UPDATE Currency ; isocode[unique=true] ; conversion ; digits ; displayDigits ; symbol ; base  ; active 
                       ; AED                  ; 1          ; 10      ;2			    ; AED    ; true  ; true   
                       ; USD                  ; 0.27       ; 10      ;2				; USD    ; false ; false  
                                     
# SiteMap Language Currencies
INSERT_UPDATE SiteMapLanguageCurrency ; &siteMapLanguageCurrency ; language(isoCode)[unique=true] ; currency(isocode)[unique=true] ;  
                                      ; enAed                    ; en                             ; AED                            
                                      ; arAed                    ; ar                             ; AED                            
                                      
                                      
# Vendor                              
INSERT_UPDATE Vendor ; code[unique=true] ; name  
                     ; earth             ; earth 
                     
INSERT Warehouse                                                                                              ; code[unique=true] ; name            ; vendor(code)[default=earth, forceWrite=true] ; default[default=true, forceWrite=true] 
"#% beforeEach:                                                                                               
import de.hybris.platform.core.Registry                                                                       ;                   
import de.hybris.platform.ordersplitting.model.WarehouseModel                                                 ;                   
String warehouseCode = line.get(Integer.valueOf(1))                                                           ;                   
WarehouseModel warehouse                                                                                      ;                   
try                                                                                                           
{                                                                                                             
warehouse = Registry.getApplicationContext().getBean(""warehouseService"").getWarehouseForCode(warehouseCode) ;                   
}                                                                                                             
catch (Exception e)                                                                                           
{                                                                                                             
warehouse = null                                                                                              ;                   
}                                                                                                             
if (warehouse != null)                                                                                        
{                                                                                                             
line.clear()                                                                                                  ;                   
}"                                                                                                            
                                                                                                              ; earth-ws          ; earth Warehouse ;                                              
                                                                                                              
# Disable preview for email pages                                                                             
UPDATE CMSPageType ; code[unique=true] ; previewDisabled 
                   ; EmailPage         ; true            
                   
# Titles           
INSERT_UPDATE Title ; code[unique=true] 
                    ; mr                
                    ; mrs               
                    ; miss              
                    ; ms                
                    
                    
# Media Folders     
INSERT_UPDATE MediaFolder ; qualifier[unique=true] ; path[unique=true] 
                          ; images                 ; images            
                          ; email-body             ; email-body        
                          ; email-attachments      ; email-attachments 
                          ; documents              ; documents         
                          
# Media formats           
INSERT_UPDATE MediaFormat ; qualifier[unique=true] 
                          ; 1200Wx1200H            
                          ; 515Wx515H              
                          ; 365Wx246H              
                          ; 300Wx300H              
                          ; 96Wx96H                
                          ; 65Wx65H                
                          ; 30Wx30H                
                          ; mobile                 
                          ; tablet                 
                          ; desktop                
                          ; widescreen             
                          
# Tax & Tax Groups        
INSERT_UPDATE UserTaxGroup ; code[unique=true] 
                           ; earth-taxes       
                           
                           
INSERT_UPDATE ProductTaxGroup ; code[unique=true] 
                              ; earth-vat-full    
                              
                              
INSERT_UPDATE Tax ; code[unique=true] ; value ; currency(isocode) 
                  ; earth-vat-full    ; 5     
                  
INSERT_UPDATE ServicelayerJob ; code[unique=true] ; springId[unique=true] 
                              ; cartRemovalJob    ; cartRemovalJob        
                              ; siteMapMediaJob   ; siteMapMediaJob       
                              
 
INSERT_UPDATE Unit;unitType[unique=true];code[unique=true];conversion
;CAR;CAR;1
;BAG;BAG;1
;BOX;BOX;1
;CT1;CT1;1
;CAE;CAE;1
;CAN;CAN;1
;CAR;CAR;1
;CP;CP;1
;CT1;CT1;1
;CT2;CT2;1
;CT3;CT3;1
;CT7;CT7;1
;CTN;CTN;1
;DZ;DZ;1
;EA;EA;1
;GAL;GAL;1
;GR;GR;1
;KAR;KAR;1
;KG;KG;1
;L;L;1
;OT1;OT1;1
;OT2;OT2;1
;OT3;OT3;1
;OUT;OUT;1
;PAC;PAC;1
;PC;PC;1
;PKT;PKT;1
;ROL;ROL;1
;S01;S01;1
;SO1;SO1;1
;SO2;SO2;1
;DR;DR;1
;ST;ST;1

INSERT_UPDATE UnitFactorRange;code[unique=true];startUnitValue;unitValueStep;endUnitValue
;KG_100g_Scale50;100;50;50000
;KG_200g_Scale100;200;100;50000
;KG_1500g_Scale500;1500;500;50000
;KG_4000g_Scale500;4000;500;50000
;KG_500g_Scale500;500;500;50000
;KG_500g_Scale250;500;250;50000
;KG_500g_Scale100;500;100;50000
;KG_1000g_Scale1000;1000;1000;50000
;KG_1000g_Scale500;1000;500;50000
;KG_2000g_Scale500;2000;500;50000
;KG_250g_Scale100;250;100;50000
;KG_250g_Scale250;250;250;50000
                             
# Create oAuth2 clients       
INSERT_UPDATE OAuthClientDetails ; clientId[unique=true] ; resourceIds ; scope    ; authorizedGrantTypes                                                                   ; authorities         ; clientSecret ; registeredRedirectUri                                              
                                 ; client-side           ; hybris      ; basic    ; implicit,client_credentials                                                            ; ROLE_CLIENT         ; secret       ; http://localhost:9001/authorizationserver/oauth2_implicit_callback ;  
                                 ; mobile_android        ; hybris      ; basic    ; authorization_code,refresh_token,password,client_credentials,otp,signature             ; ROLE_CLIENT         ; secret123    ; http://localhost:9001/authorizationserver/oauth2_callback          ;  
                                 ; occ_mobile            ; hybris      ; basic    ; authorization_code,refresh_token,password,client_credentials,otp,signature,third_party ; ROLE_CLIENT         ; Erabia@123   ; http://localhost:9001/authorizationserver/oauth2_callback          ;  
                                 ; trusted_client        ; hybris      ; extended ; authorization_code,refresh_token,password,client_credentials                           ; ROLE_TRUSTED_CLIENT ; secret       ;                                                                    ;  
								 ;oms_webservice_client	 ;hybris	   ;basic	  ;refresh_token,password,client_credentials,authorization_code	                           ;ROLE_CLIENT			 ;ErabiaOMS@2021;http://localhost:9001/authorizationserver/oauth2_callback           ;
                                 