/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthmessagecentercscustomocc.constants;

/**
 * Global class for all earthmessagecentercscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthmessagecentercscustomoccConstants extends GeneratedEarthmessagecentercscustomoccConstants
{
	public static final String EXTENSIONNAME = "earthmessagecentercscustomocc";

	private EarthmessagecentercscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
