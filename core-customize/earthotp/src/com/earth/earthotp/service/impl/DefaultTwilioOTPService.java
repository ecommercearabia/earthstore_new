package com.earth.earthotp.service.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthotp.context.OTPProviderContext;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.enums.OTPExceptionType;
import com.earth.earthotp.model.OTPProviderModel;
import com.earth.earthotp.model.TwilioOTPProviderModel;
import com.earth.earthotp.service.OTPService;
import com.earth.earthotp.twilio.service.TwilioService;


/**
 * @author mnasro
 * @author monzer
 *
 *         The Class DefaultTwilioOTPService.
 */
public class DefaultTwilioOTPService implements OTPService
{

	/** The twilio service. */
	@Resource(name = "twilioService")
	private TwilioService twilioService;

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	/**
	 * Send OTP code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean sendOTPCode(final String countryCode, final String mobileNumber) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if (!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final TwilioOTPProviderModel twilioOTPProviderModel = (TwilioOTPProviderModel) provider.get();

		return getTwilioService().sendOTPCode(twilioOTPProviderModel.getAuthToken(), twilioOTPProviderModel.getApiKey(),
				twilioOTPProviderModel.getAccountSid(), countryCode, mobileNumber);
	}

	/**
	 * Verify code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryCode, final String mobileNumber, final String code) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if (!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final TwilioOTPProviderModel twilioOTPProviderModel = (TwilioOTPProviderModel) provider.get();

		return getTwilioService().verifyCode(twilioOTPProviderModel.getAuthToken(), twilioOTPProviderModel.getApiKey(),
				twilioOTPProviderModel.getAccountSid(), countryCode, mobileNumber, code);
	}


	/**
	 * Gets the twilio service.
	 *
	 * @return the twilio service
	 */
	protected TwilioService getTwilioService()
	{
		return twilioService;
	}

	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	@Override
	public boolean sendSMSMessage(final String to, final String message) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if (!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final TwilioOTPProviderModel twilioOTPProviderModel = (TwilioOTPProviderModel) provider.get();


		return getTwilioService().sendSMSMessage(twilioOTPProviderModel.getAuthToken(), twilioOTPProviderModel.getAccountSid(), to,
				twilioOTPProviderModel.getMessagingServiceSid(), message);
	}

	@Override
	public String sendOrderConfirmationWhatsappMessage(final String customerName, final String mobileNumber, final String date,
			final String time) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

	@Override
	public String sendOrderDeliveredWhatsappMessage(final String customerName, final String mobileNumber, final String orderId)
			throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

}


