/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.service;

import com.earth.earthotp.exception.OTPException;



/**
 * The Interface OTPService.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Interface OTPService.
 */
public interface OTPService
{

	/**
	 * Send OTP code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendOTPCode(final String countryCode, final String mobileNumber) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryCode, final String mobileNumber, final String code) throws OTPException;

	/**
	 * Send SMS message.
	 *
	 * @param to
	 *           the to
	 * @param message
	 *           the message
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendSMSMessage(String to, String message) throws OTPException;

	public String sendOrderConfirmationWhatsappMessage(String customerName, String mobileNumber, String date, String time)
			throws OTPException;

	public String sendOrderDeliveredWhatsappMessage(String customerName, String mobileNumber, String orderId)
			throws OTPException;
}
