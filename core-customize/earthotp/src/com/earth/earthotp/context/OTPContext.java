/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.earth.earthotp.entity.SessionData;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.model.OTPProviderModel;



/**
 * The Interface OTPContext.
 *
 * @author mnasro
 * @author abu-muhasien
 */
public interface OTPContext
{

	/**
	 * Checks if is enabled by current site.
	 *
	 * @return true, if is enabled by current site
	 */
	public boolean isEnabledByCurrentSite();

	/**
	 * Checks if is enabled.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if is enabled
	 */
	public boolean isEnabled(final CMSSiteModel cmsSiteModel);

	/**
	 * Gets the session data.
	 *
	 * @param sessionKey
	 *           the session key
	 * @return the session data
	 */
	public Optional<SessionData> getSessionData(String sessionKey);

	/**
	 * Removes the session data.
	 *
	 * @param sessionKey
	 *           the session key
	 */
	public void removeSessionData(String sessionKey);

	/**
	 * Send OTP code by current site and session data.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param data
	 *           the data
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCodeByCurrentSiteAndSessionData(final String countryisoCode, final String mobileNumber, SessionData data)
			throws OTPException;

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param otpProviderModel
	 *           the otp provider model
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException;

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param cmsSiteModel
	 *           the cms site model
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final CMSSiteModel cmsSiteModel)
			throws OTPException;

	/**
	 * Send OTP code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCodeByCurrentSite(final String countryisoCode, final String mobileNumber) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param otpProviderModel
	 *           the otp provider model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final CMSSiteModel cmsSiteModel) throws OTPException;

	/**
	 * Verify code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCodeByCurrentSite(final String countryisoCode, final String mobileNumber, final String code)
			throws OTPException;


	/**
	 * Send order confirmation sms.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendOrderConfirmationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException;

	/**
	 * Send order cancelation SMS message.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendOrderCancelationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException;


	/**
	 * Send order cancelation SMS message.
	 *
	 * @param consignmentModel
	 *           the consignment model
	 * @return true, if successful
	 */
	public boolean sendDeliveryConfirmationSMSMessage(final ConsignmentModel consignmentModel);



	/**
	 * Send shipping confirmation SMS message.
	 *
	 * @param consignmentModel
	 *           the consignment model
	 * @return true, if successful
	 */
	public boolean sendShippingConfirmationSMSMessage(final ConsignmentModel consignmentModel);

	public boolean sendOrderConfirmationWhatsappMessage(final AbstractOrderModel abstractOrderModel) throws OTPException;

	public boolean sendOrderDeliveredWhatsappMessage(final ConsignmentModel consignmentModel) throws OTPException;

}
