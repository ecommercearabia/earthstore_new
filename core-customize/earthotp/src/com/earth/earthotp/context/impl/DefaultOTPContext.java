/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthotp.context.OTPContext;
import com.earth.earthotp.context.OTPProviderContext;
import com.earth.earthotp.entity.SessionData;
import com.earth.earthotp.entity.SmsForm;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.enums.OTPExceptionType;
import com.earth.earthotp.model.OTPProviderModel;
import com.earth.earthotp.strategy.OTPStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPContext.
 *
 * @author mnasro
 */
public class DefaultOTPContext implements OTPContext
{
	/**
	 *
	 */
	private static final String OTP_PROVIDER_MODEL_IS_NULL2 = "otpProviderModel is null";

	/**
	 *
	 */
	private static final String STRATEGY_WAS_NOT_FOUND_FOR = "Strategy was not found for ";

	/**
	 *
	 */
	private static final String EXCEPTION_OCCURRED_SENDING_SMS_DEFAULT_OTP_CONTEXT_324 = "Exception occurred sending SMS DefaultOTPContext::324";

	/**
	 *
	 */
	private static final String FOOD_CROWD = "Food Crowd";

	/**
	 *
	 */
	private static final String ABSTRACT_ORDER_MODEL_IS_NULL = "abstractOrderModel is null";

	/**
	 *
	 */
	private static final String DATA_SESSION_KEY_IS_NULL_OR_EMPTY = "data SessionKey is null or empty";

	/**
	 *
	 */
	private static final String OTP_PROVIDER_MODEL_IS_NULL = "OTPProviderModel is null";

	private static final Logger LOG = Logger.getLogger(DefaultOTPContext.class);

	/** The Constant OTP_STRATEGY_NOT_FOUND. */
	private static final String OTP_STRATEGY_NOT_FOUND = "strategy not found";

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;
	/** The payment strategy map. */
	@Resource(name = "otpStrategyMap")
	private Map<Class<?>, OTPStrategy> otpStrategyMap;

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}


	protected Map<Class<?>, OTPStrategy> getOtpStrategyMap()
	{
		return otpStrategyMap;
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<OTPStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPStrategy strategy = getOtpStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, OTP_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}


	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param otpProviderModel
	 *           the otp provider model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		final Optional<OTPStrategy> strategy = getStrategy(otpProviderModel.getClass());
		if (strategy.isPresent())
		{
			strategy.get().sendOTPCode(countryisoCode, mobileNumber, otpProviderModel);
		}
		else
		{
			LOG.error("Strategy by class " + otpProviderModel.getClass() + " was not found!");
		}
	}

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param cmsSiteModel
	 *           the cms site model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final CMSSiteModel cmsSiteModel)
			throws OTPException
	{
		final Optional<OTPProviderModel> oTPProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(oTPProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL);
		sendOTPCode(countryisoCode, mobileNumber, oTPProviderModel.get());
	}

	/**
	 * Send OTP code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCodeByCurrentSite(final String countryisoCode, final String mobileNumber) throws OTPException
	{
		final Optional<OTPProviderModel> oTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(oTPProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL);
		sendOTPCode(countryisoCode, mobileNumber, oTPProviderModel.get());
	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param otpProviderModel
	 *           the otp provider model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		final Optional<OTPStrategy> strategy = getStrategy(otpProviderModel.getClass());
		if (strategy.isPresent())
		{
			return strategy.get().verifyCode(countryisoCode, mobileNumber, code, otpProviderModel);
		}

		LOG.error("Strategy by class " + otpProviderModel.getClass() + " was not found!");
		return false;

	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final CMSSiteModel cmsSiteModel) throws OTPException
	{
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(otpProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL);
		return verifyCode(countryisoCode, mobileNumber, code, otpProviderModel.get());
	}

	/**
	 * Verify code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCodeByCurrentSite(final String countryisoCode, final String mobileNumber, final String code)
			throws OTPException
	{
		final Optional<OTPProviderModel> oTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(oTPProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL);
		return verifyCode(countryisoCode, mobileNumber, code, oTPProviderModel.get());
	}

	@Override
	public boolean isEnabled(final CMSSiteModel cmsSiteModel)
	{
		if (cmsSiteModel == null || cmsSiteModel.getRegistrationOTPProvider() == null
				|| StringUtils.isBlank(cmsSiteModel.getRegistrationOTPProvider()))
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);

		return otpProviderModel.isPresent();
	}

	@Override
	public boolean isEnabledByCurrentSite()
	{
		Optional<OTPProviderModel> otpProviderModel = Optional.empty();
		try
		{
			otpProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			return false;
		}
		return otpProviderModel.isPresent();
	}

	@Override
	public void sendOTPCodeByCurrentSiteAndSessionData(final String countryisoCode, final String mobileNumber,
			final SessionData data) throws OTPException
	{
		Preconditions.checkArgument(data != null, "data is null");
		Preconditions.checkArgument(data.getData() != null, "data object is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(data.getSessionKey()), DATA_SESSION_KEY_IS_NULL_OR_EMPTY);

		sendOTPCodeByCurrentSite(countryisoCode, mobileNumber);
		getSessionService().setAttribute(data.getSessionKey(), data);
		getSessionService().setAttribute("isSend", Boolean.TRUE);

	}

	@Override
	public Optional<SessionData> getSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), DATA_SESSION_KEY_IS_NULL_OR_EMPTY);

		final Object data = getSessionService().getAttribute(sessionKey);
		if (!(data instanceof SessionData))
		{
			return Optional.empty();
		}
		return Optional.ofNullable((SessionData) data);
	}

	@Override
	public void removeSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), DATA_SESSION_KEY_IS_NULL_OR_EMPTY);

		getSessionService().removeAttribute(sessionKey);

	}

	@Override
	public boolean sendOrderConfirmationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACT_ORDER_MODEL_IS_NULL);
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderConfirmationSmsMessage())
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL);

		final String message = replace(cmsSiteModel.getOrderConfirmationSmsMessage(), abstractOrderModel.getCode());
		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
		final String mobileNumber = getMobileNumber(customerModel, abstractOrderModel);

		String sendSMSMessageWithDescription = null;

		abstractOrderModel.setSmsOrderConfermationRequest(new SmsForm(mobileNumber, FOOD_CROWD, message).toString());
		try
		{
			sendSMSMessageWithDescription = getStrategy(otpProviderModel.get().getClass()).get() // NOSONAR otpProviderModel is checked above
					.sendSMSMessageWithDescription(mobileNumber, message, otpProviderModel.get());
		}
		catch (final OTPException e)
		{
			LOG.error(EXCEPTION_OCCURRED_SENDING_SMS_DEFAULT_OTP_CONTEXT_324, e);
			abstractOrderModel.setSmsOrderConfermationResponse(e.getMessage());
			modelService.save(abstractOrderModel);
			modelService.refresh(abstractOrderModel);
			return false;
		}
		abstractOrderModel.setSmsOrderConfermationResponse(sendSMSMessageWithDescription);
		modelService.save(abstractOrderModel);
		modelService.refresh(abstractOrderModel);
		return true;

	}

	private String replace(final String text, final String newString)
	{

		return text + " " + newString;

	}

	private String replace(final String message, final Map<String, String> replacements)
	{
		if (message == null || message.trim().isBlank() || replacements == null || replacements.isEmpty())
		{
			return message;
		}

		final Pattern pattern = Pattern.compile("\\{\\d*\\}");
		final Matcher matcher = pattern.matcher(message);
		final StringBuilder builder = new StringBuilder();
		int i = 0;
		while (matcher.find())
		{
			final String replacement = replacements.get(matcher.group());
			builder.append(message.substring(i, matcher.start()));
			if (replacement == null)
			{
				builder.append(matcher.group(0));
			}
			else
			{
				builder.append(replacement);
			}
			i = matcher.end();
		}
		if (builder.toString().length() - 1 != i)
		{
			builder.append(message.substring(i));
		}
		return builder.toString();
	}

	@Override
	public boolean sendOrderCancelationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACT_ORDER_MODEL_IS_NULL);
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderCancellationSmsMessage())
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL2);

		String orderCancelationSmsMessage = "";
		if (StringUtils.isNotBlank(cmsSiteModel.getOrderCancelationSmsMessage())
				&& !"null".equalsIgnoreCase(cmsSiteModel.getOrderCancelationSmsMessage()))
		{
			orderCancelationSmsMessage = cmsSiteModel.getOrderCancelationSmsMessage();
		}
		else
		{
			orderCancelationSmsMessage = "Thank you for your Food Crowd order. Your Order Number is ";

		}
		final String message = replace(orderCancelationSmsMessage, abstractOrderModel.getCode());

		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();

		final String mobileNumber = getMobileNumber(customerModel, abstractOrderModel);


		String sendSMSMessageWithDescription = "";
		abstractOrderModel.setSmsOrderCancelationRequest(new SmsForm(mobileNumber, FOOD_CROWD, message).toString());
		final Optional<OTPStrategy> strategy = getStrategy(otpProviderModel.get().getClass());
		if (strategy.isPresent())
		{
			try
			{
				sendSMSMessageWithDescription = strategy.get().sendSMSMessageWithDescription(mobileNumber, message,
						otpProviderModel.get());
			}
			catch (final OTPException e)
			{
				LOG.error("Exception occurred sending SMS DefaultOTPContext::392", e);
				abstractOrderModel.setSmsOrderCancelationResponse(e.getMessage());
				modelService.save(abstractOrderModel);
				modelService.refresh(abstractOrderModel);
				return false;
			}
		}

		abstractOrderModel.setSmsOrderCancelationResponse(sendSMSMessageWithDescription);
		modelService.save(abstractOrderModel);
		modelService.refresh(abstractOrderModel);
		return true;

	}

	protected String getMobileNumber(final CustomerModel customerModel, final AbstractOrderModel abstractOrderModel)
	{

		final String deliveryAddressMobileNumber = abstractOrderModel.getDeliveryAddress() != null
				&& StringUtils.isNotBlank(abstractOrderModel.getDeliveryAddress().getMobile())
						? abstractOrderModel.getDeliveryAddress().getMobile()
						: null;

		if (StringUtils.isNotBlank(deliveryAddressMobileNumber))
		{
			return getNormalizedMobileNumber(abstractOrderModel.getDeliveryAddress().getMobileCountry().getIsocode(),
					abstractOrderModel.getDeliveryAddress().getMobile());
		}
		else
		{
			return getNormalizedMobileNumber(customerModel.getMobileCountry().getIsocode(), customerModel.getMobileNumber());
		}

	}

	protected String getNormalizedMobileNumber(final String countryisocode, final String mobileNumber)
	{

		Optional<String> normalizedPhoneNumber = Optional.empty();

		if (!StringUtils.isEmpty(countryisocode) && !StringUtils.isEmpty(mobileNumber))
		{
			normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(countryisocode, mobileNumber);
		}
		return normalizedPhoneNumber.isPresent() ? normalizedPhoneNumber.get() : mobileNumber;
	}

	@Override
	public boolean sendDeliveryConfirmationSMSMessage(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, "consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "order in the consignmentModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderDeliveredSmsMessage())
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL2);

		String message = StringUtils.isBlank(cmsSiteModel.getOrderDeliveryConfirmationSmsMessage())
				? "Your order number {0} has been delivered"
				: cmsSiteModel.getOrderDeliveryConfirmationSmsMessage();

		final Map<String, String> map = new HashMap<>();
		map.put("{0}", consignmentModel.getOrder().getCode());
		message = replace(message, map);

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
		final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());

		String sendSMSMessageWithDescription = null;
		consignmentModel.setSmsDeliveryConfirmationRequest(new SmsForm(mobileNumber, FOOD_CROWD, message).toString());
		final Optional<OTPStrategy> strategy = getStrategy(otpProviderModel.get().getClass());
		if (strategy.isPresent())
		{
			try
			{
				sendSMSMessageWithDescription = strategy.get().sendSMSMessageWithDescription(mobileNumber, message,
						otpProviderModel.get());
			}
			catch (final Exception e)
			{
				LOG.error(EXCEPTION_OCCURRED_SENDING_SMS_DEFAULT_OTP_CONTEXT_324, e);
				consignmentModel.setSmsDeliveryConfirmationResponse(e.getMessage());
				modelService.save(consignmentModel);
				modelService.refresh(consignmentModel);
				return false;
			}
		}
		consignmentModel.setSmsDeliveryConfirmationResponse(sendSMSMessageWithDescription);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
		return true;

	}

	@Override
	public boolean sendShippingConfirmationSMSMessage(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, "consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "order in the consignmentModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableShippingConfirmationSmsMessage())
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), OTP_PROVIDER_MODEL_IS_NULL2);

		String message = StringUtils.isBlank(cmsSiteModel.getOrderShippingConfirmationSmsMessage())
				? "Your order number {0} has been shipped"
				: cmsSiteModel.getOrderShippingConfirmationSmsMessage();

		final Map<String, String> map = new HashMap<>();
		map.put("{0}", consignmentModel.getOrder().getCode());
		message = replace(message, map);

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
		final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());

		String sendSMSMessageWithDescription = null;
		consignmentModel.setSmsShippingConfirmationRequest(new SmsForm(mobileNumber, FOOD_CROWD, message).toString());
		final Optional<OTPStrategy> strategy = getStrategy(otpProviderModel.get().getClass());
		if (strategy.isEmpty())
		{
			LOG.error(STRATEGY_WAS_NOT_FOUND_FOR + otpProviderModel.get().getClass());
			return false;
		}
		try
		{
			sendSMSMessageWithDescription = strategy.get().sendSMSMessageWithDescription(mobileNumber, message,
					otpProviderModel.get());
		}
		catch (final Exception e)
		{
			LOG.error(EXCEPTION_OCCURRED_SENDING_SMS_DEFAULT_OTP_CONTEXT_324, e);
			consignmentModel.setSmsShippingConfirmationResponse(e.getMessage());
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return false;
		}
		consignmentModel.setSmsShippingConfirmationResponse(sendSMSMessageWithDescription);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
		return true;

	}


	@Override
	public boolean sendOrderConfirmationWhatsappMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE,
				"Order Confirmation SMS messages not supported for this store");
	}

	@Override
	public boolean sendOrderDeliveredWhatsappMessage(final ConsignmentModel consignmentModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Delivery SMS messages not supported for this store");
	}

}
