package com.earth.earthotp.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import com.earth.earthotp.enums.OTPVerificationTokenType;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.TokenInvalidatedException;
import com.earth.earthotp.model.OTPVerificationTokenModel;




/**
 * The Interface OTPContext.
 *
 * @author mnasro
 */
public interface CustomOTPContext
{

	public boolean isEnabledByCurrentSite(final OTPVerificationTokenType type);

	public boolean isEnabled(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel);

	public Optional<OTPVerificationTokenModel> sendOTPCode(final OTPVerificationTokenType type, final String countryisoCode,
			final String mobileNumber, final Object data, final CustomerModel customerModel, final CMSSiteModel cmsSiteModel)
			throws OTPException;

	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSite(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data, final CustomerModel customerModel)
			throws OTPException;

	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSiteAndCustomer(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data) throws OTPException;

	public boolean verifyCode(final String token, final String countryisoCode, final String mobileNumber, final String code,
			final CustomerModel customerModel, final CMSSiteModel cmsSiteModel) throws OTPException, TokenInvalidatedException;

	public boolean verifyCodeByCurrentSite(final String token, final String countryisoCode, final String mobileNumber,
			final String code, final CustomerModel customerModel) throws OTPException, TokenInvalidatedException;

	public boolean verifyCodeByCurrentSiteAndCustomer(final String token, final String countryisoCode, final String mobileNumber,
			final String code) throws OTPException, TokenInvalidatedException;

	public boolean verifyCodeByCurrentSiteAndCustomer(final String token, final String code)
			throws OTPException, TokenInvalidatedException;

	public boolean verifyToken(final String token, final CustomerModel customerModel) throws TokenInvalidatedException;

	public boolean verifyTokenByCurrentCustomer(final String token) throws TokenInvalidatedException;

	public Optional<OTPVerificationTokenModel> getToken(final String token, final CustomerModel customerModel)
			throws TokenInvalidatedException;

	public Optional<OTPVerificationTokenModel> getTokenByCurrentCustomer(final String token) throws TokenInvalidatedException;


	public void removeToken(final String token, final CustomerModel customerModel) throws TokenInvalidatedException;

	public void removeTokenByCurrentCustomer(final String token) throws TokenInvalidatedException;

	public Optional<OTPVerificationTokenModel> generateToken(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode, final CustomerModel customerModel);

	public Optional<OTPVerificationTokenModel> generateTokenCurrentCustomer(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode);


}
