package com.earth.earthotp.unifonic.service;

import com.earth.earthotp.unifonic.exception.UnifonicException;


public interface UnifonicService
{
	boolean sendVerificationCode(final String baseUrl, final String appSid, final String message,
			final String countryCode, final String messageTo) throws UnifonicException;

	boolean verifyCode(final String baseUrl, final String appSid, final String countryCode,
			final String messageTo, final String code) throws UnifonicException;
}
