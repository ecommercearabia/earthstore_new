package com.earth.earthotp.unifonic.exception;

import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.enums.OTPExceptionType;
import com.earth.earthotp.unifonic.exception.type.UnifonicExceptionType;


public class UnifonicException extends OTPException
{

	private static final long serialVersionUID = 198494638948933538L;
	private final UnifonicExceptionType unifonicExceptionType;


	public UnifonicException(final String message, final UnifonicExceptionType type, final String errorResponse)
	{

		super(findOTPExceptionType(type), message, errorResponse);

		this.unifonicExceptionType = type;


	}

	/**
	 *
	 */
	private static OTPExceptionType findOTPExceptionType(final UnifonicExceptionType type)
	{
		// XXX Auto-generated method stub
		return null;
	}

	/**
	 * @return the UnifonicExceptionType
	 */
	public UnifonicExceptionType getUnifonicExceptionType()
	{
		return unifonicExceptionType;
	}



}
