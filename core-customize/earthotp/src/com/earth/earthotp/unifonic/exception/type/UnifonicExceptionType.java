/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.unifonic.exception.type;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum UnifonicExceptionType
{
	INVALID_APPSID("ER-01", "Invalid AppSid"),
	MISSING_PARAMETER("ER-02", "Missing parameter"),
	SENDER_ID_ALREADY_EXISTS("ER-03", "Sender ID already exists"),
	WRONG_SENDER_ID_FORMAT("ER-04", "Wrong sender ID format, sender ID should not exceed 11 characters or 16 numbers, only English letters allowed with no special characters or spaces"),
	SENDER_ID_IS_BLOCKED("ER-05", "Sender ID is blocked and can’t be used"),
	SENDER_ID_DOESNT_EXIST("ER-06", "Sender ID doesn't exist"),
	DEFAULT_SENDER_ID_CANT_BE_DELETED("ER-07", "Default sender ID can't be deleted"),
	SENDER_ID_IS_NOT_APPROVED("ER-08", "Sender ID is not approved"),
	NO_SUFFICIENT_BALANCE("ER-09", "No sufficient balance"),
	WRONG_NUMBER_FORMAT("ER-10", "Wrong number format , mobile numbers must be in international format without 00 or + Example: (4452023498)"),
	UNSUPPORTED_DESTINATION("ER-11", "Unsupported destination"),
	MESSAGE_BODY_EXCEEDED_LIMIT("ER-12", "Message body exceeded limit"),
	SERVICE_NOT_FOUND("ER-13", "Service not found"),
	SENDER_ID_IS_BLOCKED_ON_THE_REQUIRED_DESTINATION("ER-14", "Sender ID is blocked on the required destination"),
	USER_IS_NOT_ACTIVE("ER-15", "User is Not active"),
	EXCEED_THROUGHPUT_LIMIT("ER-16", "Exceed throughput limit"),
	INAPPROPRIATE_CONTENT_IN_MESSAGE_BODY("ER-17", "Inappropriate content in message body"),
	INVALID_MESSAGE_ID("ER-18", "Invalid Message ID"),
	WRONG_DATE_FORMAT("ER-19", "Wrong date format, date format should be yyyy-mm-dd"),
	PAGE_LIMIT_EXCEEDED("ER-20", "Page limit Exceeds (10000)"),
	METHOD_NOT_FOUND("ER-21", "Method not found."),
	LANGUAGE_NOT_SUPPORTED("ER-22", "Language not supported"),
	YOU_HAVE_EXCEEDED_YOUR_SENDER_IDS_REQUESTS_LIMIT("ER-23", "You have exceeded your Sender ID’s requests limit, delete one sender ID to request a new one"),
	WRONG_STATUS_DATA("ER-24", "Wrong Status data , message status should be one of the following statuses “Queued” , “Sent” , “Failed” or “Rejected”"),
	THIS_REQUEST_IS_NOT_INCLUDED_IN_YOUR_ACCOUNT_PLAN("ER-25", "This request is not included in your account plan"),
	INVALID_CALL_ID("ER-26", "Invalid Call ID"),
	WRONG_STATUS_DATA_0("ER-27", "Wrong Status Data"),
	WRONG_EMAIL_FORMAT("ER-28", "Wrong Email Format"),
	INVALID_EMAIL_ID("ER-29", "Invalid Email ID"),
	INVALID_SECURITY_TYPE("ER-30", "Invalid Security Type"),
	WRONG_PASSCODE("ER-31", "Wrong Passcode"),
	PASSCODE_EXPIRED("ER-32", "Passcode expired"),
	WRONG_CHANNEL_TYPE("ER-33", "Wrong channel type, Channel value should be TextMessage, Call or Both"),
	WRONG_TIME_TO_LIVE_VALUE_TTL("ER-34", "Wrong time to live value TTL, TTL should be between 1 – 60 minutes, and should not exceed the Expiry time"),
	MESSAGEID_ALREADY_SENT("ER-35", "MessageID already sent"),
	WRONG_VOICE_TYPE("ER-36", "Wrong voice type, voice value should be Male or Female"),
	WRONG_DELAY_TYPE("ER-37", "Wrong Delay type, Delay value should be between 0 and 5 seconds"),
	INVALID_NUMBER("ER-38", "Invalid number; number is not available or had expired for the submitted AppSid"),
	INVALID_RULE("ER-39", "Invalid Rule ; rule should be: Is, StartsWith, Contains, Any. Only \"Is\" is available for a shared number"),
	KEYWORD_NOT_DEFINED("ER-40", "Keyword not defined"),
	INVALID_COUNTRY_CODE("ER-41", "Invalid Country code"),
	INTERNAL_ERROR("000", "Internal Error"),
	UNKNOWN_ERROR_CODE("001", "Uknown Error Code");

	private final String code;
	private final String msg;

	/**
	 *
	 */
	private UnifonicExceptionType(final String code, final String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}


	private static Map<String, UnifonicExceptionType> map = new HashMap<>();

	static
	{
		for (final UnifonicExceptionType wathqExceptionType : UnifonicExceptionType.values())
		{
			map.put(wathqExceptionType.code, wathqExceptionType);
		}
	}

	public static UnifonicExceptionType getExceptionFromCode(final String code)
	{
		final UnifonicExceptionType exception = map.get(code);

		return exception == null ? UNKNOWN_ERROR_CODE : exception;
	}


}
