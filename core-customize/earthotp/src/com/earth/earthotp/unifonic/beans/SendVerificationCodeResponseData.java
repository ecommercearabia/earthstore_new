package com.earth.earthotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SendVerificationCodeResponseData
{
	@SerializedName("VerifyID")
	@Expose
	private String verifyID;
	@SerializedName("MessageID")
	@Expose
	private long messageID;
	@SerializedName("MessageStatus")
	@Expose
	private String messageStatus;
	@SerializedName("NumberOfUnits")
	@Expose
	private int numberOfUnits;
	@SerializedName("Cost")
	@Expose
	private String cost;
	@SerializedName("CurrencyCode")
	@Expose
	private String currencyCode;
	@SerializedName("Balance")
	@Expose
	private String balance;
	@SerializedName("Recipient")
	@Expose
	private String recipient;
	@SerializedName("TimeCreated")
	@Expose
	private String timeCreated;

	public String getVerifyID()
	{
		return verifyID;
	}

	public void setVerifyID(final String verifyID)
	{
		this.verifyID = verifyID;
	}

	public long getMessageID()
	{
		return messageID;
	}

	public void setMessageID(final long messageID)
	{
		this.messageID = messageID;
	}

	public String getMessageStatus()
	{
		return messageStatus;
	}

	public void setMessageStatus(final String messageStatus)
	{
		this.messageStatus = messageStatus;
	}

	public int getNumberOfUnits()
	{
		return numberOfUnits;
	}

	public void setNumberOfUnits(final int numberOfUnits)
	{
		this.numberOfUnits = numberOfUnits;
	}

	public String getCost()
	{
		return cost;
	}

	public void setCost(final String cost)
	{
		this.cost = cost;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public String getBalance()
	{
		return balance;
	}

	public void setBalance(final String balance)
	{
		this.balance = balance;
	}

	public String getRecipient()
	{
		return recipient;
	}

	public void setRecipient(final String recipient)
	{
		this.recipient = recipient;
	}

	public String getTimeCreated()
	{
		return timeCreated;
	}

	public void setTimeCreated(final String timeCreated)
	{
		this.timeCreated = timeCreated;
	}

}
